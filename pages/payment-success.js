import React from "react";
import {Button
} from "@material-ui/core";
import Router from 'next/router';

function Payment() {

  const routeToDashboard = async (event) => {
    if (true){
    //if (props.supplierId){
        //const result = await axios.post('/api/create_conversation/' + event.target.name)
        Router.push(
            {
                pathname: '/dashboard'               
            }
        );
    }
};

  return (
    <>
      Payment Successful!
      <Button
      color="#008000"
      variant="contained"
      onClick={routeToDashboard}>
          Back to Dashboard
      </Button>
    </>
  );
}

export default Payment;