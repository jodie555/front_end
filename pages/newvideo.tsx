import React from 'react';
import ReactDOM from 'react-dom';

import { CssBaseline } from '@material-ui/core';
import { MuiThemeProvider } from '@material-ui/core/styles';

import App from '../components/VideoComponents/App/App';
import AppStateProvider, { useAppState } from '../state';
import { BrowserRouter as Router, Navigate , Route, Routes  } from 'react-router-dom';
import ErrorDialog from '../components/VideoComponents/ErrorDialog/ErrorDialog';
// import LoginPage from '../components/VideoComponents/LoginPage/LoginPage';
// import PrivateRoute from '../components/VideoComponents/PrivateRoute/PrivateRoute';
import theme from '../styles/videoTheme/theme';
import '../components/VideoComponents/types';
import { ChatProvider } from '../components/VideoComponents/ChatProvider';
import { VideoProvider } from '../components/VideoComponents/VideoProvider';
import useConnectionOptions from '../utils/useConnectionOptions/useConnectionOptions';
import UnsupportedBrowserWarning from '../components/VideoComponents/UnsupportedBrowserWarning/UnsupportedBrowserWarning';
import { withRouter } from 'next/router'

const VideoApp = () => {
  const { error, setError } = useAppState();
  const connectionOptions = useConnectionOptions();

  return (
    <VideoProvider options={connectionOptions} onError={setError}>
      <ErrorDialog dismissError={() => setError(null)} error={error} />
      <ChatProvider>
        <App />
      </ChatProvider>
    </VideoProvider>
  );
};

const WholeApp = (props:any) => {

  return(
    <MuiThemeProvider 
    theme={theme}
    >
    <CssBaseline />
    <UnsupportedBrowserWarning>
      <Router>
        <AppStateProvider 
        client={props.router.query.client}
        requestId={props.router.query.requestId} 
        supplierId={props.router.query.supplierId}
        userId={props.router.query.userId}
        >

            {/* <PrivateRoute path="/">
              <VideoApp />
            </PrivateRoute>
            <PrivateRoute path="/room/:URLRoomName">
              <VideoApp />
            </PrivateRoute>
            <Route path="/login">
              <LoginPage />
            </Route>
            <Navigate to="/" /> */}
            <VideoApp />

        </AppStateProvider>
      </Router>
    </UnsupportedBrowserWarning>
  </MuiThemeProvider>
  )
}


export default withRouter(WholeApp)
