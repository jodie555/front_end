import withRoot from '../components/Landing/withRoot';
// --- Post bootstrap -----
import React, { Component,useState,useEffect } from "react";

import { useRouter } from 'next/router';

import VideoContainer from "../components/Video/Main.js";
import DashboardContainer from "../components/Dashboard/DashboardContainer/DashboardContainer"

function Index() {
  const [isSupplier, setIsSupplier] = useState('{"Dummy":"Dummy"}')
  useEffect(() => {
    setIsSupplier(JSON.parse(localStorage.getItem("isSupplier")))
    }, []);


  const router = useRouter()
  console.log("router",router.query.isUser)
    return (
      <DashboardContainer
        isUserDasboard = {false}
        isSupplier = {isSupplier}
      >
          <VideoContainer 
          supplierId={router.query.supplierId}
          requestId={router.query.requestId} 
          userId={router.query.userId}
          isUser={router.query.isUser}
          />
    </DashboardContainer>

      
    );
  }
  
  export default withRoot(Index);