
import React, { useState, useEffect } from 'react';

import { GoogleLogin } from 'react-google-login';
import axiosDefault from '../../utils/axios'
const axios = axiosDefault.axiosDefault()
import { Client as ConversationsClient } from "@twilio/conversations";
import ChatBox from "../../components/ChatRoom/ChatBox/ChatBox";
import ChatForm from "../../components/ChatRoom/ChatForm/ChatForm"
import ChatInfo from "../../components/ChatRoom/ChatInfo/ChatInfo"
import StartCallButton from "../../components/ChatRoom/StartCallButton/StartCallButton"

import EndChatRoomButton from "../../components/ChatRoom/EndChatRoomButton/EndChatRoomButton"


import { useRouter } from 'next/router'
import Router from "next/router";
import { Row, Column, Item } from "@mui-treasury/components/flex";


declare var process : {
  env: {
    BACKEND_URL: string,
    REACT_APP_GOOGLE_CLIENT_ID:string
  }
}

declare global {
  interface Window {
    conversationsClient:any;
  }
}


function isNotEmpty(obj:any) {
  return Object.keys(obj).length > 0;
}


const App = (context:any) => {
    const router = useRouter()

    const [count, setCount] = useState(0);
    const [token, setToken] = useState("");
    const [firstInit, setFristInit] = useState(0)
    // const [selectedConversation,setSelectedConversation] = useState("")
    const [selectedConversation,setSelectedConversation] = useState({
      sendMessage:(value: any) => {},
      getMessages:() => Promise.resolve(),
      on:()=>{}
    })
    const [ selectedConversationSid,setSelectedConversationSid ] = useState("")
    const [ requestId,setRequestId ] = useState("")

    const [chatValues,setChatValues] = useState({
      name:"",
      loggedIn:false,
      token: null,
      statusString: "",
      conversationsReady: false,
      conversations: [],
      newMessage: "",
      status:""
    })
    const [requestInfo,setRequestInfo] = useState({
      chatTitle: "",
      requestStatus: 0,
      supplierId: "",
      supplierImageURL: "",
      supplierName: "",
      supplierUniversity: "",
      supplierMajor: "",

    })


    useEffect(() => {

      if(isNotEmpty(router.query)){

        if(router.query.conversationSID){
          if(selectedConversationSid!=router.query.conversationSID){
            setSelectedConversationSid(router.query.conversationSID as string)

          }
          if(requestId!=router.query.requestId){
            setRequestId(router.query.requestId as string)
          }
  
        }else{

          Router.push(
            {
              pathname: "/chat-room/create-chat-room-button",
  
            }
          );
        }
      }

      if(token===""){
        getToken()
      }else{
        if(firstInit===0){
          initConversations()
          setFristInit(1)
        }

      }
      // getToken()
    },[chatValues,router]);
    console.log("requestInfo",requestInfo)

    useEffect(()=>{


      if(selectedConversationSid){
        axios.get(
          "/api/requests/" +
          selectedConversationSid +
            "/chatroom-info"
        ).then(response => {
          console.log("abcde")
          setRequestInfo({
            ...requestInfo,
            chatTitle:response.data.description,
            requestStatus: response.data.status,
            supplierId: response.data._id,
            supplierImageURL: response.data.supplierId.avatarUrl,
            supplierName: response.data.supplierId.firstName,
            supplierUniversity: response.data.supplierId.universityIdentities[0].university.name,
            supplierMajor: response.data.supplierId.universityIdentities[0].university.name,
      
          })
        })

      }
    },[selectedConversationSid])


    const handleInputChange = (e:any) => {
      // event.preventDefault();
      const { name, value } = e.target;
      setChatValues((prevState:any) => ({
          ...prevState,
          [name]: value
      }));
    }

    const getToken = async () => {
      const response = await axios.get(`/token/id`);
      console.log("response", response.data);
      setToken(response.data.token)
      setChatValues({...chatValues,name: response.data.identity})

    }

    const initConversations = async () => {
      window.conversationsClient = ConversationsClient;
      if(token!==""){
        var conversationsClient = await ConversationsClient.create(token)


        setChatValues({...chatValues,statusString: "Connecting to Twilio…"})
        
        conversationsClient.on("connectionStateChanged", (state) => {
          if (state === "connecting")
            setChatValues({
              ...chatValues,
              statusString: "Connecting to Twilio…",
              status: "default"
            });
          if (state === "connected") {
            setChatValues({
              ...chatValues,
              statusString: "You are connected.",
              status: "success"
            });
          }
          if (state === "disconnecting")
            setChatValues({
              ...chatValues,
              statusString: "Disconnecting from Twilio…",
              conversationsReady: false,
              status: "default"
            });

          if (state === "disconnected")
            setChatValues({
              ...chatValues,
              statusString: "Disconnected",
              conversationsReady: false,
              status: "warning"
            });

          if (state === "denied")
            setChatValues({
              ...chatValues,
              statusString: "Failed to connect.",
              conversationsReady: false,
              status: "error"
            });
        });
        conversationsClient.on("conversationJoined", (conversation) => {
          if (conversation.sid === selectedConversationSid) {
            setSelectedConversation(conversation)
          }
        });
        conversationsClient.on("conversationLeft", (thisConversation) => {
          setChatValues({
            ...chatValues,
            conversations: [...chatValues.conversations.filter((it) => it !== thisConversation)]
          });

        });
      }
    };

    const sendMessage = (event:any) => {
      event.preventDefault();
  
      // const message = this.state.newMessage;
      if (selectedConversation) {
        selectedConversation.sendMessage(chatValues.newMessage);
      }
      setChatValues({...chatValues,newMessage: ""})

    };





    let conversationContent;
    if (selectedConversation) {
      conversationContent = (
        <ChatBox
          conversationProxy={selectedConversation}
          myIdentity={chatValues.name}
          email={chatValues.name}
        />
      );
    }

    const buttonInfo = {
      requestId:requestId,
      supplierId:requestInfo.supplierId,
    }


    return (
      <div >

        <header>
          <Row p={1.5} gap={1}  borderRadius={8}>

          <ChatInfo
            {
              ...requestInfo
            }
          />
          <Column position={"right"}>
            <Item ml={1} position={"middle"}>
              <StartCallButton {...buttonInfo}/>
            </Item>
          </Column>
          <Column position={"right"}>
            <Item ml={1} position={"middle"}>
              <EndChatRoomButton/>
            </Item>
          </Column>
          </Row>
          {conversationContent}
          <ChatForm
          sendMessage={sendMessage}
          handleInputChange={handleInputChange}
          sendButtonDisabled={!chatValues.newMessage}
          newMessage={chatValues.newMessage}
        />
        </header>
      </div>
    );
  }
  
  export default App;
  