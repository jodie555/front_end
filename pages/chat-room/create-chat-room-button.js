
import React, { useState, useEffect } from 'react';

import axiosDefault from '../../utils/axios'
const axios = axiosDefault.axiosDefault()
import { Button } from "@material-ui/core";
import Router from "next/router";

function BeginChatButton(props) {
  const [isDisabled, setIsDisabled] = useState(false);

  const routeToChatroom = async (event) => {

    //Create a new request
    let response = await axios.post(`/api/requests/user`, {supplierId: "61ad335e94c036b963a9324a"});
    console.log("response", response)
    let chatroomResponse;

    let conversationSID;
    if (response.data.request.chatroomId){
      chatroomResponse = await axios.get(`/api/get_conversationId/`+response.data.request.chatroomId);
      conversationSID = chatroomResponse.data.conversationSID;
    }else{
      conversationSID = null;
    }



    if (!conversationSID) {
      const result = await axios.post(`/api/create_conversation/request`, {
        requestId: response.data.request._id,
      });
      conversationSID = result.data.conversationSID;
    }

    Router.push(
      {
        pathname: "/chat-room/chat-room",
        query: {
          conversationSID: conversationSID,
          requestId: response.data.request._id,
          isUser: true,
        },
      }
    );
  };

  return (
    <Button
      color="primary"
      variant="contained"
      disabled={isDisabled}
      onClick={routeToChatroom}
    >
      Chat
    </Button>
  );
}

export default BeginChatButton;
  