import withRoot from '../components/Landing/withRoot';
// --- Post bootstrap -----
import React from 'react';
import ProductReviews from '../components/Landing/views/ProductReviews';
import ProductSmokingHero from '../components/Landing/views/ProductSmokingHero';
import AppFooter from '../components/Landing/views/AppFooter';
import ProductHero from '../components/Landing/views/ProductHero';
import SellingPoints from '../components/Landing/views/SellingPoints';
import ProductHowItWorks from '../components/Landing/views/ProductHowItWorks';
import ProductCTA from '../components/Landing/views/ProductCTA';
import AppAppBar from '../components/Landing/views/AppAppBar';
import Header from "../components/Header/Header.js";
import CssBaseline from "@material-ui/core/CssBaseline";
import { createTheme, ThemeProvider } from "@material-ui/core/styles";

function Index() {

  return (
      <React.Fragment>
        <CssBaseline />
        <Header />
        <ProductHero />
        <SellingPoints />
        <ProductHowItWorks />
        <ProductReviews />
        {/* <ProductCTA />
        <ProductSmokingHero /> */}
        <AppFooter />
      </React.Fragment>
    
  );
}

export default withRoot(Index);