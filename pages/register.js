import React, { Component } from "react";
import  axiosDefault from '../utils/axios'
const axios = axiosDefault.axiosDefault()
import Router from "next/router";
import PropTypes from "prop-types";
import SignUpContainer from "../components/SignUp/SignUpContainer";
// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import { withRouter } from "next/router";
import {
  MuiThemeProvider,
  createTheme,
  withStyles,
  makeStyles,
} from "@material-ui/core/styles";
import { Paper, ThemeProvider, Box, Tab, Tabs } from "@material-ui/core";
import {
  validateSignUpForm,
  validateSignUpFormSupplier,
  validateSignUpFormSupplierOther,
} from "../functions/validation/signup/validate";
import CssBaseline from "@material-ui/core/CssBaseline";
import Header from "../components/Header/Header.js";
import RegisterContainer from "../components/SignUp/RegisterContainer.js";
import Copyright from "../components/Copyright/Copyright.js";
import setLocalStorage from "../functions/localStorage/localStorage.js";
import routeToDashboard from "../functions/routings/defaultRoute.js";
// const styles = theme => ({
//   root:{
//     backgroundColor:"red"
//   }
// })

const styles = (theme) => ({
  paper: {
    marginTop: theme.spacing(9),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
});

class ChoiceClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      password: null,
      username: null,
      avatar: null,
      categoryCollection: [],
      universityOptions: [],
      universityMajorOptions: [],
      errors: {},
      formState: 0,
      token: null,
      supplierGoogleLoggedIn: false
    };
  }

  theme = createTheme({
    palette: {
      primary: {
        light: "#2ecc71",
        main: "#2ecc71",
        dark: "#2ecc71",
        contrastText: "#fff",
      },
      secondary: {
        light: "#ff7961",
        main: "#f44336",
        dark: "#ba000d",
        contrastText: "#000",
      },
    },
  });

  useStyles = makeStyles((theme) => ({
    root: {
      marginTop: theme.spacing(10),
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
  }));

  async componentDidMount() {
    const universityOptions = await axios
      .get(`/api/universities`)
      .catch((e) => {
        return e;
      });
    const universityMajorOptions = await axios
      .get(`/api/subjects`)
      .catch((e) => {
        return e;
      });

    // console.log(universityOptions)
    console.log(universityOptions.data)
    this.setState({
      universityOptions: universityOptions.data,
      universityMajorOptions: universityMajorOptions.data,
    });
  }

  responseGoogleUser = async (googleData) => {
    const res = await axios.post(`/api/google/register`, {
      token: googleData.tokenId,
    });
    const data = res.data;
    if (res.data.status === true) {
      setLocalStorage(res);
      // localStorage.setItem('isSupplier', res.data.isSupplier && res.data.verifiedSupplierStatus);
      // localStorage.setItem('userId', res.data._id);

      routeToDashboard(1);
    } else {
      this.setState({
        errors: { message: res.data.message },
      });
    }
  };

  responseGoogleSupplier = async (googleData) => {
    const res = await axios.post(`/api/google/register?registerAsSupplier=true`, {
      token: googleData.tokenId,
    });
    // .catch(err => err)
    // store returned user somehow
    console.log("res", res);
    if (res.data) {
      // Router.push(
      //   {
      //     pathname: "/register/otherSignUpSupplier",
      //     query: { token: googleData.tokenId },
      //   },
      //   "/register/otherSignUpSupplier"
      // );
      this.setState({
        token: googleData.tokenId,
        supplierGoogleLoggedIn: true,
      })

    } else {
      this.setState({
        errors: res.message,
      });
    }
  };

  validateMethodUser = (user) => {
    var payload = validateSignUpForm(user);

    return payload;
  };

  validateMethodSupplier = (user) => {
    var payload = validateSignUpFormSupplier(user);

    return payload;
  };

  validateMethodGoogleLoggedInSupplier = (user) => {
    var payload = validateSignUpFormSupplierOther(user);

    return payload;
  };

  handleChange = (event, formStateClicked) => {
    this.setState({
      formState: formStateClicked,
    });
  };

  routeToNextPage = (messageStatusNum) => {
    console.log("messageStatusNum",messageStatusNum)
    Router.push(
      {
        pathname: "/message",
        query: {messageStatus: messageStatusNum} //"00002"
      }
    )
  }

  submitSignupUser = async (user) => {
    var body = { username: user.usr, password: user.pw, email: user.email };
    axios
      .post("/api/users", body)
      .then((res) => {
        if (res.data.status === true) {
          this.routeToNextPage("00002")
        } else {
          this.setState({
            errors: { message: res.data.message },
          });
        }
      })
      .catch((err) => {
        this.setState({
          errors: err.response.data,
        });
      });
  };

  submitSignupSupplier = async (user) => {
    var bodyFormData = new FormData();
    // user.usr && bodyFormData.append("username", user.usr);
    user.pw && bodyFormData.append("password", user.pw);
    user.email && bodyFormData.append("email", user.email);
    user.avatar && bodyFormData.append("avatar", user.avatar);
    user.university && bodyFormData.append("university", user.university);
    user.universityMajor &&
      bodyFormData.append("universityMajor", user.universityMajor);
    user.universityEmail &&
      bodyFormData.append("universityEmail", user.universityEmail);

    // const message = await axios.post("/api/users", body)
    // console.log("message",message.response)
    console.log("sending API request for supplier new")
    axios
      .post("/api/suppliers", bodyFormData)
      .then((res) => {
        if (res.data.success === true) {
          // localStorage.token = res.data.token;
          // localStorage.isAuthenticated = true;
          // window.location.reload();
          routeToNextPage(res.data.messageStatus)
          console.log("successful")
          
        } else {
          this.setState({
            errors: { message: res.data.message },
          });
        }
      })
      .catch((err) => {
        console.log("Sign up data submit error: ", err);

        this.setState({
          errors: err.response.data,
        });
      });
  };

  submitSignupGoogleLoggedInSupplier = async (user) => {
    var bodyFormData = new FormData();
    user.avatar && bodyFormData.append("avatar", user.avatar);
    user.university && bodyFormData.append("university", user.university);
    user.universityMajor &&
      bodyFormData.append("universityMajor", user.universityMajor);
    user.universityEmail &&
      bodyFormData.append("universityEmail", user.universityEmail);
    this.state.token && bodyFormData.append("token", this.state.token);
    // console.log("token",this.state.token)

    axios
      .post("/api/google/register/supplier/add-additional-info", bodyFormData)
      .then((res) => {
        console.log("res.data", res.data);
        if (res.data.success === true) {
          routeToNextPage(res.data.messageStatus)
        } else {
          this.setState({
            errors: { message: res.data.message },
          });
        }
      })
      .catch((err) => {
        // console.log("Sign up data submit error: ", err);

        this.setState({
          errors: err.response.data,
        });
      });
  };

  render() {
    const { errors } = this.state;
    const { classes } = this.props;

    return (
      <ThemeProvider theme={this.theme}>
        <CssBaseline />
        <Header />
        <Paper square className={classes.paper}>
          <Tabs
            value={this.state.formState}
            onChange={this.handleChange}
            variant="fullWidth"
            indicatorColor="primary"
            textColor="primary"
            aria-label="icon tabs sign up"
          >
            <Tab label="User Sign up" />
            <Tab label="Mentor Sign up" />
          </Tabs>

          {this.state.formState === 0 && (
            <SignUpContainer
              submitSignup={this.submitSignupUser}
              errors={errors}
              responseGoogle={this.responseGoogleUser}
              validateMethod={this.validateMethodUser}
            />
          )}

          {this.state.formState === 1 && !this.state.supplierGoogleLoggedIn && (
            <SignUpContainer
              forSupplier
              // universityOptions = {[{_id:"4f4f4",name:"cece"},{_id:"4f43",name:"c4f4"}]}
              universityOptions={this.state.universityOptions}
              universityMajorOptions={this.state.universityMajorOptions}
              responseGoogle={this.responseGoogleSupplier}
              submitSignup={this.submitSignupSupplier}
              validateMethod={this.validateMethodSupplier}
              errors={errors}
            />
          )}

          {this.state.formState === 1 &&this.state.supplierGoogleLoggedIn && (
            <SignUpContainer
            forSupplier
            forOtherSignUpSupplier
            // universityOptions = {[{_id:"4f4f4",name:"cece"},{_id:"4f43",name:"c4f4"}]}
            universityOptions = {this.state.universityOptions}

            universityMajorOptions = {this.state.universityMajorOptions}
            token = {this.state.token}
            submitSignup = {this.submitSignupGoogleLoggedInSupplier}
            validateMethod = {this.validateMethodGoogleLoggedInSupplier}
            errors = {errors}
            />
          )}
        </Paper>
        <Box mt={8}>
          <Copyright />
        </Box>

        {/* <SignUpContainer 
            forSupplier
            // universityOptions = {[{_id:"4f4f4",name:"cece"},{_id:"4f43",name:"c4f4"}]}
            universityOptions = {universityOptions}

            universityMajorOptions = {universityMajorOptions}
            responseGoogle = {this.responseGoogle}
            submitSignup = {this.submitSignup}
            validateMethod = {this.validateMethod}
            errors = {errors}
            /> */}
      </ThemeProvider>
      // <SignUpContainer
      // submitSignup = {this.submitSignup}
      // errors = {errors}
      // />
    );
  }
}

ChoiceClass.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ChoiceClass);
