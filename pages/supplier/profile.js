import React, { Component,useState,useEffect } from "react";
// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import UniversityIdentityCardLayout from "../../components/Card/UniversityIdentityCardLayout/UniversityIdentityCardLayout.js";
import DashboardContainer from "../../components/Dashboard/DashboardContainer/DashboardContainer"
import DeleteUniversityIdentityButton from "../../components/Buttons/DeleteUniversityIdentityButton/DeleteUniversityIdentityButton"
import AddUniversityIdentityButton from "../../components/Buttons/AddUniversityIdentityButton/AddUniversityIdentityButton"

import getUniversityIdentityById from '../../functions/getUniversityIdentityById/getUniversityIdentityById.js';
import getUserInfo from '../../functions/getUserInfo/getUserInfo.js';
import LoadingComponent from '../../components/Loading/Loading'



import Container from "@material-ui/core/Container";
import { Typography, Paper, Link, Grid, Button, Box } from "@material-ui/core";



function profile() {


  const [isSupplier, setIsSupplier] = useState('{"Dummy":"Dummy"}')
  const [universityIdentitiesList, setUniversityIdentitiesList] = useState([])
  const [loading, setLoading] = useState(true)




  useEffect(() => {
    async function getUserData(){
        const result = await getUserInfo(); //TODO
        console.log("result",result)
        var universityIdentities = result.data.data.universityIdentities
        var univerisityIdentitiesInfo = []

        // get the university identity info
        if (typeof universityIdentities !== 'undefined' && universityIdentities ){
          // await Promise.all(universityIdentities.map(async value =>{
          //   var universityIdentityInfo = await getUniversityIdentityById(value)
          //   console.log("value",universityIdentityInfo)
          //   univerisityIdentitiesInfo.push({
          //     university:universityIdentityInfo.data.university,
          //     course:universityIdentityInfo.data.course
          //   })
          //   console.log("univerisityIdentitiesInfo",univerisityIdentitiesInfo)

          // }))

          for (const Id of universityIdentities) {
            var universityIdentityInfo = await getUniversityIdentityById(Id)
            console.log("value",universityIdentityInfo)
            if(universityIdentityInfo.data != ""){
              univerisityIdentitiesInfo.push({
                id:universityIdentityInfo.data._id,
                university:universityIdentityInfo.data.university.name,
                course:universityIdentityInfo.data.course.name,
                entryYear:universityIdentityInfo.data.entryYear
              })
            }


          }

        }
        setUniversityIdentitiesList(univerisityIdentitiesInfo)
        setLoading(false)

    }
    getUserData();
    setIsSupplier(JSON.parse(localStorage.getItem("isSupplier")))
    
  }, []);



  const universityIdentityArray = universityIdentitiesList.map(universityIdentity => {
    return (
      // <div>{universityIdentity.university}</div>
      <UniversityIdentityCardLayout 
      university = {universityIdentity.university}
      course = {universityIdentity.course}
      entryYear = {universityIdentity.entryYear}
      editButton = {
        <DeleteUniversityIdentityButton 
          universityidentityId = {universityIdentity.id}
        
        />
      }
      />
  )
  })


  return (


          <DashboardContainer
            isUserDasboard = {false}
            isSupplier = {isSupplier}
          >
            {universityIdentityArray}
            {loading?LoadingComponent():(
            <AddUniversityIdentityButton />
            )}

          </DashboardContainer>


  );
}

export default profile;
