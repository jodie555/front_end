import React, { Component,useState,useEffect } from "react";
// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import { createTheme} from "@material-ui/core/styles";
import AddIdentityForm from "../../components/AddIdentityForm/AddIdentityForm.js";
import DashboardContainer from "../../components/Dashboard/DashboardContainer/DashboardContainer"


function AddIdentity() {
  const [isSupplier, setIsSupplier] = useState('{"Dummy":"Dummy"}')
  useEffect(() => {
    setIsSupplier(JSON.parse(localStorage.getItem("isSupplier")))
    }, []);

  return (

    <DashboardContainer
      isUserDasboard = {false}
      isSupplier = {isSupplier}

    >
      <AddIdentityForm/>
    </DashboardContainer>
  );

}

export default AddIdentity;