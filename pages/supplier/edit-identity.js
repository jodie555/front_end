import React, { Component,useState,useEffect } from "react";
import Header from "../../components/Header/Header.js";
// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import {  Grid,Paper,Typography,Button } from "@material-ui/core";

import DashboardContainer from "../../components/Dashboard/DashboardContainer/DashboardContainer"
import getUniversityIdentityById from '../../functions/getUniversityIdentityById/getUniversityIdentityById.js';
import DeleteUniversityIdentityButton from "../../components/Buttons/DeleteUniversityIdentityButton/DeleteUniversityIdentityButton"

import Router from 'next/router'
import { withRouter } from 'next/router'

import { useRouter } from 'next/router'



function editIdentity(props) {


  const [isSupplier, setIsSupplier] = useState('{"Dummy":"Dummy"}')
  const [universityIdentityInfo, setUniversityIdentityInfo] = useState(null)
  const [universityidentityId,setUniversityidentityId] = useState("")

  const router = useRouter();


  useEffect(async () => {

    if(router.isReady){
      const universityidentityId = Router.query.universityidentityId
  
      // if we cannot find any university identity in the url
      if(typeof universityidentityId == "undefined" || !universityidentityId){
        console.log("redirected")
        Router.push(
            {
              pathname: "/supplier/profile",
            }
        );
      }else{
        console.log("universityidentityId",universityidentityId)
        console.log("universityidentityId",typeof universityidentityId == "undefined")
        console.log("universityidentityId",!universityidentityId)
        console.log("universityidentityId",universityidentityId == "undefined")
  
  
  
    
        const universityIdentityInfoResult = await getUniversityIdentityById(universityidentityId)
  
        setUniversityIdentityInfo(universityIdentityInfoResult.data)
        setIsSupplier(JSON.parse(localStorage.getItem("isSupplier")))
        setUniversityidentityId(universityidentityId)
        console.log(universityIdentityInfoResult.data)
      }

    }


  }, [router.isReady]);





  return (


          <DashboardContainer
            isUserDasboard = {true}
            isSupplier = {isSupplier}
          >
            <Grid
            container
            spacing={3}
            direction="row"
            justifyContent="center"
            alignItems="center"
            >
              <Grid item xs={12}>
                <Paper >
                  <Typography
                    variant="h4"
                    align="center"
                    component="h1"
                    gutterBottom
                  >
                   University Identity
                  </Typography>
                  <form  encType="multipart/form-data">
                    <Grid container>
                      <Grid item xs={12}>
                        {universityIdentityInfo?universityIdentityInfo.university.name:null}
                      </Grid>
                      <Grid item xs={12}>
                        {universityIdentityInfo?universityIdentityInfo.course.name:null}
                      </Grid>
                      <Grid item xs={12}>
                        {universityIdentityInfo?universityIdentityInfo.verificationEmail:null}
                      </Grid>
                      <Grid item xs={12}>
                        {universityIdentityInfo?universityIdentityInfo.entryYear:null}
                      </Grid>
                      <Grid item xs={12}>

                      </Grid>
                      <Grid item xs={12}>

                      </Grid>

                      {/* <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        onClick={deleteUniversityIdentity}

                      >
                        Delete
                      </Button> */}
                      <DeleteUniversityIdentityButton 
                        universityidentityId = {universityidentityId}
                      
                      />
                    </Grid>
                  </form>
                </Paper>
              </Grid>
            </Grid>

            {/* {universityIdentityInfo?universityIdentityInfo.university.name:null} */}
          </DashboardContainer>


  );
}

export default withRouter(editIdentity);
