
import React, { useState, useEffect } from 'react';
import openSocket, { Socket } from 'socket.io-client'
import RequestCardUserInfo from '../../components/Card/RequestCardUserInfo/RequestCardUserInfo'
import DashboardContainer from "../../components/Dashboard/DashboardContainer/DashboardContainer"
import Paper from "@material-ui/core/Paper";
import TextField from '@material-ui/core/TextField';
import getUsersInfo from '../../functions/getUserRequests/getUserRequests.js';
import LoadingComponent from '../../components/Loading/Loading'




function supplierDashBoard() {




    const [filteredName, setFilteredName] = useState("");

    const [filteredEmail, setFilteredEmail] = useState("");
    const [users, setUsers] = useState([]);
    const [allUsers, setAllUsers] = useState([]);
    const [isSupplier, setIsSupplier] = useState('{"Dummy":"Dummy"}')
    const [loading, setLoading] = useState(true)


    useEffect(() => {
      async function getUsersData(){
          const usersArray = await getUsersInfo(); //TODO
          console.log("usersArray",usersArray)
          setUsers(usersArray);
          setAllUsers(usersArray);
          setLoading(false)

      }
      getUsersData();
      setIsSupplier(JSON.parse(localStorage.getItem("isSupplier")))

      
    }, []);

    useEffect(() => {
      var filteredUsers = [];

      for (const user of allUsers){

          if(user.userName){
            let userNameStr = user.userName.toLowerCase();
            let filteredNameStr = filteredName.toLowerCase();
  
            // let userEmailStr = user.email.toLowerCase();
            // let filteredEmailStr = filteredEmail.toLowerCase();
  
            if (userNameStr.includes(filteredNameStr)){
              filteredUsers.push(user);
            }
          }
          else if(filteredName == ""){
            filteredUsers.push(user);
          }
          
      }
      setUsers(filteredUsers)
  }, [filteredName, filteredEmail])




    const usersArray = users.map(users => {
      return (
      <RequestCardUserInfo 
      user = {users}
    />)
    })
    return (


            <DashboardContainer
              isUserDasboard = {false}
              isSupplier = {isSupplier}
            >
               {loading?LoadingComponent():(
                <div style={{maxHeight: 500, overflow: 'auto'}}>
                <Paper square >
                  {/* https://v4.mui.com/components/text-fields/#text-field */}
                  <TextField 
                      id="standard-search" 
                      label="Filter Name" 
                      type="search"
                      value={filteredName}
                      onChange={(e) => setFilteredName(e.target.value)}
                      style={{ margin: "0px 22px 8px 0px" }}
                  />

                  {/* <TextField 
                      id="standard-search" 
                      label="Filter Email" 
                      type="search"
                      value={filteredEmail}
                      onChange={(e) => setFilteredEmail(e.target.value)}
                  /> */}
                  
              </Paper>
              </div>
               )}
              {usersArray}
            </DashboardContainer>


    );
  }
  
  export default supplierDashBoard;
  