import React from "react";
import axios from "../../util/api"
import Router from 'next/router'

class SupplierList extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      result:""
    }
  }

  componentDidMount = async () => {
    const result = await axios.get(`/api/requests/supplier`).catch(e=>{return e})
    console.log("result",result)
    

    this.setState({result:result})
    
  }

  createRoom = async event => {
    console.log(event.target.name)
    // const result = await axios.post(`/api/create_conversation/full`,{supplierId:event.target.name})
    const result = await axios.get(`/api/chatrooms/`+event.target.name)

    Router.push(
      // '/chat?room=${room}&email=${email}',
    {
      pathname: '/openChatRoom',
      query: { conversationSID: result.data.conversationSID }
    }
      ,'openChatRoom'
    )
  }




  render(){
    const { result} = this.state;


    return (
      <div>
        list of accepted requests
        {/* {resultList} */}
        {result.data && result.data.length > 0 && result.data.map((item)=>
          <button onClick={this.createRoom} name={item.chatroomId}>{item.chatroomId}</button>
        )}

      </div>
    );
  }
  
}




export default SupplierList;