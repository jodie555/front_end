import React from "react";
// import { BrowserRouter, Switch, Route } from "react-router-dom";
import PropTypes from "prop-types";
import ChatScreen from "../../components/ChatRoom/ChatScreen/ChatScreen";
import ChatContainer from "../../components/ChatRoom/ChatRoomContainer/ChatContainer.js";
import ChatHeaderSupplier from "../../components/ChatRoom/ChatHeader/ChatHeaderSupplier";

import { withRouter } from 'next/router'



class OpenNewChatRoom extends React.Component {

  constructor(props){
    super(props);

  }



  render() {

    return (

            <ChatContainer>
              <ChatHeaderSupplier
                selectedConversationSID = {this.props.router.query.conversationSID}
                requestId = {this.props.router.query.requestId}
              />

              <ChatScreen
                selectedConversationSID = {this.props.router.query.conversationSID}
                requestId = {this.props.router.query.requestId}
                isUser = {this.props.router.query.isUser}
              ></ChatScreen>
            </ChatContainer>
      
    )
  }
}

// OpenNewChatRoom.propTypes = {
//   classes: PropTypes.object.isRequired,
// };


export default withRouter(OpenNewChatRoom)

//To retain props upon refreshing
export async function getServerSideProps(context) {
  return {
    props: {}, // will be passed to the page component as props
  };
}