import React, { Component } from "react";
import axios from "../../util/api";
import Router from "next/router";
import PropTypes from "prop-types";
import SignUpContainer from "../../components/SignUp/SignUpContainer";
// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import {
  MuiThemeProvider,
  createTheme,
  withStyles,
  makeStyles,
} from "@material-ui/core/styles";
import { Paper, ThemeProvider, Box } from "@material-ui/core";
import Select from "../../components/Select/select";
import { withRouter } from "next/router";
import {
  validateSignUpForm,
  validateSignUpFormSupplier,
  validateSignUpFormSupplierOther,
} from "../../functions/validation/signup/validate";
import CssBaseline from "@material-ui/core/CssBaseline";
import Header from "../../components/Header/Header.js";
import Copyright from "../../components/Copyright/Copyright.js";

// const styles = theme => ({
//   root:{
//     backgroundColor:"red"
//   }
// })
const styles = (theme) => ({
  paper: {
    marginTop: theme.spacing(9),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
});

class ChoiceClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      password: null,
      username: null,
      avatar: null,
      categoryCollection: [],
      universityOptions: [],
      universityMajorOptions: [],
      token: this.props.router.query.token,
      errors: {},
    };
  }

  theme = createTheme({
    palette: {
      primary: {
        light: "#2ecc71",
        main: "#2ecc71",
        dark: "#2ecc71",
        contrastText: "#fff",
      },
      secondary: {
        light: "#ff7961",
        main: "#f44336",
        dark: "#ba000d",
        contrastText: "#000",
      },
    },
  });

  async componentDidMount() {
    if (!this.state.token) {
      Router.push(
        {
          pathname: "/register/supplier",
        },
        "/register/supplier"
      );
    }

    const universityOptions = await axios
      .get(`/api/universities`)
      .catch((e) => {
        return e;
      });
    const universityMajorOptions = await axios
      .get(`/api/subjects`)
      .catch((e) => {
        return e;
      });

    // console.log(universityOptions)
    this.setState({
      universityOptions: universityOptions.data,
      universityMajorOptions: universityMajorOptions.data,
    });
  }

  handleInputChange = (event) => {
    if (event.target.files) {
      const { name, files } = event.target;

      this.setState({
        [name]: files[0],
      });
    } else {
      const { name, value } = event.target;

      this.setState({
        [name]: value,
      });
    }
  };

  validateMethod = (user) => {
    var payload = validateSignUpFormSupplierOther(user);

    return payload;
  };

  submitSignup = async (user) => {
    var bodyFormData = new FormData();
    user.avatar && bodyFormData.append("avatar", user.avatar);
    user.university && bodyFormData.append("university", user.university);
    user.universityMajor &&
      bodyFormData.append("universityMajor", user.universityMajor);
    user.universityEmail &&
      bodyFormData.append("universityEmail", user.universityEmail);
    this.state.token && bodyFormData.append("token", this.state.token);
    // console.log("token",this.state.token)

    axios
      .post("/api/google/register/supplier/add-additional-info", bodyFormData)
      .then((res) => {
        console.log("res.data", res.data);
        if (res.data.success === true) {
          Router.push(
            {
              pathname: "/dashboard",
            },
            "/dashboard"
          );
        } else {
          this.setState({
            errors: { message: res.data.message },
          });
        }
      })
      .catch((err) => {
        // console.log("Sign up data submit error: ", err);

        this.setState({
          errors: err.response.data,
        });
      });
  };

  render() {
    const { universityOptions, universityMajorOptions, token, errors } =
      this.state;
    const { classes } = this.props;
    return (
      <ThemeProvider theme={this.theme}>
        <CssBaseline />
        <Header />
        <Paper square className={classes.paper}>
          <SignUpContainer
            forSupplier
            forOtherSignUpSupplier
            // universityOptions = {[{_id:"4f4f4",name:"cece"},{_id:"4f43",name:"c4f4"}]}
            universityOptions={universityOptions}
            universityMajorOptions={universityMajorOptions}
            token={token}
            submitSignup={this.submitSignup}
            validateMethod={this.validateMethod}
            errors={errors}
          />
        </Paper>
        <Box mt={8}>
          <Copyright />
        </Box>

        {/* <SignUpContainer 
            forSupplier
            // universityOptions = {[{_id:"4f4f4",name:"cece"},{_id:"4f43",name:"c4f4"}]}
            universityOptions = {universityOptions}

            universityMajorOptions = {universityMajorOptions}
            responseGoogle = {this.responseGoogle}
            submitSignup = {this.submitSignup}
            validateMethod = {this.validateMethod}
            errors = {errors}
            /> */}
      </ThemeProvider>
      // <SignUpContainer
      //   forSupplier
      //   forOtherSignUpSupplier
      //   // universityOptions = {[{_id:"4f4f4",name:"cece"},{_id:"4f43",name:"c4f4"}]}
      //   universityOptions = {universityOptions}

      //   universityMajorOptions = {universityMajorOptions}
      //   token = {token}
      //   submitSignup = {this.submitSignup}
      //   validateMethod = {this.validateMethod}
      //   errors = {errors}
      //   />
    );
  }
}

ChoiceClass.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(ChoiceClass));
