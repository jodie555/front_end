import React, { Component } from "react";
import axios from "../../util/api";
import Router from 'next/router'
import SignUpContainer from '../../components/SignUp/SignUpContainer'
// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import { MuiThemeProvider, createTheme } from '@material-ui/core/styles'
import { Paper, ThemeProvider, Box } from '@material-ui/core';
import Select from "../../components/Select/select"
import {validateSignUpForm,validateSignUpFormSupplier,validateSignUpFormSupplierOther } from "../../functions/validation/signup/validate"

// const styles = theme => ({
//   root:{
//     backgroundColor:"red"
//   }
// })


class ChoiceClass extends React.Component {

    constructor(props){
      super(props);
      this.state = {
        name:"",
        password:null,
        username:null,
        avatar:null,
        categoryCollection:[],
        universityOptions:[],
        universityMajorOptions:[],
        errors: {},

      }
    }

  
    async componentDidMount() {
      const universityOptions = await axios.get(`/api/universities`).catch(e=>e)
      const universityMajorOptions = await axios.get(`/api/subjects`).catch(e=>e)

      // console.log(universityOptions)
      this.setState({
        universityOptions:universityOptions.data,
        universityMajorOptions:universityMajorOptions.data,
      })

  
    }



    responseGoogle = async (googleData ) => {

      const res = await axios.post(`/api/google/register?registerAsSupplier=true`,{token: googleData.tokenId})
                  .catch(err => err)
      // store returned user somehow
      if(res.data){
        Router.push(
          {
            pathname: '/register/otherSignUpSupplier',
            query:{token:googleData.tokenId}
          }
            ,'/register/otherSignUpSupplier'
          )
      } else {
        this.setState({
          errors: res.response.data
        });
      }
  
  
    }


    validateMethod= user => {
      var payload = validateSignUpFormSupplier(user)

      return payload
    }



    submitSignup = async user => {
      var bodyFormData = new FormData();
      user.usr && bodyFormData.append('username',user.usr)
      user.pw && bodyFormData.append('password',user.pw)
      user.email && bodyFormData.append('email',user.email)
      user.avatar && bodyFormData.append('avatar',user.avatar)
      user.university && bodyFormData.append('university',user.university)
      user.universityMajor && bodyFormData.append('universityMajor',user.universityMajor)
      user.universityEmail && bodyFormData.append('universityEmail',user.universityEmail)

  
      // const message = await axios.post("/api/users", body)
      // console.log("message",message.response)

      const res = await axios.post("/api/suppliers", bodyFormData).catch(e=>e)
      

      if(res.data){
        if (res.data.success === true) {
          // localStorage.token = res.data.token;
          // localStorage.isAuthenticated = true;
          // window.location.reload();
        } else {
  
          this.setState({
            errors: { message: res.data.message }
          });
        }
      }else{
        this.setState({
          errors: res.response.data
        });
      }


    }
    
      render() {
        const { universityOptions, universityMajorOptions, errors} = this.state;

        return (
          // <div>dd</div>
          <SignUpContainer 
            forSupplier
            // universityOptions = {[{_id:"4f4f4",name:"cece"},{_id:"4f43",name:"c4f4"}]}
            universityOptions = {universityOptions}

            universityMajorOptions = {universityMajorOptions}
            responseGoogle = {this.responseGoogle}
            submitSignup = {this.submitSignup}
            validateMethod = {this.validateMethod}
            errors = {errors}
            />


        )
      }
    }

export default ChoiceClass