import React, { Component } from "react";
import axios from "../../util/api";
import Router from 'next/router'
import SignUpContainer from '../../components/SignUp/SignUpContainer'
// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import { MuiThemeProvider, createTheme } from '@material-ui/core/styles'
import { Paper, ThemeProvider, Box } from '@material-ui/core';
import {validateSignUpForm,validateSignUpFormSupplier,validateSignUpFormSupplierOther } from "../../functions/validation/signup/validate"

// const styles = theme => ({
//   root:{
//     backgroundColor:"red"
//   }
// })


class ChoiceClass extends React.Component {

    constructor(props){
      super(props);
      this.state = {
        name:"",
        password:null,
        username:null,
        avatar:null,
        categoryCollection:[],
        errors:{},


      }
    }

  
    async componentDidMount() {

  
  
    }




    responseGoogle = async (googleData ) => {

      const res = await axios.post(`/api/google/register`,{token: googleData.tokenId})
                  .catch(err => err)
      console.log("res",res)
      if (res.data.status === true) {
        Router.push(
          {
            pathname: '/dashboard'
          }
            ,'/dashboard'
          )

      } else {

        this.setState({
          errors: res.response.data
        });
      }
  
    }

    validateMethod= user => {
      var payload = validateSignUpForm(user)

      return payload
    }


    submitSignup = async user => {
      var body = { username: user.usr, password: user.pw, email: user.email};
      axios
        .post("/api/users", body)
        .then(res => {

          if (res.data.success === true) {
            Router.push(
              {
                pathname: '/dashboard'
              }
                ,'/dashboard'
              )

          } else {
  
            this.setState({
              errors: { message: res.data.message }
            });
          }
        })
        .catch(err => {
  
          this.setState({
            errors: err.response.data
          });
        });
    }
    
      render() {
        const { errors } = this.state
      
  
        return (

          <SignUpContainer
          submitSignup = {this.submitSignup}
          errors = {errors}
          responseGoogle = {this.responseGoogle}
          />


        )
      }
    }

export default ChoiceClass