import React, { Component } from "react";
import { GoogleLogin } from 'react-google-login';
import axios from "../util/api";
import Router from 'next/router'

export default class ChoiceClass extends React.Component {

    constructor(props){
      super(props);
      this.state = {
        email:"",
        password:""
      }
    }

  
    async componentDidMount() {
  
  
  
    }

    responseGoogle = async (googleData ) => {

      const res = await axios.post(`/api/google/login`,{token: googleData.tokenId})
      const data = res.data
      console.log(data)
      // store returned user somehow

    }

    handleInputChange = event => {
      const value = event.target.value
      const name = event.target.name

      this.setState({
       [name]: value
     });

    }


    sendLogin = async event => {
      event.preventDefault();
      const res = await axios.post(`/api/login`,{email: this.state.email,password:this.state.password})
      localStorage.setItem("loggedInStatus", res.data.status)
      console.log("res",res)
      Router.push(
      {
        pathname: '/dashboard'
      })
      

    }
    
      render() {
  
        return (
          <div>
            <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
            buttonText="Login"
            onSuccess={this.responseGoogle}
            // onFailure={this.responseGoogle}
            cookiePolicy={'single_host_origin'}
            />
            <form onSubmit={this.sendLogin}>
              <label>
                  email:
                  <input type="text" name="email" onChange={this.handleInputChange}/>
              </label>
              <label>
                  password:
                  <input type="text" name="password" onChange={this.handleInputChange}/>
              </label>
              <input type="submit" value="Submit" />
            </form>
          </div>



        )
      }
    }