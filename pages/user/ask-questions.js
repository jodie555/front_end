import React from "react";
import axios from "../../util/api"
import Router from 'next/router'

class SupplierList extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      result:""
    }
  }

  componentDidMount = async () => {
    const result = await axios.get(`/api/requests/user/status?statusNum=1&&statusNum=2&&statusNum=3`).catch(e=>{return e})
    console.log("result",result)
    

    this.setState({result:result})
    
  }

  enterRoom = async event => {


    const result = await axios.get(`/api/chatroom/` + event.target.name)
    console.log("new result",result.data)

    Router.push(
    {
      pathname: '/openChatRoom',
      query: { conversationSID: result.data}
    }
      ,'openChatRoom'
    )
  }




  render(){
    const { result} = this.state;



    console.log("result",this.state.result)
    let resultList = ""
    if (result.data){
      resultList = result.data.map((item) => <div>{item._id}</div>)
    }

    return (
      <div>
        aaaaaa
        {/* {resultList} */}
        {result.data && result.data.length > 0 && result.data.map((item)=>
                    <div>
                    {item.status}
                    <button onClick={this.enterRoom} name={item.chatroomId}>{item._id}</button>
                  </div>
      )}

      </div>
    );
  }
  
}




export default SupplierList;