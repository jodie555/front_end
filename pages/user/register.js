import React from "react";
import FormLayout from "../../components/FormLayout"
// import Button from "../components/FormLayout/Button"
// import TextField from "../components/FormLayout/TextField"

import { Typography, Paper, Link, Grid, Box, FormControlLabel, Checkbox } from "@material-ui/core";
import { GoogleLogin } from 'react-google-login';

import axios from '../../util/api'

const FormValidators = require("../../functions/validation/login/validate");
const validateLoginForm = FormValidators.validateLoginForm;
const zxcvbn = require("zxcvbn");
import Router from 'next/router'
import setLocalStorage from "../../functions/localStorage/localStorage.js";
import routeToDashboard from "../../functions/routings/defaultRoute.js"
import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import theme from "../../styles/theme"
import Header from "../../components/Header/Header.js";
import CssBaseline from "@material-ui/core/CssBaseline";
import styles from '../../components/FormLayout/FormLayout.module.css'

class Login extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      errors: {},
      user: {
        email: "",
        password: "",

      },
      btnTxt: "show",
      type: "password",

      score: "0",
      newfile: null
    };

    // this.handleChange = this.handleChange.bind(this);
    // this.submitLogin = this.submitLogin.bind(this);
    // this.validateForm = this.validateForm.bind(this);
    // this.pwHandleChange = this.pwHandleChange.bind(this);
  }


// this is about normal registration (without using google or facebook)

//   async submitLogin(user) {
//     var body = { password: user.pw, email: user.email };
//     // var body = { password: "", email: user.email};

//     const result = await axios
//       .post("/api/login/user", body)
//       .then((res) => {
//         return res;
//       })
//       .catch((err) => {
//         return err;
//       });

    
//     if (result.data) {     
//       setLocalStorage(result);
//       // console.log(result)
//       // localStorage.setItem('isSupplier', result.data.isSupplier && result.data.verifiedSupplierStatus);
//       // localStorage.setItem('userId', result.data_id);  
//       console.log(result.data._id);      
//       routeToDashboard(1);
//     } else {
//       console.log("result",result)

//       console.log("result",result.type)
//       this.setState({
//         errors: {message : "cannot find your account"},
//       });
//     }
//   }


//   handleChange(event) {
//     const name = event.target.name;
//     const user = this.state.user;

//     if(event.target.files){

//       const {name,files} = event.target
//       user[name] = files[0]
      

//     }else{
//       user[name] = event.target.value;
//     }

//     this.setState({
//       user
//     });
//   }


//   pwHandleChange(event) {
//     const name = event.target.name;
//     const user = this.state.user;
//     user[name] = event.target.value;

//     this.setState({
//       user
//     });

//     if (event.target.value === "") {
//       this.setState(state =>
//         Object.assign({}, state, {
//           score: "null"
//         })
//       );
//     } else {
//       var pw = zxcvbn(event.target.value);
//       this.setState(state =>
//         Object.assign({}, state, {
//           score: pw.score + 1
//         })
//       );
//     }
//   }

//   validateForm(event) {
//     event.preventDefault();
//     var payload = validateLoginForm(this.state.user);
//     if (payload.success) {
//       this.setState({
//         errors: {}
//       });
//       var user = {
//         pw: this.state.user.password,
//         email: this.state.user.email,

//       };
//       this.submitLogin(user);
//     } else {
//       const errors = payload.errors;
//       this.setState({
//         errors
//       });
//     }
//   }


  responseGoogle = async (googleData ) => {
    const res = await axios.post(`/api/google/register`,{token: googleData.tokenId})

    if(res.data.status){

      setLocalStorage(res)
      Router.push(
        {
          pathname: '/user/dashboard',
        //   query: {
        //     requestContentState: 1, //go to the ongoing requests tab
        //   },
        }
          ,'/user/dashboard'
        )
    }else{
        this.setState({
          errors: result.response.data
        });
    }

  }

  render(){
    const { user,errors,type} = this.state;
    console.log("errors message",errors.message)

    return (
      <div>
        <Header />
          <FormLayout 
          title="Sign up"
          errors={errors} 
          stylesClassName={styles.loginBox}
          >
          <div> 
            <GoogleLogin
              clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
              buttonText="Sign up"
              onSuccess={this.responseGoogle}
              // onFailure={this.responseGoogle}
              cookiePolicy={"single_host_origin"}
            />
          </div> 
          {/* <TextField
            required
            id="email"
            label="Email Address"
            name="email"
            label="email"
            value={user.email}
            onChange={this.handleChange}
            error={errors.email}
            helperText={errors.email}
            autoComplete="email"
            autoFocus
          />
          <TextField
            required
            type={type}
            id="password"
            name="password"
            label="password"
            value={user.password}
            onChange={this.pwHandleChange}
            error={errors.password}
            helperText={errors.password}
            autoComplete="current-password"
          />
          <Button 
            content="submit" 
            onSubmit = {this.validateForm}
            /> */}
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="/login" variant="body2">
                Already have an account? Log in
              </Link>
            </Grid>
          </Grid>
          </FormLayout>

        <div style={{ float:"left", clear: "both" }}
            ref={(el) => { this.messagesEnd = el; }}>
        </div>
      </div>

    );
  }
  
}


export default Login;