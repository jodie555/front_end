
import React, { useState, useEffect } from 'react';
import openSocket, { Socket } from 'socket.io-client'
import RequestCardSupplierInfo from '../../components/Card/RequestCardSupplierInfo/RequestCardSupplierInfo'
import DashboardContainer from "../../components/Dashboard/DashboardContainer/DashboardContainer"
import Paper from "@material-ui/core/Paper";
import TextField from '@material-ui/core/TextField';
import getSuppliersInfo from '../../functions/getAllSuppliers/getAllSuppliers.js';
import LoadingComponent from '../../components/Loading/Loading'




function userDashboard() {




    const [filteredUni, setFilteredUni] = useState("");

    const [filteredSub, setFilteredSub] = useState("");
    const [suppliers, setSuppliers] = useState([]);
    const [allSuppliers, setAllSuppliers] = useState([]);
    const [isSupplier, setIsSupplier] = useState('{"Dummy":"Dummy"}')
    const [loading, setLoading] = useState(true)


    useEffect(() => {
      async function getSuppliersData(){

          const suppliersArray = await getSuppliersInfo(); //TODO
          setSuppliers(suppliersArray);
          setAllSuppliers(suppliersArray);
          setLoading(false)
      }
      getSuppliersData();
      setIsSupplier(JSON.parse(localStorage.getItem("isSupplier")))
    }, []);

    useEffect(() => {
      var filteredSuppliers = [];

      for (const supplier of allSuppliers){
          let supplierUniStr = supplier.university.toLowerCase();
          let filteredUniStr = filteredUni.toLowerCase();

          let supplierSubStr = supplier.course.toLowerCase();
          let filteredSubStr = filteredSub.toLowerCase();

          if (supplierUniStr.includes(filteredUniStr) && supplierSubStr.includes(filteredSubStr)){
                  filteredSuppliers.push(supplier);
          }
      }
      setSuppliers(filteredSuppliers);


  }, [filteredUni, filteredSub])


    const suppliersArray = suppliers.map(supplier => {
      return (
      <RequestCardSupplierInfo 
      supplier = {supplier}
    />)
    })


    return (


            <DashboardContainer
              isUserDasboard = {true}
              isSupplier = {isSupplier}
            >
              {loading?LoadingComponent():(
                <div style={{maxHeight: 500, overflow: 'auto'}}>
                <Paper square >
                  {/* https://v4.mui.com/components/text-fields/#text-field */}
                  <TextField 
                      id="standard-search" 
                      label="Filter University" 
                      type="search"
                      value={filteredUni}
                      onChange={(e) => setFilteredUni(e.target.value)}
                      style={{ margin: "0px 22px 8px 0px" }}
                  />

                  <TextField 
                      id="standard-search" 
                      label="Filter Subject" 
                      type="search"
                      value={filteredSub}
                      onChange={(e) => setFilteredSub(e.target.value)}
                  />
                  
              </Paper>
              </div>)}
              {suppliersArray}

            </DashboardContainer>


    );
  }
  
  export default userDashboard;
  