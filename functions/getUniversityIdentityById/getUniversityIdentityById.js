// import axios from "axios";
import axiosDefault from "../../utils/axios"
const axios = axiosDefault.axiosDefault()
export default async function getUniversityIdentityById(id){
    
    let universityIdentity = await axios.get('/api/universityIdentities/'+id);
    return universityIdentity
}