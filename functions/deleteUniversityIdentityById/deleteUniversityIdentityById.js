// import axios from "axios";
import axiosDefault from "../../utils/axios"
const axios = axiosDefault.axiosDefault()
export default async function deleteUniversityIdentityById(id){
    
    let universityIdentity = await axios.delete('/api/universityIdentities/'+id);
    return universityIdentity
}