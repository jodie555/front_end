// import axios from "axios";
import axiosDefault from "../../utils/axios"
const axios = axiosDefault.axiosDefault()
export default async function getuserInfo(){
    
    let userInfo = await axios.get('/api/user');
    return userInfo
}