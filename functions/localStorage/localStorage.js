export default async function setLocalStorage(response) {
  localStorage.setItem(
    "isSupplier",
    response.data.isSupplier 
  );
  localStorage.setItem("userId", response.data._id);
}
