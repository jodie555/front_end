import Router from "next/router";
import axios from "../../util/api";

export default async function logOut(){
    const result = await axios.delete(`/api/logout`);
    console.log("result", result);
    if (result.data.status === true) {
      Router.push({
        pathname: "/login",
      });
    }
}