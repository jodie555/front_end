import Router from "next/router";

//Helper function to route to User Dashboard with a content state parameter:
//0:new requests tab, 1: ongoing requests tab, 2: finished requests tab
export default async function routeToDashboard(state) { 
  Router.push(
    {
      pathname: "/user/dashboard",
      query: {
        requestContentState: state, 
      },
    },
    "/user/dashboard"
  );
}
