const validator = require("validator");


const validateUniversityIdentity = payload => {
  const errors = {};
  let message = "";
  let isFormValid = true;


  if (
    !payload ||
    typeof payload.university !== "string" ||
    payload.university.trim().length === 0
  ) {
    isFormValid = false;
    errors.university = "Please provide a correct university .";
  }


  if (
    !payload ||
    typeof payload.subject !== "string" ||
    payload.subject.trim().length === 0
  ) {
    isFormValid = false;
    errors.subject = "Please provide a correct subject.";
  }

  if (
    !payload ||
    typeof payload.course !== "string" ||
    payload.course.trim().length === 0
  ) {
    isFormValid = false;
    errors.course = "Please provide a correct course.";
  }

  if (
    !payload ||
    typeof payload.verificationEmail !== "string" ||
    !validator.isEmail(payload.verificationEmail)
  ) {
    isFormValid = false;
    errors.verificationEmail = "Please provide a correct university email address.";
  }

  if (
    !payload ||
    typeof payload.entryYear !== "string" ||
    payload.entryYear.length != 4
  ) {
    isFormValid = false;
    errors.entryYear = "Please provide a correct entryYear.";
  }

  if (!isFormValid) {
    message = "Check the form for errors.";
  }

  return {
    success: isFormValid,
    message,
    errors
  };
};


module.exports = {

  validateUniversityIdentity: validateUniversityIdentity
};
