const validator = require("validator");

const validateAddIdentity = payload => {
    const errors = {};
    let message = "";
    let isFormValid = true;
  
    if (
      !payload ||
      typeof payload.university !== "string" ||
      payload.university.trim().length === 0
    ) {
      isFormValid = false;
      errors.university = "Please provide a correct university .";
    }
  
  
    if (
      !payload ||
      typeof payload.email !== "string" ||
      !validator.isEmail(payload.email)
    ) {
      isFormValid = false;
      errors.email = "Please provide a correct university email address.";
    }
  
    if (
      !payload ||
      typeof payload.subject !== "string" ||
      payload.subject.trim().length === 0
    ) {
      isFormValid = false;
      errors.subject = "Please provide a correct university major.";
    }
  
    if (!isFormValid) {
      message = "Check the form for errors.";
    }
  
    return {
      success: isFormValid,
      message,
      errors
    };
  };

  module.exports = {
    validateAddIdentity: validateAddIdentity
  };