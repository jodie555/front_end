const validator = require("validator");

const validateSignUpForm = payload => {
  const errors = {};
  let message = "";
  let isFormValid = true;

  if (
    !payload ||
    typeof payload.email !== "string" ||
    !validator.isEmail(payload.email)
  ) {
    isFormValid = false;
    errors.email = "Please provide a correct email address.";
  }



  if (
    !payload ||
    typeof payload.password !== "string" ||
    payload.password.trim().length < 8
  ) {
    isFormValid = false;
    errors.password = "Password must have at least 8 characters.";
  }

  if (!payload || payload.pwconfirm !== payload.password) {
    isFormValid = false;
    errors.pwconfirm = "Password confirmation doesn't match.";
  }

  if (!isFormValid) {
    message = "Check the form for errors.";
  }

  return {
    success: isFormValid,
    message,
    errors
  };
};


const validateSignUpFormSupplier = payload => {
  const errors = {};
  let message = "";
  let isFormValid = true;

  if (
    !payload ||
    typeof payload.email !== "string" ||
    !validator.isEmail(payload.email)
  ) {
    isFormValid = false;
    errors.email = "Please provide a correct email address.";
  }


  if (
    !payload ||
    typeof payload.universityEmail !== "string" ||
    !validator.isEmail(payload.universityEmail)
  ) {
    isFormValid = false;
    errors.universityEmail = "Please provide a correct university email address.";
  }

  console.log("typeof payload.universityMajor",typeof payload.universityMajor)
  if (
    !payload ||
    typeof payload.universityMajor !== "string" ||
    payload.universityMajor.trim().length === 0
  ) {
    isFormValid = false;
    errors.universityMajor = "Please provide a correct university major.";
  }
  

  if (
    !payload ||
    typeof payload.password !== "string" ||
    payload.password.trim().length < 8
  ) {
    isFormValid = false;
    errors.password = "Password must have at least 8 characters.";
  }

  if (!payload || payload.pwconfirm !== payload.password) {
    isFormValid = false;
    errors.pwconfirm = "Password confirmation doesn't match.";
  }

  if (!isFormValid) {
    message = "Check the form for errors.";
  }

  return {
    success: isFormValid,
    message,
    errors
  };
};


const validateSignUpFormSupplierOther = payload => {
  const errors = {};
  let message = "";
  let isFormValid = true;


  if (
    !payload ||
    typeof payload.university !== "string" ||
    payload.university.trim().length === 0
  ) {
    isFormValid = false;
    errors.university = "Please provide a correct university .";
  }


  if (
    !payload ||
    typeof payload.universityEmail !== "string" ||
    !validator.isEmail(payload.universityEmail)
  ) {
    isFormValid = false;
    errors.universityEmail = "Please provide a correct university email address.";
  }

  if (
    !payload ||
    typeof payload.universityMajor !== "string" ||
    payload.universityMajor.trim().length === 0
  ) {
    isFormValid = false;
    errors.universityMajor = "Please provide a correct university major.";
  }

  if (!isFormValid) {
    message = "Check the form for errors.";
  }

  return {
    success: isFormValid,
    message,
    errors
  };
};


module.exports = {
  validateSignUpForm: validateSignUpForm,
  validateSignUpFormSupplier:validateSignUpFormSupplier,
  validateSignUpFormSupplierOther: validateSignUpFormSupplierOther
};
