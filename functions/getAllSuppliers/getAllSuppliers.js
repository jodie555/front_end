// import axios from "axios";
import axiosDefault from "../../utils/axios"
const axios = axiosDefault.axiosDefault()
export default async function getSuppliersInfo(){
    var results = []
    
    let supplierResults = await axios.get('/api/suppliers');
    console.log("supplierResults",supplierResults)

    for (const supplierRawData of supplierResults.data){
        let supplierData = {
            "supplierId": supplierRawData._id,
            "firstName": supplierRawData.firstName,
            "university": supplierRawData.universityIdentities[0].university.name,
            "course": supplierRawData.universityIdentities[0].course.name
        }
        results.push(supplierData)
    }
    console.log(results)
    return results
}