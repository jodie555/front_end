
import axios from "../../../util/api";
// error: create a conversation room with name that been used
// a route to check whether the conversation room is created 

export async function checkChatRoomExistByClient(conversationsClient,uniqueName){


  const createdOrNot =  await conversationsClient.getConversationByUniqueName(uniqueName)
  .then(conversation => {
    // console.log("getConversationBySid",conversation)
    return true
  })
  .catch(error => {
    // console.log("error",error)
    return false
  })

  return createdOrNot
}
  
export async function checkChatRoomExistBySession(conversationsClient){
    const result = await axios.get(`/api/session/roomName`);
    const roomName = result.data.roomName
    
    console.log("roomName",roomName)
    if(roomName){
    
      const createdOrNot =  await conversationsClient.getConversationByUniqueName(roomName)
      .then(conversation => {
        console.log("getConversationBySid",conversation)
        return true
      })
      .catch(error => {
        // console.log("error",error)
        return false
      })
  
      return createdOrNot
    }





}
  