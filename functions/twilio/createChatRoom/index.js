import axios from "../../../util/api";


// error: create a conversation room with name that been used
// a route to check whether the conversation room is created 

export async function createChatRoomByUniqueName(uniqueName){
  //create a room by the input name
  const createdConversation = await axios.post(`/api/create_conversation/by_body`,{"uniqueName":uniqueName});

  return createdConversation

}
  


export async function createChatRoomBySession(){
  //create a room by the input name
  const createdConversation = await axios.post(`/api/create_conversation/by_session`);

  return createdConversation

}