import axios from "../../../util/api";
// error: create a conversation room with name that been used
// a route to check whether the conversation room is created 

export async function postRoomName(roomName){


    const result = await axios.post(`/api/session/roomName`,{roomName:roomName});

    return result
}
  