import axios from "../../../util/api";

// error: create a conversation room with name that been used
// a route to check whether the conversation room is created 

export async function joinChatRoomByBodyIdentity(identity,conversationSid){
  //create a room by the input name
  const addedParticipant = await axios.post(`/api/add_participant/body_identity`,{"identity":identity,"conversationSid":conversationSid});

  return addedParticipant

}
  

export async function joinChatRoomBySessionIdentity(conversationSid){
  //create a room by the input name
  const addedParticipant = await axios.post(`/api/add_participant/session_identity`,{"conversationSid":conversationSid});

  return addedParticipant

}