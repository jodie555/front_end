// import axios from "axios";
import axiosDefault from "../../utils/axios"
const axios = axiosDefault.axiosDefault()
export default async function getUsersInfo(){
    var results = []
    
    let requestResults = await axios.get('/api/requests/supplier');

    for (const request of requestResults.data){

        let userResult = await axios.get('/api/user/'+request.userId)

        let userData = {
            "userId": userResult.data.data.userId,
            "userName": userResult.data.data.username,
            "chatroomId":request.chatroomId,
            "requestId":request._id
        }
        results.push(userData)
    }
    // console.log(results)
    return results
}