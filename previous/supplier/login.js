import React, { Component } from "react";
import axios from "../../util/api";
import Router from 'next/router'
import LoginContainer from '../../components/Login/LoginContainer'
// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import { MuiThemeProvider, createTheme } from '@material-ui/core/styles'
import { Paper, ThemeProvider, Box } from '@material-ui/core';

// const styles = theme => ({
//   root:{
//     backgroundColor:"red"
//   }
// })


class ChoiceClass extends React.Component {

    constructor(props){
      super(props);
      this.state = {
        name:"",
        password:null,
        username:null,
        avatar:null,
        categoryCollection:[]


      }
    }

  
    async componentDidMount() {


    }

    async submitLogin(user) {
      var body = { password: user.pw, email: user.email};
      // var body = { password: "", email: user.email};
  
      const result = await axios.post("/api/login/supplier", body)
        .then(res => {
          return res
        })
        .catch(err => {
          return err
        });
      
      if(result.data){
        Router.push(
          {
            pathname: '/dashboard'
          }
            ,'/dashboard'
          )
      }else{
          this.setState({
            errors: result.response.data
          });
      }
  
    }

    
      render() {
        const { classes, theme } = this.props
      
  
        return (

          <LoginContainer 
            submitLogin= {this.submitLogin}
          />

        )
      }
    }

export default ChoiceClass