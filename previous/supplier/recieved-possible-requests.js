import React from "react";
import axios from "../../util/api"
import Router from 'next/router'

class SupplierList extends React.Component {
  
  constructor(props) {
    super(props);

    this.state = {
      requests:[]
    }
  }

  componentDidMount = async () => {
    const data = await axios.get(`/api/requests/supplier/possible-requests`).catch(e=>{return e})
    console.log("result",data)
    

    this.setState({requests:data.data})
    
  }

  acceptRequest = async event => {
    console.log(event.target.name)
    // const result = await axios.post(`/api/create_conversation/full`,{supplierId:event.target.name})
    // const result = await axios.get(`/api/chatrooms/`+event.target.name)
    const result = await axios.patch(`/api/requests/`+event.target.name +`/supplier/add-to-accepted-list`)


    console.log("result",result)

    // Router.push(
    //   // '/chat?room=${room}&email=${email}',
    // {
    //   pathname: '/openChatRoom',
    //   query: { conversationSID: result.data.conversationSID }
    // }
    //   ,'openChatRoom'
    // )
  }




  render(){
    const { requests} = this.state;


    return (
      <div>
        received requests
        click the button to accept the request
        {/* {resultList} */}
        {requests && requests.length > 0 && requests.map((request)=>
          <div>
            <div>university:{request.university}</div>
            <div>subject:{request.subject}</div>
            
            <button onClick={this.acceptRequest} name={request._id}>{request._id}</button>
          </div>
        )}

      </div>
    );
  }
  
}




export default SupplierList;