import React, { Component } from "react";
import axios from "../../util/api";
import Router from "next/router";
import PropTypes from "prop-types";
import SignUpContainer from "../../components/SignUp/SignUpContainer";
import NavbarDrawer from "../../components/NavbarDrawer/NavbarDrawer.js";
import {
  createTheme,
  withStyles,
  makeStyles,
} from "@material-ui/core/styles";
import {
  Paper,
  ThemeProvider,
  Box,
  Tab,
  Tabs,
  Container,
  Grid,
  Typography,
} from "@material-ui/core";
import { validateSignUpFormSupplierOther } from "../../functions/validation/signup/validate";
import CssBaseline from "@material-ui/core/CssBaseline";
import Copyright from "../../components/Copyright/Copyright.js";

const styles = (theme) => ({
  paper: {
    marginTop: theme.spacing(9),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  form: {
    // width:'100%',
    margin: "0.8rem",
  },
  title: {
    color: "#2ecc71",
  },
  inputLabel: {
    [theme.breakpoints.down("sm")]: {},
  },
  dropDown: {
    [theme.breakpoints.down("sm")]: {
      minWidth: "300px",
      marginBottom: "2.5rem",
    },
    [theme.breakpoints.up("lg")]: {
      minWidth: "300px",
      marginBottom: "2.5rem",
    },
  },
  descriptionField: {
    [theme.breakpoints.down("sm")]: {
      minWidth: "300px",
      marginBottom: "3.5rem",
    },
    [theme.breakpoints.up("lg")]: {
      minWidth: "500px",
      marginBottom: "2.5rem",
    },
  },
  submitButton: {
    left: "10%",
  },
});

class ChoiceClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      password: null,
      username: null,
      avatar: null,
      categoryCollection: [],
      universityOptions: [],
      universityMajorOptions: [],
      errors: {},
      formState: 0,
      token: null,
      supplierGoogleLoggedIn: false,
    };
  }

  theme = createTheme({
    palette: {
      primary: {
        light: "#2ecc71",
        main: "#2ecc71",
        dark: "#2ecc71",
        contrastText: "#fff",
      },
      secondary: {
        light: "#ff7961",
        main: "#f44336",
        dark: "#ba000d",
        contrastText: "#000",
      },
    },
  });

  useStyles = makeStyles((theme) => ({
    root: {
      marginTop: theme.spacing(10),
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
  }));

  async componentDidMount() {
    const universityOptions = await axios
      .get(`/api/universities`)
      .catch((e) => {
        return e;
      });
    const universityMajorOptions = await axios
      .get(`/api/subjects`)
      .catch((e) => {
        return e;
      });

    this.setState({
      universityOptions: universityOptions.data,
      universityMajorOptions: universityMajorOptions.data,
    });
  }

  validateMethodGoogleLoggedInSupplier = (user) => {
    var payload = validateSignUpFormSupplierOther(user);

    return payload;
  };

  handleChange = (event, formStateClicked) => {
    this.setState({
      formState: formStateClicked,
    });
  };

  routeToNextPage = (messageStatusNum) => {
    console.log("messageStatusNum", messageStatusNum);
    Router.push({
      pathname: "/message",
      query: { messageStatus: messageStatusNum }, //"00002"
    });
  };

  submitSignupGoogleLoggedInSupplier = async (user) => {
    var bodyFormData = new FormData();
    user.avatar && bodyFormData.append("avatar", user.avatar);
    user.university && bodyFormData.append("university", user.university);
    user.universityMajor &&
      bodyFormData.append("universityMajor", user.universityMajor);
    user.universityEmail &&
      bodyFormData.append("universityEmail", user.universityEmail);
    this.state.token && bodyFormData.append("token", this.state.token);
    // console.log("token",this.state.token)

    axios
      .post("/api/google/register/supplier/add-additional-info", bodyFormData)
      .then((res) => {
        console.log("res.data", res.data);
        if (res.data.success === true) {
          routeToNextPage(res.data.messageStatus);
        } else {
          this.setState({
            errors: { message: res.data.message },
          });
        }
      })
      .catch((err) => {
        // console.log("Sign up data submit error: ", err);

        this.setState({
          errors: err.response.data,
        });
      });
  };

  render() {
    const { errors } = this.state;
    const { classes } = this.props;

    return (
      <div style={{ display: "flex" }}>
        <ThemeProvider theme={this.theme}>
          <CssBaseline />
          <NavbarDrawer />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                {/* Requests */}
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <form
                      className={classes.form}
                      encType="multipart/form-data"
                    >
                      <Typography
                        className={classes.title}
                        variant="h4"
                        align="center"
                        component="h1"
                        gutterBottom
                      >
                        Mentor Registration Form
                      </Typography>
                      <SignUpContainer
                        forSupplier
                        forOtherSignUpSupplier
                        // universityOptions = {[{_id:"4f4f4",name:"cece"},{_id:"4f43",name:"c4f4"}]}
                        universityOptions={this.state.universityOptions}
                        universityMajorOptions={
                          this.state.universityMajorOptions
                        }
                        token={this.state.token}
                        submitSignup={this.submitSignupGoogleLoggedInSupplier}
                        validateMethod={
                          this.validateMethodGoogleLoggedInSupplier
                        }
                        errors={errors}
                      />
                    </form>
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </ThemeProvider>
      </div>
    );
  }
}

ChoiceClass.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ChoiceClass);
