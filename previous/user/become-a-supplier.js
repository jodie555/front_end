import React, { Component } from "react";
import Header from "../../components/Header/Header.js";
// import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import AddIdentityForm from "../../components/AddIdentityForm/AddIdentityForm.js";

// const styles = theme => ({
//   root:{
//     backgroundColor:"red"
//   }
// })

class BecomeASupplier extends React.Component {
  constructor(props) {
    super(props);

  }

  theme = createTheme({
    palette: {
      primary: {
        light: "#2ecc71",
        main: "#2ecc71",
        dark: "#2ecc71",
        contrastText: "#fff",
      },
      secondary: {
        light: "#ff7961",
        main: "#f44336",
        dark: "#ba000d",
        contrastText: "#000",
      },
    },
  });

  async componentDidMount() {}

  render() {
    const { classes, theme } = this.props;

    return (
      <ThemeProvider theme={this.theme}>
        <CssBaseline />
        <Header />
        <AddIdentityForm/>
      </ThemeProvider>
    );
  }
}

export default BecomeASupplier;
