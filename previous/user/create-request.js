import React from "react";
import axios from "../../util/api";
import Router from "next/router";
import CssBaseline from "@material-ui/core/CssBaseline";
import NavbarDrawer from "../../components/NavbarDrawer/NavbarDrawer.js";
import CreateRequestForm from "../../components/CreateRequestForm/CreateRequestForm.js";
import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import routeToDashboard from "../../functions/routings/defaultRoute.js";

class CreateRequest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      university: "",
      subject: "",
      description: "",
    };

    this.theme = createTheme({
      palette: {
        primary: {
          light: "#2ecc71",
          main: "#2ecc71",
          dark: "#2ecc71",
          contrastText: "#fff",
        },
        secondary: {
          light: "#ff7961",
          main: "#f44336",
          dark: "#ba000d",
          contrastText: "#000",
        },
      },
    });
  }

  componentDidMount = async () => {};

  // componentDidUpdate = async () => {
  //   let universitiesData = await axios.get("http://localhost:3000/api/universities")
  //   this.setState({universities:universitiesData.data});

  //   let subjectsData = await axios.get("http://localhost:3000/api/subjects")
  //   this.setState({subjects:subjectsData.data});
  // }

  handleInputChange = (event) => {
    const value = event.target.value;
    const name = event.target.name;
    console.log("a", name);
    console.log("b", value);
    this.setState({
      [name]: value,
    });
  };

  onSubmit = async (event) => {
    event.preventDefault();
    console.log(event.target);
    const result = await axios.post(`/api/requests/user`, {
      universityId: this.state.university,
      subjectId: this.state.subject,
      description: this.state.description,
    });

    routeToDashboard(0);
  };

  validate = (values) => {
    const errors = {};
    if (!values.descriptions) {
      errors.descriptions = "Required";
    }
    return errors;
  };

  render() {
    return (
      <div style={{ display: "flex" }}>
        <ThemeProvider theme={this.theme}>
          <CssBaseline />
          <NavbarDrawer />
          <CreateRequestForm
            subject={this.state.subject}
            university={this.state.university}
            handleInputChange={this.handleInputChange}
            onSubmit={this.onSubmit}
          />
        </ThemeProvider>
      </div>
    );
  }
}

export default CreateRequest;
