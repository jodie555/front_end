import React, {useState, useEffect} from "react";
// import SupplierCard from "../components/SupplierCard/SupplierCard.js"
// import getSupplierCardInfo from '../functions/supplier/getSupplierCardInfo/index.js';
import Header from '../../components/Header/Header.js'
// import RequestsDynamicContent from '../../components/RequestsDynamicContent/RequestsDynamicContent.js'
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import { useRouter } from 'next/router'
import axios from "../../util/api";

const theme = createTheme({
  palette: {
    primary: {
      light: '#2ecc71',
      main: '#2ecc71',
      dark: '#2ecc71',
      contrastText: '#fff',
    },
    secondary: {
      light: '#ff7961',
      main: '#f44336',
      dark: '#ba000d',
      contrastText: '#000',
    },
  },
});

function Verification(props){

  const [message, setMessage] = useState("")

  useEffect(()=>{
    async function fetchMyAPI() {
      const {token,email} = router.query
      let result = await axios.post("/api/useremailvalidation",{token,email})
      console.log("router.query",token)
      console.log("result",result.data.message)
      setMessage(result.data.message)
    }
    
    fetchMyAPI()
  })
  const router = useRouter()

  return(
      <div>
            <Header />
            {message}
      </div>
  );
}

export default Verification;