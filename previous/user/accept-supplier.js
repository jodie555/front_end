import React from "react";
import PropTypes from "prop-types";
import axios from "../../util/api";
import Router from "next/router";
import { withRouter } from "next/router";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { Column, Row, Item } from "@mui-treasury/components/flex";
import Link from "@material-ui/core/Link";
import Divider from "@material-ui/core/Divider";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Copyright from "../../components/Copyright/Copyright.js";
import CssBaseline from "@material-ui/core/CssBaseline";
import NavbarDrawer from "../../components/NavbarDrawer/NavbarDrawer.js";
import CreateRequestForm from "../../components/CreateRequestForm/CreateRequestForm.js";
import { createTheme, ThemeProvider } from "@material-ui/core/styles";

import AcceptSupplierCard from "../../components/SupplierCard/AcceptSupplierCard/AcceptSupplierCard.js";

// //////////////////
//  Client may change the requestId or supplierId before they send the information to create a chatroom
// ///////////////////////

const styles = (theme) => ({
  card: {
    width: "100%",
    borderRadius: 16,
    boxShadow: "0 8px 16px 0 #BDC9D7",
    overflow: "hidden",
  },
  header: {
    fontFamily: "Barlow, san-serif",
    backgroundColor: "#fff",
  },
  headline: {
    color: "#122740",
    fontSize: "1.25rem",
    fontWeight: 600,
  },
  link: {
    color: "#2281bb",
    padding: "0 0.25rem",
    fontSize: "0.875rem",
  },
  actions: {
    color: "#BDC9D7",
  },
  divider: {
    backgroundColor: "#d9e2ee",
    margin: "0 20px",
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  fixedHeight: {
    height: 240,
  },
});

class SupplierList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      request: {},
      acceptedSuppliers: [],
    };
  }

  theme = createTheme({
    palette: {
      primary: {
        light: '#2ecc71',
        main: '#2ecc71',
        dark: '#2ecc71',
        contrastText: '#fff',
      },
      secondary: {
        light: '#ff7961',
        main: '#f44336',
        dark: '#ba000d',
        contrastText: '#000',
      },
    },
  });

  componentDidMount = async () => {
    if (this.props.router.query.requestId) {
      const requestId = this.props.router.query.requestId;
      const requestResult = await axios
        .get(`/api/requests/` + requestId)
        .catch((e) => {
          return e;
        });
      const suppliersResult = await axios
        .get(`/api/requests/` + requestId + `/accepted-suppliers`)
        .catch((e) => {
          return e;
        });

      console.log("suppliersResult.data",suppliersResult.data)
      this.setState({
        request: requestResult.data,
        acceptedSuppliers: suppliersResult.data,
      });
    } else {
      Router.push(
        {
          pathname: "/create-request",
        },
        "create-request"
      );
    }
  };

  createRoom = async (event) => {
    // add chosen supplier to the request
    const addSupplierResult = await axios.patch(
      `/api/requests/` + this.state.requestId + `/user/add-supplier`,
      { supplierId: event.target.name }
    );

    const result = await axios.post(`/api/create_conversation/request`, {
      requestId: this.state.requestId,
    });

    Router.push(
      {
        pathname: "/openChatRoom",
        query: { conversationSID: result.data.conversationSID },
      },
      "openChatRoom"
    );
  };

  render() {
    const { acceptedSuppliers } = this.state;
    const { classes } = this.props;

    return (
      <div style={{ display: "flex" }}>
        <ThemeProvider theme={this.theme}>
          <CssBaseline />
          <NavbarDrawer />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Paper className={classes.paper}>
                    <div>
                      <Column p={0} gap={0} className={classes.card}>
                        <Row
                          wrap
                          p={2}
                          alignItems={"baseline"}
                          className={classes.header}
                        >
                          <Item stretched className={classes.headline}>
                            Accept a Supplier
                          </Item>
                          <Item className={classes.actions}>
                            <Link className={classes.link}>Refresh</Link> •{" "}
                            <Link className={classes.link}>See all</Link>
                          </Item>
                        </Row>
                        {acceptedSuppliers &&
                          acceptedSuppliers.length > 0 &&
                          acceptedSuppliers.map((supplier) => (
                            <AcceptSupplierCard
                              imageURL={supplier.imageURL}
                              firstName={supplier.username}
                              university={supplier.university}
                              universityMajor={supplier.universityMajor}

                              // university={supplier.universityIdentities[0].university.name}
                              // universityMajor={supplier.universityIdentities[0].course.name}
                              rating={supplier.rating}
                              supplerId={supplier._id}
                              requestId={this.props.router.query.requestId}
                            />
                          ))}
                      </Column>
                    </div>
                  </Paper>
                </Grid>
              </Grid>
              <Box pt={4}>
                <Copyright />
              </Box>
            </Container>
          </main>
        </ThemeProvider>
      </div>
    );

  }
}

SupplierList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(SupplierList));

//To retain props upon refreshing
export async function getServerSideProps(context) {
  return {
    props: {}, // will be passed to the page component as props
  };
}
