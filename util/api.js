import Axios from "axios";

export default Axios.create({
    withCredentials: true,
    proxy: false,
    baseURL: process.env.BACKEND_URL,
});