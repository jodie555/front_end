import axios, { Method } from 'axios'
// module.exports = {
//     defaultPath: ()=>{
//         axios.defaults.withCredentials = true;
//         const instance = axios.create({
//             baseURL: process.env.BACKEND_URL,
//           });
//         return instance
//     }
// }

const axiosDefault = () => {
    axios.defaults.withCredentials = true;
    console.log('process.env.BACKEND_URL',process.env.BACKEND_URL)
    const instance = axios.create({
        baseURL: process.env.BACKEND_URL,
      });
    return instance
}


export default {
    axiosDefault
}