import { createTheme } from "@material-ui/core/styles";


const theme = createTheme({
  palette: {
    primary: {
      light: "#2ecc71",
      main: "#2ecc71",
      dark: "#2ecc71",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ff7961",
      main: "#f44336",
      dark: "#ba000d",
      contrastText: "#000",
    },
  },
});


export default theme