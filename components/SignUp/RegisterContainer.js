import React from "react";
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SignUpContainer from './SignUpContainer.js';


const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(10),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
}));

export default function RegisterContainer(props){
    const classes = useStyles();
    const [formState, setFormState] = React.useState(0);

    const handleChange = (event, formState) => {
        setFormState(formState);
    };

    let responseGoogle;
    

    return (
        <Paper square className={classes.root}>
            <Tabs
                value={formState}
                onChange={handleChange}
                variant="fullWidth"
                indicatorColor="primary"
                textColor="primary"
                aria-label="icon tabs sign up"
            >
                <Tab label="Sign up as User"  />
                <Tab label="Sign up as Mentor" />
            </Tabs>

            {formState === 0 &&
            <SignUpContainer
            submitSignup = {props.submitSignup}
            errors = {props.errors}
            responseGoogle = {this.responseGoogle}
            />
            }

            {formState === 1 &&
            <SignUpContainer 
            forSupplier
            // universityOptions = {[{_id:"4f4f4",name:"cece"},{_id:"4f43",name:"c4f4"}]}
            universityOptions = {universityOptions}

            universityMajorOptions = {universityMajorOptions}
            responseGoogle = {this.responseGoogle}
            submitSignup = {this.submitSignup}
            validateMethod = {this.validateMethod}
            errors = {props.errors}
            />
            }
        </Paper>
    )
}