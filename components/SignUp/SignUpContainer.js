import React, { useRef, Component } from "react";
import SignUpForm from "./SignUpForm.js";
import Route from "next/router";
const zxcvbn = require("zxcvbn");
import Router from 'next/router'

class SignUpContainer extends Component {
  constructor(props) {
    
    super(props);
    this.state = {
      errors: props.errors,
      forSupplier: this.props.forSupplier,
      forOtherSignUpSupplier: this.props.forOtherSignUpSupplier,
      user: {
        // username: "",
        email: "",
        university:"",
        universityMajor:"",
        universityEmail:"",
        password: "",
        pwconfirm: "",
        avatar:"",
        documentProof:"",
      },
      btnTxt: "show",
      type: "password",
      score: "0",
      newfile: null,
      universityOptions: props.universityOptions,
      universityMajorOptions:props.universityMajorOptions,
      token:props.token
    };
  }

  componentDidUpdate = prevProps => {
    if(prevProps.universityOptions !== this.props.universityOptions) {
      this.setState({universityOptions: this.props.universityOptions});
    }
    if(prevProps.universityMajorOptions !== this.props.universityMajorOptions) {
      this.setState({universityMajorOptions: this.props.universityMajorOptions});
    }
    if(prevProps.errors !== this.props.errors) {
      this.setState({errors: this.props.errors});
    }
  }





  handleChange= event=> {
    const name = event.target.name;
    const user = this.state.user;
    console.log(event.target.name)
    console.log( event.target.value)
    if(event.target.files){

      const {name,files} = event.target
      user[name] = files[0]
      

    }else{
      user[name] = event.target.value;
    }

    this.setState({
      user
    });
  }

  
  // handleAutocompleteChange = (event, newValue) => {
  //   const name = this.autocompleteRef.current.getAttribute("name");
  //   const user = this.state.user;
  //   console.log(event.target.name)
  //   console.log( event.target.value)
  //   if(newValue){
  //     user[name] = newValue._id;
  //   }
  //   this.setState({
  //     user
  //   });
  // }

  pwHandleChange = event => {
    const name = event.target.name;
    const user = this.state.user;
    user[name] = event.target.value;

    this.setState({
      user
    });

    if (event.target.value === "") {
      this.setState(state =>
        Object.assign({}, state, {
          score: "null"
        })
      );
    } else {
      var pw = zxcvbn(event.target.value);
      this.setState(state =>
        Object.assign({}, state, {
          score: pw.score + 1
        })
      );
    }
  }


  validateForm = event => {
    event.preventDefault();
    console.log("this.state.user",this.state.user)
    var payload = this.props.validateMethod(this.state.user);

    if (payload.success) {
      this.setState({
        errors: {}
      });
      var user = {
        pw: this.state.user.password,
        email: this.state.user.email,
        avatar: this.state.user.avatar,
        documentProof: this.state.user.documentProof,
        university: this.state.user.university,
        universityMajor: this.state.user.universityMajor,
        universityEmail: this.state.user.universityEmail
      };
      this.props.submitSignup(user);

    } else {
      const errors = payload.errors;
      this.setState({
        errors
      });
    }
  }

  pwMask = event => {
    event.preventDefault();
    this.setState(state =>
      Object.assign({}, state, {
        type: this.state.type === "password" ? "input" : "password",
        btnTxt: this.state.btnTxt === "show" ? "hide" : "show"
      })
    );
  }

  render() {

    return (
      <div>
        <SignUpForm
          onSubmit={this.validateForm}
          onChange={this.handleChange}
          onPwChange={this.pwHandleChange}
          responseGoogle = {this.props.responseGoogle}
          errors={this.state.errors}
          user={this.state.user}
          score={this.state.score}
          btnTxt={this.state.btnTxt}
          type={this.state.type}
          pwMask={this.pwMask}
          forSupplier={this.state.forSupplier}
          forOtherSignUpSupplier={this.state.forOtherSignUpSupplier}
          universityOptions={this.state.universityOptions}
          universityMajorOptions={this.state.universityMajorOptions}
          // handleAutocompleteChange={this.handleAutocompleteChange}
          // autocompleteRef={this.autocompleteRef}
        />
      </div>
    );
  }
}

module.exports = SignUpContainer;
