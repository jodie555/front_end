import React, {useRef} from "react";
// import FlatButton from "material-ui/FlatButton";
import Button from "@material-ui/core/Button";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
// import RaisedButton from "material-ui/RaisedButton";
import TextField from "@material-ui/core/TextField";
import AutocompleteCustom from "./Autocomplete.js";
import { Input } from "@material-ui/core";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Avatar from "@material-ui/core/Avatar";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Checkbox from "@material-ui/core/Checkbox";
import Divider from "./Divider.js";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import PasswordStr from "./PasswordStr";
import { makeStyles } from "@material-ui/core/styles";
// import "./style.module.css";
import styles from "./SignUp.module.css";
import Select from "../../components/Select/select";

import { GoogleLogin } from "react-google-login";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  subtitle: {
    color: "gray",
  },
  googleLogin: {
    margin: theme.spacing(2),
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  uploadButton: {
    color: "white",
    backgroundColor: "#19b5fe",
  },
}));

const SignUpForm = ({
  history,
  onSubmit,
  onChange,
  errors,
  user,
  score,
  btnTxt,
  type,
  pwMask,
  onPwChange,
  forSupplier,
  forOtherSignUpSupplier,
  universityOptions,
  universityMajorOptions,
  responseGoogle,
}) => {
  const classes = useStyles();
  
  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography variant="subtitle2" className={classes.subtitle}>
          A mentorship platform that helps you find your path
        </Typography>
        {errors.message && <p style={{ color: "red" }}>{errors.message}</p>}
        {!forOtherSignUpSupplier && (
          <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
            buttonText="Login"
            onSuccess={responseGoogle}
            // onFailure={this.responseGoogle}
            cookiePolicy={"single_host_origin"}
            className={classes.googleLogin}
          />
        )}
        {!forOtherSignUpSupplier && <Divider>OR</Divider>}
        <form className={classes.form} onSubmit={onSubmit}>
          <Grid container spacing={2}>
            {/* {!forOtherSignUpSupplier && (
              <Grid item xs={12}>
                <TextField
                  name="username"
                  label="Username"
                  value={user.username}
                  onChange={onChange}
                  error={errors.username}
                  helperText={errors.username}
                  required
                  fullWidth
                  autoFocus
                />
              </Grid>
            )} */}
            {!forOtherSignUpSupplier && (
              <Grid item xs={12}>
                <TextField
                  name="email"
                  label="Email"
                  value={user.email}
                  onChange={onChange}
                  error={errors.email}
                  helperText={errors.email}
                  required
                  fullWidth
                />
              </Grid>
            )}
            {forSupplier && (
              <Grid item xs={12}>
                <AutocompleteCustom
                  name={"university"}
                  label={"University"}
                  options={universityOptions}
                  user={user}
                  error={errors.university}
                  value={user.university}
                />           
                {/* <Select
                  name="university"
                  label="University"
                  value={user.university}
                  onChange={onChange}
                  error={errors.university}
                  helperText={errors.university}
                  options={universityOptions}
                  styles={{ backgroundColor: null }}
                /> */}
              </Grid>
            )}
            {forSupplier && (
              <Grid item xs={12}>
                <AutocompleteCustom
                  name={"universityMajor"}
                  label={"University Major"}
                  options={universityMajorOptions}
                  user={user}
                  error={errors.universityMajor}
                  value={user.universityMajor}
                />        
                {/* <Autocomplete
                  id="combo-box-demo"
                  options={universityMajorOptions}
                  getOptionLabel={(option) => option.name}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Combo box"
                      variant="outlined"
                      name="universityMajor"
                      label="University Major"
                      value={user.universityMajor}
                      onChange={onChange}
                      error={errors.universityMajor}
                      helperText={errors.universityMajor}
                      required
                    />
                  )}
                /> */}
                {/* <Select
                  name="universityMajor"
                  label="University Major"
                  value={user.universityMajor}
                  onChange={onChange}
                  error={errors.universityMajor}
                  helperText={errors.universityMajor}
                  options={universityMajorOptions}
                  required
                  fullWidth
                /> */}
              </Grid>
            )}
            {forSupplier && (
              <Grid item xs={12}>
                <TextField
                  name="universityEmail"
                  label="University Email"
                  value={user.universityEmail}
                  onChange={onChange}
                  error={errors.universityEmail}
                  helperText={errors.universityEmail}
                  required
                  fullWidth
                />
              </Grid>
            )}
            {!forOtherSignUpSupplier && (
              <Grid item xs={12} sm={6}>
                <TextField
                  type={type}
                  name="password"
                  label="Password"
                  value={user.password}
                  onChange={onPwChange}
                  error={errors.password}
                  helperText={errors.password}
                  required
                  fullWidth
                />
              </Grid>
            )}
            {!forOtherSignUpSupplier && (
              <Grid item xs={12} sm={6}>
                <TextField
                  type={type}
                  name="pwconfirm"
                  label="Confirm Password"
                  value={user.pwconfirm}
                  onChange={onChange}
                  error={errors.pwconfirm}
                  helperText={errors.pwconfirm}
                  required
                  fullWidth
                />
              </Grid>
            )}
            {forSupplier && (
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  component="label"
                  className={classes.uploadButton}
                  startIcon={<CloudUploadIcon />}
                  onChange={onChange}
                >
                  Upload Document Proof
                  <input
                    accept="image/*"
                    name="documentProof"
                    id="raised-button-file"
                    type="file"
                  />
                </Button>
                {/* <Input
                  accept="image/*"
                  name="documentProve"
                  label="Document Proof"
                  // style={{ display: 'none' }}
                  id="raised-button-file"
                  type="file"
                  color="primary"
                  onChange={onChange}
                  // value={user.avatar}
                  // error={errors.pwconfirm}
                  // helperText={errors.pwconfirm}
                /> */}
              </Grid>
            )}
            {forSupplier && (
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  component="label"
                  className={classes.uploadButton}
                  startIcon={<CloudUploadIcon />}
                  onChange={onChange}
                >
                  Upload Profile Picture
                  <input
                    accept="image/*"
                    name="avatar"
                    id="raised-button-file"
                    type="file"
                  />
                </Button>
                {/* <Input
                  accept="image/*"
                  name="avatar"
                  // style={{ display: 'none' }}
                  id="raised-button-file"
                  type="file"
                  onChange={onChange}
                  // value={user.avatar}
                  // error={errors.pwconfirm}
                  // helperText={errors.pwconfirm}
                /> */}
              </Grid>
            )}

            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="I want to receive inspiration, marketing promotions and updates via email."
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link href="#" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>

    // <div className={styles.loginBox}>
    //   <h1>Sign Up</h1>
    //   {errors.message && <p style={{ color: "red" }}>{errors.message}</p>}
    //   {!forOtherSignUpSupplier&&
    //   <GoogleLogin
    //         clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
    //         buttonText="Login"
    //         onSuccess={responseGoogle}
    //         // onFailure={this.responseGoogle}
    //         cookiePolicy={'single_host_origin'}
    //         />
    //   }
    //   <form onSubmit={onSubmit} >
    //     {!forOtherSignUpSupplier&&
    //     <TextField
    //       name="username"
    //       label="user name"
    //       value={user.username}
    //       onChange={onChange}
    //       error={errors.username}
    //       helperText={errors.username}
    //     />
    //     }
    //     {!forOtherSignUpSupplier&&
    //     <TextField
    //       name="email"
    //       label="email"
    //       value={user.email}
    //       onChange={onChange}
    //       error={errors.email}
    //       helperText={errors.email}

    //     />
    //     }
    //     {forSupplier &&
    //       <Select
    //       name="university"
    //       label="university"
    //       value={user.university}
    //       onChange={onChange}
    //       error={errors.university}
    //       helperText={errors.university}
    //       options = {universityOptions}
    //       styles={{backgroundColor:  null,}}
    //     />
    //     }

    //     {forSupplier &&
    //       <Select
    //       name="universityMajor"
    //       label="University Major"
    //       value={user.universityMajor}
    //       onChange={onChange}
    //       error={errors.universityMajor}
    //       helperText={errors.universityMajor}
    //       options = {universityMajorOptions}
    //     />
    //     }
    //     {forSupplier &&
    //       <TextField
    //       name="universityEmail"
    //       label="University Email"
    //       value={user.universityEmail}
    //       onChange={onChange}
    //       error={errors.universityEmail}
    //       helperText={errors.universityEmail}

    //     />
    //     }
    //     {!forOtherSignUpSupplier&&
    //     <TextField
    //       type={type}
    //       name="password"
    //       label="password"
    //       value={user.password}
    //       onChange={onPwChange}
    //       error={errors.password}
    //       helperText={errors.password}
    //     />
    //     }
    //     {/* <div className={styles.pwStrRow}>
    //       {score >= 1 && (
    //         <div>
    //           <PasswordStr score={score} />
    //           <Button
    //             className={styles.pwShowHideBtn}
    //             label={btnTxt} onClick={pwMask}
    //             // style={{position: 'relative', left: '50%', transform: 'translateX(-50%)'}}
    //           />
    //         </div>
    //         )}
    //     </div> */}
    //     {!forOtherSignUpSupplier&&
    //     <TextField
    //       type={type}
    //       name="pwconfirm"
    //       label="confirm password"
    //       value={user.pwconfirm}
    //       onChange={onChange}
    //       error={errors.pwconfirm}
    //       helperText={errors.pwconfirm}
    //     />
    //     }

    //     {forSupplier &&
    //     <Input
    //       accept="image/*"
    //       name = "avatar"
    //       // style={{ display: 'none' }}
    //       id="raised-button-file"
    //       type="file"
    //       onChange={onChange}
    //       // value={user.avatar}
    //       // error={errors.pwconfirm}
    //       // helperText={errors.pwconfirm}
    //     />
    //     }

    //     {forSupplier &&
    //       <Input
    //         accept="image/*"
    //         name = "documentProve"
    //         // style={{ display: 'none' }}
    //         id="raised-button-file"
    //         type="file"
    //         onChange={onChange}
    //         // value={user.avatar}
    //         // error={errors.pwconfirm}
    //         // helperText={errors.pwconfirm}
    //       />
    //     }

    //     <br />

    //     <Button variant="contained" type="submit" label="submit" color="primary">
    //     Submit
    //     </Button>
    //   </form>
    //   <p>
    //     Aleady have an account? <br />
    //     <a href="/">Log in here</a>
    //   </p>
    // </div>
  );
};

export default SignUpForm;
