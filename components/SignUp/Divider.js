import React from "react";
import { Grid, Divider as MuiDivider } from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
//ref: https://codesandbox.io/s/mui-divider-with-text-pzyjd?file=/index.js

const useStyles = makeStyles((theme) => ({
    content: {
        color: "gray",
    }
}))

const Divider = ({ children, ...props }) => {
    const classes = useStyles()
    return(
  <Grid container alignItems="center" spacing={3} {...props}>
    <Grid item xs>
      <MuiDivider />
    </Grid>
    <Grid item className={classes.content}>{children}</Grid>
    <Grid item xs>
      <MuiDivider />
    </Grid>
  </Grid>
    )
};

export default Divider;