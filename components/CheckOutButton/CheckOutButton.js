import React, {useState} from 'react';
import API from "../../util/api.js";
import {loadStripe} from '@stripe/stripe-js';
const stripePromise = loadStripe('pk_test_51HT2lEFLwR7dWnVk5DTXKTuBYvGufAcEaN9OWTvM1IlLJTZdbBwxvzn3nj2op5YuSrhRB0IAVWdlJHj765kDfKWT00i9Jl38uy');

function CheckOutButton(props) {
    
    const handleCheckOut = async (event) => {
        const stripe = await stripePromise;
        const response = API.post("/api/auth/payment/create_checkout_session");
        const session = await response;
        const result = await stripe.redirectToCheckout({
          sessionId: session.data.id,
        });
    
        if (result.error) {
          // If `redirectToCheckout` fails due to a browser or network
          // error, display the localized error message to your customer
          // using `result.error.message`.
          console.log(result.error);
        }
        console.log(result);
      };

    return (
    <button type="button" className="check-out-button" role="link" onClick={handleCheckOut}>
        Checkout
    </button>
    );
};

export default CheckOutButton;