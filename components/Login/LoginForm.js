import React from "react";
import axios from "../../util/api";
import Router from "next/router";
// import FlatButton from "material-ui/FlatButton";
import Button from "@material-ui/core/Button";
import Avatar from '@material-ui/core/Avatar';
// import RaisedButton from "material-ui/RaisedButton";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Copyright from "../Copyright/Copyright.js";
import { GoogleLogin } from 'react-google-login';
import setLocalStorage from "../../functions/localStorage/localStorage.js";

// import "./style.module.css";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignUpForm = ({ onSubmit, onChange, errors, user, type, onPwChange }) => {
  const classes = useStyles();
  console.log("email", user);

  const responseGoogle = async (googleData ) => {
    console.log("googlelogin")
    const res = await axios.post(`/api/google/login`,{token: googleData.tokenId})
    
    if(res.data){
      setLocalStorage(res)
      Router.push(
        {
          pathname: '/user/dashboard',
          query: {
            requestContentState: 1, //go to the ongoing requests tab
          },
        }
          ,'/user/dashboard'
        )
    }else{
        this.setState({
          errors: result.response.data
        });
    }

  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Log in
        </Typography>
        <form className={classes.form} onSubmit={onSubmit}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            label="email"
            value={user.email}
            onChange={onChange}
            error={errors.email}
            helperText={errors.email}
            autoComplete="email"
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            type={type}
            id="password"
            name="password"
            label="password"
            value={user.password}
            onChange={onPwChange}
            error={errors.password}
            helperText={errors.password}
            autoComplete="current-password"
          />
          {errors.message && <p style={{ color: "red" }}>{errors.message}</p>}
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
            buttonText="Login"
            onSuccess={responseGoogle}
            // onFailure={this.responseGoogle}
            cookiePolicy={"single_host_origin"}
          />
          <Button
            type="submit"
            label="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="/register" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );

  // return (
  //     <div>
  //     {errors.message && <p style={{ color: "red" }}>{errors.message}</p>}

  //     <form onSubmit={onSubmit}>

  //       <TextField
  //         name="email"
  //         label="email"
  //         value={user.email}
  //         onChange={onChange}
  //         error={errors.email}
  //         helperText={errors.email}

  //       />
  //       <TextField
  //         type={type}
  //         name="password"
  //         label="password"
  //         value={user.password}
  //         onChange={onPwChange}
  //         error={errors.password}
  //         helperText={errors.password}
  //       />

  //       <Button variant="contained" type="submit" label="submit" color="primary">
  //       Submit
  //       </Button>
  //     </form>
  //     <p>
  //       Not a member yet? <br />
  //       <a href="/">Join Now</a>
  //     </p>
  //   </div>
  // );
};

export default SignUpForm;
