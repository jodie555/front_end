import React, { Component } from "react";
import LoginForm from "./LoginForm.js";
import axios from '../../util/api';
const FormValidators = require("../../functions/validation/login/validate");
const validateLoginForm = FormValidators.validateLoginForm;
const zxcvbn = require("zxcvbn");
import Router from 'next/router'
import styles from './Login.module.css'
import { GoogleLogin } from 'react-google-login';
import setLocalStorage from "../../functions/localStorage/localStorage.js";

class LoginContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errors: {},
      user: {
        email: "",
        password: "",

      },
      btnTxt: "show",
      type: "password",
      score: "0",
      newfile: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.submitLogin = props.submitLogin.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.pwHandleChange = this.pwHandleChange.bind(this);
  }

  handleChange(event) {
    const name = event.target.name;
    const user = this.state.user;

    if(event.target.files){

      const {name,files} = event.target
      user[name] = files[0]
      

    }else{
      user[name] = event.target.value;
    }

    this.setState({
      user
    });
  }

  pwHandleChange(event) {
    const name = event.target.name;
    const user = this.state.user;
    user[name] = event.target.value;

    this.setState({
      user
    });

    if (event.target.value === "") {
      this.setState(state =>
        Object.assign({}, state, {
          score: "null"
        })
      );
    } else {
      var pw = zxcvbn(event.target.value);
      this.setState(state =>
        Object.assign({}, state, {
          score: pw.score + 1
        })
      );
    }
  }


  validateForm(event) {
    event.preventDefault();
    var payload = validateLoginForm(this.state.user);
    if (payload.success) {
      this.setState({
        errors: {}
      });
      var user = {
        pw: this.state.user.password,
        email: this.state.user.email,

      };
      this.submitLogin(user);
    } else {
      const errors = payload.errors;
      this.setState({
        errors
      });
    }
  }

  responseGoogle = async (googleData ) => {
    console.log("googlelogin")
    const res = await axios.post(`/api/google/login`,{token: googleData.tokenId})

    if(res.data.status){

      setLocalStorage(res)
      Router.push(
        {
          pathname: '/user/dashboard',
          query: {
            requestContentState: 1, //go to the ongoing requests tab
          },
        }
          ,'/user/dashboard'
        )
    }else{
        this.setState({
          errors: result.response.data
        });
    }

  }


  render() {
    return (
      <div>
          <div className={styles.loginBox}>
             
            <LoginForm
              onSubmit={this.validateForm}
              onChange={this.handleChange}
              onPwChange={this.pwHandleChange}
              errors={this.state.errors}
              user={this.state.user}
              score={this.state.score}
              btnTxt={this.state.btnTxt}
              type={this.state.type}
            />
          </div>
      </div>
    );
  }
}

module.exports = LoginContainer;
