import React, {useContext} from "react";
import {AuthContext} from '../AuthComponents/AuthContext'
import {Button
} from "@material-ui/core";
import { useRouter } from 'next/router'

const TestContext = () => {
  
    const authCtx = useContext(AuthContext)
  
  
    console.log(authCtx.isLoggedIn)
    // authCtx.login("3f3")
    const router = useRouter()
  
      return (
        <div>
            <h1>{authCtx.token}</h1>
            <Button
              onClick={() => {
                authCtx.login("3f3")
              }}
            >dddddf</Button>
        </div>
  
      );
    
  }
  
  

  export default TestContext;