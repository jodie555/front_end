import React from "react";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(11),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  message: {
    alignItems: "center",
  },
}));

const MessageContainer = ({ message }) => {
  const classes = useStyles();

  return (
    <Container maxWidth="lg" className={classes.paper}>
      <Grid container>
        <Grid item xs={3}></Grid>
        <Grid item>
          <Typography variant="h6" className={classes.message}>
            {message}
          </Typography>
        </Grid>
        <Grid item xs={3}></Grid>
      </Grid>
    </Container>
  );
};

export default MessageContainer;
