import React, {useState, useEffect} from "react";
import axios from "../../util/api";
import clsx from 'clsx';
import Router from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import { Menu, MenuItem, Toolbar, AppBar, Typography, IconButton, Button  } from "@material-ui/core"
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuIcon from '@material-ui/icons/Menu';
import routeToDashboard from "../../functions/routings/defaultRoute.js";
import handleLogOut from "../../functions/logOut/logOut.js";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
      },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
}))

export default function NavBar(props){
    const classes = useStyles();

    const handleSwitchToSupplierButton = () => {
        Router.push(
            {
                pathname: '/supplier/dashboard'
            }
        )
    }

    const handleSwitchToUserButton = () => {
        Router.push(
            {
                pathname: '/user/dashboard'
            }
        )
    }

    const handleBecomeSupplierButton = () => {
        Router.push(
            {
                pathname: '/supplier/add-identity'
            }
        )
    }

    

    const handleDashboardButton = () => {
        routeToDashboard(1);
    }

    // const handleLogOut = async () => {
    //     const result = await axios.delete(`/api/logout`);
    //     console.log("result",result)
    //     if (result.data.status === true){
    //         Router.push(
    //             {
    //                 pathname: '/'
    //             }
    //         )
    //     }
    // }

    const [anchorEl, setAnchorEl] = React.useState(null);
    const open = Boolean(anchorEl);
    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget)
    }
    const handleClose = () => {
        setAnchorEl(null);
    }

    const handleProfile = () => {
        // setAnchorEl(null);
        Router.push(
            {
                pathname: '/supplier/profile'
            }
        )
    }

    console.log("props.notShowSwitchBar",props.notShowSwitchBar)
    
    return (    
        <AppBar position="absolute" className={clsx(classes.appBar, props.open && classes.appBarShift)}>
            <Toolbar className={classes.toolbar}>
                <IconButton
                    edge="start"
                    color="inherit"
                    aria-label="open drawer"
                    onClick={props.handleDrawerOpen}
                    className={clsx(classes.menuButton, props.open && classes.menuButtonHidden)}
                >
                    <MenuIcon />
                </IconButton>
                <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                    JustAsk
                </Typography>
                {(
                    <div>
                        <Button 
                            color="inherit"
                            onClick={handleDashboardButton}
                        >
                            Dashboard
                        </Button>
                        {(props.isSupplier && props.isUserDashboard && !props.notShowSwitchBar ) && <Button 
                            color="inherit"
                            onClick={handleSwitchToSupplierButton}
                        >
                            Switch to Supplier
                        </Button>}
                        {(!props.isSupplier && props.isUserDashboard && !props.notShowSwitchBar) && <Button 
                            color="inherit"
                            onClick={handleBecomeSupplierButton}
                        >
                            Become a Supplier
                        </Button>}
                        {(!props.isUserDashboard && !props.notShowSwitchBar) && <Button 
                            color="inherit"
                            onClick={handleSwitchToUserButton}
                        >
                            Switch to User
                        </Button>}
                        <IconButton
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleMenu}
                            color="inherit"
                        >
                            <AccountCircle />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorE1={anchorEl}
                            getContentAnchorEl={null}
                            // https://github.com/mui-org/material-ui/issues/8090
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'center',
                            }}
                            open={open}
                            onClose={handleClose}
                        >
                            <MenuItem onClick={handleProfile}>Profile</MenuItem>
                            <MenuItem onClick={handleClose}>My Account</MenuItem>
                            <MenuItem onClick={handleLogOut}>Log Out</MenuItem>
                        </Menu>
                    </div>
                )}
            </Toolbar>
        </AppBar>      
    );
}
