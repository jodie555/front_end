
import React from 'react'
import { FormControl, InputLabel, Select as MuiSelect, MenuItem, FormHelperText } from '@material-ui/core';

export default function Select(props) {

    const { name, label, value,error=null, onChange, options,style } = props;

    return (
        <FormControl variant="outlined" 
            style={{...style,minWidth: 220}} 
            // {...(error && {error:true})}
        >
            <InputLabel>{label}</InputLabel>
            <MuiSelect
                label={label}
                name={name}
                value={value}
                onChange={onChange}
                
                >
                <MenuItem value="">None</MenuItem>
                {
                    options.map(
                        item => (<MenuItem 
                            // styles={{background: "#023950"}}
                            key={item._id} 
                            value={item._id}>{item.name}</MenuItem>)
                    )
                }
            </MuiSelect>
            {error && <FormHelperText>{error}</FormHelperText>}
        </FormControl>
    )
}