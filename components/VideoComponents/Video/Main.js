import styles from './App.module.css';
import React, {Component} from 'react';
import Room from './Room';
const { connect } = require('twilio-video')
// import axios from "axios";
import  axiosDefault from '../../utils/axios'
const axios = axiosDefault.axiosDefault()

class App extends Component {
  constructor(props){
      super(props)

      this.state = {
          identity: '',
          room: null, 
          supplierId: props.supplierId,
          userId: props.userId,
          requestId: props.requestId,
          videoCallId: '',
          isUser: props.isUser,
      }

      this.inputRef = React.createRef();

      this.joinRoom = this.joinRoom.bind(this);
    this.returnToLobby = this.returnToLobby.bind(this);
    this.updateIdentity = this.updateIdentity.bind(this);
    this.removePlaceholderText = this.removePlaceholderText.bind(this);
  }

  async joinRoom(){
      try {
        if(this.state.isUser){
          const response = await axios.post('/api/create_video', 
          {"identity": this.state.identity, "supplierId": this.state.supplierId, 
          "requestId": this.state.requestId});
          console.log("videotoken ",response.data.token)
          const room = await connect(response.data.token, {
              name: 'cool-room',
              audio: true,
              video: true
          });
          console.log("room",room)
          console.log(this.state.identity)
          console.log(this.state.supplierId)
          console.log(this.state.requestId)


          this.setState({room: room, videoCallId: response.data.videoCallId});
        } else {
          const response = await axios.post('/api/join_video', 
          {"identity": this.state.identity, "userId": this.state.userId, 
          "requestId": this.state.requestId});
          console.log("videotoken ",response.data.token)
          const room = await connect(response.data.token, {
              name: 'cool-room',
              audio: true,
              video: true
          });


          this.setState({room: room, videoCallId: response.data.videoCallId});
        }
          
      } catch(err){
          console.log(err);
      }
  }

  returnToLobby() {
    this.setState({ room: null });
  }

  removePlaceholderText() {
    this.inputRef.current.placeholder = '';
  }

  updateIdentity(event) {
    this.setState({
      identity: event.target.value
    });
  }

  render() {
    const disabled = this.state.identity === ''? true : false

    return (

        
        <div className={styles.app}>
        { 
            this.state.room === null
            ? <div className={styles.lobby}>
                <input 
                    value={this.state.identity}
                    onChange={this.updateIdentity}
                    ref={this.inputRef}
                    onClick={this.removePlaceholderText} 
                    placeholder="What's your name?"/>
                <button disabled={disabled} onClick={this.joinRoom}>Join Room</button>
            </div>
            : <Room returnToLobby={this.returnToLobby} room={this.state.room} videoCallId={this.state.videoCallId}/>
        }
        </div>
    );


    }
}

export default App;