// import React from 'react';
import { createContext, useContext } from "react";
import {Context} from './contextvalue'

export function MyComponent() {
  const value = useContext(Context);

  return <span>{value}</span>;
}