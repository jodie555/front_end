import React, { useRef, Component } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

class AutocompleteCustom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.name,
      label: props.label,
      options: props.options,
      user: props.user,
      error: props.error,
      value: props.value,
    };

    this.autocompleteRef = React.createRef();
  }

  handleChange = (event, newValue) => {
    const name = this.autocompleteRef.current.getAttribute("name");
    const user = this.state.user;
    console.log("state name",name);
    console.log("state value",newValue._id);
    if (newValue) {
      user[name] = newValue._id;
    }
    this.setState({
      user,
      value:newValue,
    });
    console.log("new user state", this.state.user);
  };

  render() {
    console.log(this.state.options);
    console.log(this.state.name);
    console.log(this.state.value);
    console.log(this.state.label);
    console.log(this.state.error)
    return (
      <div>
        <Autocomplete
          id="combo-box-demo"
          ref={this.autocompleteRef}
          options={this.state.options}
          name={this.state.name}
          value={this.state.value}
          onChange={this.handleChange}
          getOptionLabel={(option) => option.name}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Combo box"
              variant="outlined"
              label={this.state.label}
              error={this.state.error}
              helperText={this.state.error}
              required
            />
          )}
        />
      </div>
    );
  }
}

module.exports = AutocompleteCustom;
