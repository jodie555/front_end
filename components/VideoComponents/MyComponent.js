function MyComponent() {
    const value = useContext(Context);
  
    return <span>{value}</span>;
  }