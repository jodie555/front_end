import React, {useState} from 'react';
import {Button
} from "@material-ui/core";
import API from "../../util/api.js";
import Router from 'next/router';
import {loadStripe} from '@stripe/stripe-js';
// import {loadStripe} from '@stripe/react-stripe-js';

import axios from "../../util/api"

const stripePromise = loadStripe('pk_test_51HT2lEFLwR7dWnVk5DTXKTuBYvGufAcEaN9OWTvM1IlLJTZdbBwxvzn3nj2op5YuSrhRB0IAVWdlJHj765kDfKWT00i9Jl38uy')

function AcceptSupplierButton(props) {
    const [isDisabled, setIsDisabled] = useState(false);

    // const enableBeginChat = async (event) => {
    //     const res = await API.get("/api/auth/payment/check_payment_status");
    //     if (res.data.status === "success"){
    //         console.log(res.data.status);
    //         setIsDisabled(false);
    //     };
    // };
    const makePayment = async (event) => {
        
        if (true){

            // add chosen supplier to the request
            const addSupplierResult = await axios.patch(`/api/requests/`+props.requestId+`/user/add-supplier`,{supplierId:props.supplierId})

            const result = await axios.post(`/api/create_conversation/request`,{requestId:props.requestId})
        //if (props.supplierId){
            //const result = await axios.post('/api/create_conversation/' + event.target.name)


            /////////////////// stripe /////////////////////////////
            // const stripe = await stripePromise;
            // const response = API.post("/api/payment/checkout_session",
            //                 {
            //                     supplierId: props.supplierId,
            //                     requestId: props.requestId
            //                 });
            // const session = await response;
            // const result = await stripe.redirectToCheckout({
            //     sessionId: session.data.id,
            // });

            if (result.error) {
                // If `redirectToCheckout` fails due to a browser or network
                // error, display the localized error message to your customer
                // using `result.error.message`.
                console.log(result.error);
              }
            console.log(result);
            
        }
    };
    
    //enableBeginChat();

    return (
    <Button
     color="#008000"
     variant="contained"
     disabled={isDisabled}
     onClick={makePayment}>
        Accept & Pay
    </Button>
    );
};

export default AcceptSupplierButton;