import React, { useState, useEffect } from "react";
import Router from "next/router";
import { makeStyles } from "@material-ui/core/styles";
import {
  Menu,
  MenuItem,
  Toolbar,
  AppBar,
  Typography,
  IconButton,
  Button,
} from "@material-ui/core";
import AccountCircle from "@material-ui/icons/AccountCircle";
import routeToDashboard from "../../functions/routings/defaultRoute.js";
import handleLogOut from "../../functions/logOut/logOut.js";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    zIndex: "100",
  },
  appBar: {
    position: "static",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function NavBar() {
  const classes = useStyles();
  
  const [isLoggedIn, setIsLoggedIn] = useState(false)
  useEffect(() => {
    setIsLoggedIn(Boolean(localStorage.getItem("userId")))
    console.log(localStorage.getItem("userId"))
  }, [])
  

  const handleLoginButton = () => {
    Router.push({
      pathname: "/login",
    });
  };

  const handleRegisterButton = () => {
    Router.push({
      pathname: "/user/register",
    });
  };

  const handleDashboardButton = () => {
    routeToDashboard(1);
  };

  const handleBecomeSupplierButton = () => {
    Router.push({
      pathname: "/supplier/add-identity",
    });
  };

  // const handleLogOut = async () => {
  //   const result = await axios.delete(`/api/logout`);
  //   console.log("result", result);
  //   if (result.data.status === true) {
  //     Router.push({
  //       pathname: "/",
  //     });
  //   }
  // };

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <Typography variant="h4" className={classes.title}>
            JustAsk
          </Typography>
          <Button color="inherit" onClick={handleLoginButton}>
              Login
            </Button>
          <Button color="inherit" onClick={handleRegisterButton}>
            Register
          </Button>


          {/* {!isLoggedIn && (
            <Button color="inherit" onClick={handleLoginButton}>
              Login
            </Button>
          )}
          {!isLoggedIn && (
            <Button color="inherit" onClick={handleRegisterButton}>
              Register
            </Button>
          )} */}

          {/* {isLoggedIn && (
            <Button color="inherit" onClick={handleDashboardButton}>
              Dashboard
            </Button>
          )}
          {isLoggedIn && (
            <Button color="inherit" onClick={handleBecomeSupplierButton}>
              Become a Supplier
            </Button>
          )}
          {isLoggedIn && (
            <>
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorE1={anchorEl}
                getContentAnchorEl={null}
                // https://github.com/mui-org/material-ui/issues/8090
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "center",
                }}
                open={open}
                onClose={handleClose}
              >
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={handleClose}>My Account</MenuItem>
                <MenuItem onClick={handleLogOut}>Log Out</MenuItem>
              </Menu>
            </>
          )} */}
        </Toolbar>
      </AppBar>
    </div>
  );
}
