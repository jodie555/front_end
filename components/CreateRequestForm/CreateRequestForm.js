import React, { useState, useEffect } from "react";
import axios from "../../util/api";
import {
  Typography,
  Paper,
  Link,
  Grid,
  Button,
  Box,
} from "@material-ui/core";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
// import styles from './CreateRequestForm.module.css';
import { makeStyles } from "@material-ui/core/styles";
import Copyright from "../Copyright/Copyright.js";

const useStyles = makeStyles((theme) => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
  },
  form: {
    // width:'100%',
    margin: "0.8rem",
  },
  title: {
    color: "#2ecc71",
  },
  inputLabel: {
    [theme.breakpoints.down("sm")]: {},
  },
  dropDown: {
    [theme.breakpoints.down("sm")]: {
      minWidth: "300px",
      marginBottom: "2.5rem",
    },
    [theme.breakpoints.up("lg")]: {
      minWidth: "300px",
      marginBottom: "2.5rem",
    },
  },
  descriptionField: {
    [theme.breakpoints.down("sm")]: {
      minWidth: "300px",
      marginBottom: "3.5rem",
    },
    [theme.breakpoints.up("lg")]: {
      minWidth: "500px",
      marginBottom: "2.5rem",
    },
  },
  submitButton: {
    left: "10%",
  },
}));

function CreateRequestForm(props) {
  const classes = useStyles();
  let [subjectsArray, setSubjectsArray] = useState([]);
  let [universitiesArray, setUniversitiesArray] = useState([]);
  useEffect(() => {
    async function getUniversitiesData() {
      let universities = [];
      let universitiesData = await axios.get(
        "/api/universities"
      );
      for (const uni of universitiesData.data) {
        universities.push(uni);
      }
      setUniversitiesArray(universities);
    }
    async function getSubjectsData() {
      let subjects = [];
      let subjectsData = await axios.get("/api/subjects");
      for (const sub of subjectsData.data) {
        subjects.push(sub);
      }
      setSubjectsArray(subjects);
    }
    getUniversitiesData();
    getSubjectsData();
  }, []);

  let subjectsOptions;
  subjectsOptions = subjectsArray.map((sub) => {
    return (
      <option key={sub.name} value={sub._id}>
        {sub.name}
      </option>
    );
  });
  // }
  let universitiesOptions;
  universitiesOptions = universitiesArray.map((uni) => {
    return (
      <option key={uni.name} value={uni._id}>
        {uni.name}
      </option>
    );
  });
  // }
  return (
    <main className={classes.content}>
      <div className={classes.appBarSpacer} />
      <Container maxWidth="lg" className={classes.container}>
        <Grid container spacing={3}>
          {/* Requests */}
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Typography
                className={classes.title}
                variant="h4"
                align="center"
                component="h1"
                gutterBottom
              >
                Create New Request
              </Typography>
              <form className={classes.form} encType="multipart/form-data">
                <Grid container>
                  <Grid item lg={1} xs={1} style={{ margin: "0.8rem" }}>
                    <InputLabel
                      className={classes.inputLabel}
                      htmlFor="university-dropdown"
                    >
                      Target University
                    </InputLabel>
                    <Select
                      native
                      inputProps={{
                        name: "university",
                        id: "university-dropdown",
                      }}
                      value={props.university}
                      className={classes.dropDown}
                      onChange={props.handleInputChange}
                    >
                      <option aria-label="None" value="" />
                      {universitiesOptions}
                    </Select>

                    <InputLabel
                      className={classes.inputLabel}
                      htmlFor="subject-dropdown"
                    >
                      Target Subject
                    </InputLabel>
                    <Select
                      native
                      inputProps={{
                        name: "subject",
                        id: "subject-dropdown",
                      }}
                      value={props.subject}
                      className={classes.dropDown}
                      onChange={props.handleInputChange}
                    >
                      <option aria-label="None" value="" />
                      {subjectsOptions}
                    </Select>

                    <TextField
                      required
                      name="description"
                      label="Question Description"
                      variant="outlined"
                      multiline
                      rows={4}
                      onChange={props.handleInputChange}
                      className={classes.descriptionField}
                    />

                    <Button
                      variant="contained"
                      color="primary"
                      type="submit"
                      onClick={props.onSubmit}
                      className={classes.submitButton}
                    >
                      Submit
                    </Button>
                  </Grid>
                </Grid>
              </form>
            </Paper>
          </Grid>
        </Grid>
        <Box pt={4}>
          <Copyright />
        </Box>
      </Container>
    </main>
  );
}

export default CreateRequestForm;
