import React, { useRef, Component } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

class AutocompleteCustom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.name,
      label: props.label,
      options: props.options,
      // user: props.user,
      error: props.error,
      value: props.value,
      disabled: props.disabled,
    };
    this.autocompleteRef = React.createRef();
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.options !== this.props.options) {
      this.setState({ options: this.props.options });
    }
    if (prevProps.disabled !== this.props.disabled) {
      this.setState({ disabled: this.props.disabled });
    }
  };

  handleChange = (event, newValue) => {
    const name = this.autocompleteRef.current.getAttribute("name");
    const user = this.props.user;
    if (newValue) {
      this.props.onChange(user, name, newValue);
      this.setState({
        value: newValue,
      });
    }
  };

  render() {
    return (
      <div>
        <Autocomplete
          id="combo-box-demo"
          ref={this.autocompleteRef}
          options={this.state.options}
          name={this.state.name}
          value={this.state.value}
          onChange={this.handleChange}
          getOptionLabel={(option) => option.name ? option.name : ""}

          // getOptionLabel={(option) => option.name}
          disabled={this.state.disabled}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Combo box"
              variant="outlined"
              label={this.state.label}
              error={this.state.error}
              helperText={this.state.error}
              required
            />
          )}
        />
      </div>
    );
  }
}

module.exports = AutocompleteCustom;
