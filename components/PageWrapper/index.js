import React, { Component, Fragment } from "react";
import Head from "next/head";
import "./cssreset.css";
import "./style.less";

export default class Page extends Component {
    render() {
        return (
            <Fragment>
                <Head>
                    <meta name="viewport" content="width=device-width, initial-scale=1" />
                    <meta charSet="utf-8" />
                    <title>MongoEd BETA</title>
                </Head>
                <div className="page-main">
                    <div className="page-wrapper">
                        {this.props.children}
                    </div>
                </div>
            </Fragment>
        );
    }
}
