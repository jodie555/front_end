import React from "react";
import {
  AppBar,
  Backdrop,
  CircularProgress,
  Container,
  CssBaseline,
  Grid,
  IconButton,
  List,
  TextField,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { Send } from "@material-ui/icons";
import axios from "../../../util/api";
import ChatItem from "../ChatItem";
const Chat = require("twilio-chat");
import Router from 'next/router'
import { Client as ConversationsClient } from "@twilio/conversations"



class ChatScreen extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
        text: "",
        messages: [],
        loading: false,
        channel: null,
        email:"",
        room:"",
        conversations: []
      };
  
      this.scrollDiv = React.createRef();
    }

    newToken = () => {
      // // Paste your unique Chat token function
      // const myToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzhmMjI1MGE3YTBlNjZlY2E4MTBhMjQ5ZTgzODg0NzdhLTE2MTIwMTYzMTkiLCJncmFudHMiOnsiaWRlbnRpdHkiOiJ0ZXN0UGluZWFwcGxlIiwiY2hhdCI6eyJzZXJ2aWNlX3NpZCI6IklTNDU2NWJmMDk2ZTMwNDA4Y2E0NmQ5MDlhYWYwNjdhMjkifX0sImlhdCI6MTYxMjAxNjMxOSwiZXhwIjoxNjEyMDE5OTE5LCJpc3MiOiJTSzhmMjI1MGE3YTBlNjZlY2E4MTBhMjQ5ZTgzODg0NzdhIiwic3ViIjoiQUM4NjI1YzE3YTA3MWJhM2JjOTI2OTBlNWNhOTAzYzc3ZSJ9.V0Inl9y9hqt5TextnnxEXXUIvVEMsQd-TfaNn9SkMsI"
      // this.setState({ token: myToken }, this.initConversations);
    };
  


    getToken = async (email) => {

        const response = await axios.get(`/token/${email}`);
        // const response = await axios.get(`http://192.168.0.102:3000/token/${email}`);

        const { data } = response;
        return data.token;
      }

    componentDidMount = async () => {
        
        this.newToken()
        // const { location } = this.props;
        // const { state } = location || {};
        // const { email, room } = state || {};
        let token = "";

        const email = localStorage.getItem('email') || null
        const room = localStorage.getItem('room') || null
        // const { email, room } = Router.query || {};
        console.log("information ",email)

      
        if (!email || !room) {
          // Router.replace('/twiliochat')
          // this.props.history.replace("/");
          Router.push({
            pathname: '/twiliochat'

          })
        }
        this.setState({ 
          loading: true, 
          email: email,
          room : room
        });

        try {
          // email = "aps19isec@gmial.com"
          token = await this.getToken(email);
        } catch {
          throw new Error("Unable to get token, please reload this page");
        }


        const client = await Chat.Client.create(token);

        client.on("tokenAboutToExpire", async () => {
          const token = await this.getToken(email);
          client.updateToken(token);
        });
      
        client.on("tokenExpired", async () => {
          const token = await this.getToken(email);
          client.updateToken(token);
        });

        client.on("channelJoined", async (channel) => {
            // getting list of all messages since this is an existing channel
            const messages = await channel.getMessages();
            this.setState({ messages: messages.items || [] });
            // this.scrollToBottom();
          });
        
          try {
            const channel = await client.getChannelByUniqueName(room);
            this.joinChannel(channel);
          } catch(err) {
            try {
              const channel = await client.createChannel({
                uniqueName: room,
                friendlyName: room,
              });
          
              this.joinChannel(channel);
            } catch {
              throw new Error("Unable to create channel, please reload this page");
            }
          } 

      }


    sendMessage = () => {
    const { text, channel } = this.state;
    if (text) {
        this.setState({ loading: true });
        channel.sendMessage(String(text).trim());
        this.setState({ text: "", loading: false });
    }
    };

    joinChannel = async (channel) => {
        if (channel.channelState.status !== "joined") {
         await channel.join();
       }
     
       this.setState({ 
           channel:channel, 
           loading: false 
       });
     
       channel.on("messageAdded", this.handleMessageAdded);
      //  this.scrollToBottom();
     };
     
     
     handleMessageAdded = (message) => {
       const { messages } = this.state;
       console.log("handleMessageAdded "+message.author)
       this.setState({
           messages: [...messages, message],
         },
         this.scrollToBottom
       );
     };
     
     scrollToBottom = () => {
      //  const scrollHeight = this.scrollDiv.current.scrollHeight
       const scrollHeight = 0

      //  const height = this.scrollDiv.current.clientHeight;
       const height = 0;

       const maxScrollTop = scrollHeight - height;
      //  this.scrollDiv.current.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
     };


     render() {
        const { loading, text, messages, channel, email, room } = this.state;
        const { location } = this.props;
        const { state } = location || {};
        // const email = localStorage.getItem('email') || null
        // const room = localStorage.getItem('room') || null

        // const { email, room } = state || {};
        // const { email, room } = Router.query || {};
      
        return (
          <Container component="main" maxWidth="md">
            <Backdrop open={loading} style={{ zIndex: 99999 }}>
              <CircularProgress style={{ color: "white" }} />
            </Backdrop>
      
            <AppBar elevation={10}>
              <Toolbar>
                <Typography variant="h6">
                  {`Room: ${room}, User: ${email}`}
                </Typography>
              </Toolbar>
            </AppBar>
      
            <CssBaseline />
      
            <Grid container direction="column" style={styles.mainGrid}>
              <Grid item style={styles.gridItemChatList} ref={this.scrollDiv}>
                <List dense={true}>
                {messages &&
                      messages.map((message) => 
                        <ChatItem
                          key={message.index}
                          message={message}
                          email={email}/>
                      )}
                </List>
              </Grid>
      
              <Grid item style={styles.gridItemMessage}>
                <Grid
                  container
                  direction="row"
                  justifyContent="center"
                  alignItems="center">
                  <Grid item style={styles.textFieldContainer}>
                    <TextField
                      required
                      style={styles.textField}
                      placeholder="Enter message"
                      variant="outlined"
                      multiline
                      rows={2}
                      value={text}
                      disabled={!channel}
                      onChange={(event) =>
                        this.setState({ text: event.target.value })
                      }/>
                  </Grid>
                  
                  <Grid item>
                    <IconButton
                      style={styles.sendButton}
                      onClick={this.sendMessage}
                      disabled={!channel}>
                      <Send style={styles.sendIcon} />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Container>
        );
      }

  }

  const styles = {
    textField: { width: "100%", borderWidth: 0, borderColor: "transparent" },
    textFieldContainer: { flex: 1, marginRight: 12 },
    gridItem: { paddingTop: 12, paddingBottom: 12 },
    gridItemChatList: { overflow: "auto", height: "70vh" },
    gridItemMessage: { marginTop: 12, marginBottom: 12 },
    sendButton: { backgroundColor: "#3f51b5" },
    sendIcon: { color: "white" },
    mainGrid: { paddingTop: 100, borderWidth: 1 },
  };
  
  export default ChatScreen;