// this one need to be used with chat box(/components/Chat) class component which has getDerivedStateFromProp


import React from "react";
import {
  AppBar,
  Backdrop,
  CircularProgress,
  Container,
  CssBaseline,
  Grid,
  IconButton,
  List,
  TextField,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { Send } from "@material-ui/icons";
import axios from "../../../util/api";
import ChatItem from "./ChatItem";
const Chat = require("twilio-chat");
import Router from 'next/router'
import { Client as ConversationsClient } from "@twilio/conversations"
import ChatBox from "../Chat/Box"


class ChatScreen extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
        text: "",
        messages: [],
        loading: false,
        channel: null,
        email:null,
        room:null,
        conversations: [],
        selectedConversationSid: null,
        selectedConversation:null,
        newMessage:[],
        // newMessage:"",
        loadingState:false,
        statusString: "disconnected",
        token:"",
        status:"default"
      };
  
      this.scrollDiv = React.createRef();
    }

    newToken = async (email) => {
      // Paste your unique Chat token function
      // const myToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzhmMjI1MGE3YTBlNjZlY2E4MTBhMjQ5ZTgzODg0NzdhLTE2MTI4NzU2MjEiLCJncmFudHMiOnsiaWRlbnRpdHkiOiJ0ZXN0UGluZWFwcGxlIiwiY2hhdCI6eyJzZXJ2aWNlX3NpZCI6IklTNDU2NWJmMDk2ZTMwNDA4Y2E0NmQ5MDlhYWYwNjdhMjkifX0sImlhdCI6MTYxMjg3NTYyMSwiZXhwIjoxNjEyODc5MjIxLCJpc3MiOiJTSzhmMjI1MGE3YTBlNjZlY2E4MTBhMjQ5ZTgzODg0NzdhIiwic3ViIjoiQUM4NjI1YzE3YTA3MWJhM2JjOTI2OTBlNWNhOTAzYzc3ZSJ9.iEbmSGK0WEB3YP5MC2aWCejVTyVoj6XHcOc7K2jA6YI"
      const response = await axios.get(`http://localhost:3000/token/${email}`);
      console.log("response",response)
      const { data } = response;
      this.setState({ token: data.token }, this.initConversations);

    };
  
    initConversations = async () => {
      window.conversationsClient = ConversationsClient;
      this.conversationsClient = await ConversationsClient.create(this.state.token);
      // this.setState({ statusString: "Connecting to Twilio…" });
  
      this.conversationsClient.on("connectionStateChanged", (state) => {
        console.log('state',state)
        if (state === "connecting"){
          this.setState({
            statusString: "Connecting to Twilio…",
            status: "default"
          });
        }

        if (state === "connected") {
          this.setState({
            statusString: "You are connected.",
            status: "success"
          });
        }
        if (state === "disconnecting"){
          this.setState({
            statusString: "Disconnecting from Twilio…",
            conversationsReady: false,
            status: "default"
          });
        }

        if (state === "disconnected"){
          this.setState({
            statusString: "Disconnected.",
            conversationsReady: false,
            status: "warning"
          });
        }

        if (state === "denied"){
          this.setState({
            statusString: "Failed to connect.",
            conversationsReady: false,
            status: "error"
          });
        }

      });
      this.conversationsClient.on("conversationJoined", (conversation) => {

        this.setState({ conversations: [...this.state.conversations, conversation] });
      });
      this.conversationsClient.on("conversationLeft", (thisConversation) => {
        this.setState({
          conversations: [...this.state.conversations.filter((it) => it !== thisConversation)]
        });
      });


    };


    componentDidMount = async () => {
        


        let token = "";

        const email = localStorage.getItem('email') || null
        const room = localStorage.getItem('room') || null

        this.newToken(email)
        // const { email, room } = Router.query || {};
        console.log("email ",email)
        // var email = "ahhisec@gmail.com"
        // var room = "abcd"
      
        if (!email || !room) {
          // Router.replace('/twiliochat')
          // this.props.history.replace("/");
          Router.push({
            pathname: '/twiliochat'

          })
        }
        this.setState({ 
          loading: true, 
          email: email,
          room : room
        });

        


      }
    

    componentDidUpdate = (oldProps, oldState) => {
      console.log("oldState",oldState)
      console.log("conversations",this.state.conversations )

    }

    // shouldComponentUpdate = (nextProps) => {
    //   return (this.props.selectedConversationSid !== nextProps.selectedConversationSid)
    // }

    sendMessage = event => {
      console.log("sendMessage newMessage", this.state.newMessage)
      event.preventDefault();

      // const message = this.state.newMessage;
      if (this.state.selectedConversation){
        this.state.selectedConversation.sendMessage(this.state.newMessage);
      }
    
      // this.setState({ newMessage: '' });
      // this.state.conversations.sendMessage(message);
    };


    joinChannel = async (channel) => {

     };

     handleInputChange = event => {
       const value = event.target.value
       const name = event.target.name

       this.setState({
        [name]: value
      });

     }

     chooseConversation = event => {
       console.log("chooseConversation event", event.target.value)
       const selectedConversationSid = event.target.value

       const selectedConversation = this.state.conversations.find(
         (it) => {
           if(it.sid === selectedConversationSid){
             return it
           }
         });

      this.setState({
        selectedConversationSid:selectedConversationSid,
        selectedConversation: selectedConversation
      })
     }
     
     
     handleMessageAdded = (message) => {

     };
     
     scrollToBottom = () => {
      //  const scrollHeight = this.scrollDiv.current.scrollHeight
       const scrollHeight = 0

      //  const height = this.scrollDiv.current.clientHeight;
       const height = 0;

       const maxScrollTop = scrollHeight - height;
      //  this.scrollDiv.current.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
     };


     render() {
        const { loading, text, messages, channel, email, room, conversations, newMessage, selectedConversationSid } = this.state;
        const { location } = this.props;
        const { state } = location || {};




        let conversationContent;
        if(this.state.selectedConversation){
          console.log("selected conversation",this.state.selectedConversation)
          conversationContent = (
            <ChatBox
              conversationProxy={this.state.selectedConversation}
              myIdentity={this.state.name}
              email={email}
            />
          )

        }
        




        var newtest = ['a','b','c']
        var testresult = newtest.find(
          (it) => it === 'a'
        )

        console.log("resulttest",testresult)
        

        




        return (
          <div>
            <h1>{this.state.selectedConversationSid}</h1>
            {this.state.email}
            aaaa
            {/* {this.state.conversations && this.state.conversations.length > 0 } */}
            {/* {conversations[0]} */}
            {conversations && conversations.length > 0 && conversations.map((item)=> 
              <div>
                <button onClick={this.chooseConversation} value={item.sid}>{item.sid}</button>
              </div>

            )}

          {conversationContent}
          <form onSubmit={this.sendMessage}>
              <label>
                  message:
                  <input type="text" name="newMessage" onChange={this.handleInputChange}/>
                </label>
                <input type="submit" value="Submit" />
          </form>
          <form>
              <label>
                  email:
                  <input type="text" name="email" onChange={this.handleInputChange}/>
                </label>
                <input type="submit" value="Submit" />
          </form>
          </div>
        );
      }

  }

  const styles = {
    textField: { width: "100%", borderWidth: 0, borderColor: "transparent" },
    textFieldContainer: { flex: 1, marginRight: 12 },
    gridItem: { paddingTop: 12, paddingBottom: 12 },
    gridItemChatList: { overflow: "auto", height: "70vh" },
    gridItemMessage: { marginTop: 12, marginBottom: 12 },
    sendButton: { backgroundColor: "#3f51b5" },
    sendIcon: { color: "white" },
    mainGrid: { paddingTop: 100, borderWidth: 1 },
  };
  
  export default ChatScreen;