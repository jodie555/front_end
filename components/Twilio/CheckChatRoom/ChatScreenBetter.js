import React from "react";
import {
  AppBar,
  Backdrop,
  CircularProgress,
  Container,
  CssBaseline,
  Grid,
  IconButton,
  List,
  TextField,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { Send } from "@material-ui/icons";
import axios from "../../../util/api";
import ChatItem from "./ChatItem";
const Chat = require("twilio-chat");
import Router from 'next/router'
import { Client as ConversationsClient } from "@twilio/conversations"
import ChatBox from "../Chat/Box"
import createChatRoomByName from "./CreateChatRoomByName"


class ChatScreen extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
        text: "",
        messages: [],
        loading: false,
        channel: null,
        email:null,
        room:null,
        conversations: [],
        selectedConversationSid: null,
        selectedConversation:null,
        newMessage:[],
        // newMessage:"",
        loadingState:false,
        statusString: "disconnected",
        token:"",
        status:"default",
        client:""
      };
  
      this.scrollDiv = React.createRef();
    }

    newToken = async () => {
      // before we generate a token, we must confirm once more the user identification

      try{
        const response = await axios.get(`/token/email`)
        console.log("response",response)

        const { data } = response;
        this.setState({ token: data.token,email:data.identity }, this.initConversations);

      }catch (err){
        console.log("error",err)
        Router.push("/google_login")


      }


    };
  
    initConversations = async () => {
      window.conversationsClient = ConversationsClient;
      this.conversationsClient = await ConversationsClient.create(this.state.token);
      // this.setState({ statusString: "Connecting to Twilio…" });
  
      this.conversationsClient.on("connectionStateChanged", (state) => {
        if (state === "connecting"){
          this.setState({
            statusString: "Connecting to Twilio…",
            status: "default"
          });
        }

        if (state === "connected") {
          this.setState({
            statusString: "You are connected.",
            status: "success"
          });
        }
        if (state === "disconnecting"){
          this.setState({
            statusString: "Disconnecting from Twilio…",
            conversationsReady: false,
            status: "default"
          });
        }

        if (state === "disconnected"){
          this.setState({
            statusString: "Disconnected.",
            conversationsReady: false,
            status: "warning"
          });
        }

        if (state === "denied"){
          this.setState({
            statusString: "Failed to connect.",
            conversationsReady: false,
            status: "error"
          });
        }

      });
      this.conversationsClient.on("conversationJoined", (conversation) => {

        this.setState({ conversations: [...this.state.conversations, conversation] });
      });
      this.conversationsClient.on("conversationLeft", (thisConversation) => {
        this.setState({
          conversations: [...this.state.conversations.filter((it) => it !== thisConversation)]
        });
      });

      // check whether the room with that name is created or not
      console.log("this.conversationsClient",this.conversationsClient)
      // this.conversationsClient.getConversationByUniqueName(localStorage.getItem('room') || null)
      const identity = localStorage.getItem('email') || null
      const room = localStorage.getItem('room') || null
      createChatRoomByName(this.conversationsClient,identity,room)



    };


    componentDidMount = async () => {
        
        let token = "";

        const email = this.state.email
        const room = localStorage.getItem('room') || null

        //create a room by the input name
        // const createdConversation = await axios.post(`http://localhost:3000/api/create_conversation`,{"uniqueName":room});
        // console.log("createdConversation",createdConversation.data.conversation.sid)
        // const conversationSid = createdConversation.data.conversation.sid
        // const addedParticipant = await axios.post(`http://localhost:3000/api/add_participant`,{"identity":email,"conversationSid":conversationSid});
        // console.log("addedParticipant",addedParticipant)

        // verify the emial in backend. If fail, route to log in page 
        this.newToken()


      
        // if (!email || !room) {
        //   // Router.replace('/twiliochat')
        //   // this.props.history.replace("/");
        //   Router.push({
        //     pathname: '/twiliochat'

        //   })
        // }


        this.setState({ 
          loading: true, 
          email: email,
          room : room
        });
        


      }
    

    componentDidUpdate = (oldProps, oldState) => {
      console.log("oldState",oldState)
      console.log("conversations",this.state.conversations )

    }




    sendMessage = event => {
      console.log("sendMessage newMessage", this.state.newMessage)
      event.preventDefault();

      // const message = this.state.newMessage;
      if (this.state.selectedConversation){
        this.state.selectedConversation.sendMessage(this.state.newMessage);
      }
    

    };


    joinChannel = async (channel) => {

     };

     handleInputChange = event => {
       const value = event.target.value
       const name = event.target.name

       this.setState({
        [name]: value
      });

     }

     chooseConversation = event => {
       console.log("chooseConversation event", event.target.value)
       const selectedConversationSid = event.target.value

       const selectedConversation = this.state.conversations.find(
         (it) => {
           if(it.sid === selectedConversationSid){
             return it
           }
         });

      this.setState({
        selectedConversationSid:selectedConversationSid,
        selectedConversation: selectedConversation
      })
     }
     
     
     handleMessageAdded = (message) => {

     };
     
     scrollToBottom = () => {
      //  const scrollHeight = this.scrollDiv.current.scrollHeight
       const scrollHeight = 0

      //  const height = this.scrollDiv.current.clientHeight;
       const height = 0;

       const maxScrollTop = scrollHeight - height;
      //  this.scrollDiv.current.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
     };


     render() {
        const { loading, text, messages, channel, email, room, conversations, newMessage, selectedConversationSid } = this.state;
        const { location } = this.props;
        const { state } = location || {};




        let conversationContent;
        if(this.state.selectedConversation){
          conversationContent = (
            <ChatBox
              conversationProxy={this.state.selectedConversation}
              myIdentity={this.state.name}
              email={email}
            />
          )

        }
        




        var newtest = ['a','b','c']
        var testresult = newtest.find(
          (it) => it === 'a'
        )

        console.log("resulttest",testresult)
        

        




        return (
          <div>
            <h1>{this.state.selectedConversationSid}</h1>
            {this.state.email}
            aaaa
            {/* {this.state.conversations && this.state.conversations.length > 0 } */}
            {/* {conversations[0]} */}
            {conversations && conversations.length > 0 && conversations.map((item)=> 
              <div>
                <button onClick={this.chooseConversation} value={item.sid}>{item.sid}</button>
              </div>

            )}

          {conversationContent}
          <form onSubmit={this.sendMessage}>
              <label>
                  message:
                  <input type="text" name="newMessage" onChange={this.handleInputChange}/>
                </label>
                <input type="submit" value="Submit" />
          </form>
          <form>
              <label>
                  email:
                  <input type="text" name="email" onChange={this.handleInputChange}/>
                </label>
                <input type="submit" value="Submit" />
          </form>
          </div>
        );
      }

  }

  const styles = {
    textField: { width: "100%", borderWidth: 0, borderColor: "transparent" },
    textFieldContainer: { flex: 1, marginRight: 12 },
    gridItem: { paddingTop: 12, paddingBottom: 12 },
    gridItemChatList: { overflow: "auto", height: "70vh" },
    gridItemMessage: { marginTop: 12, marginBottom: 12 },
    sendButton: { backgroundColor: "#3f51b5" },
    sendIcon: { color: "white" },
    mainGrid: { paddingTop: 100, borderWidth: 1 },
  };
  
  export default ChatScreen;