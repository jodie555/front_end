import React from "react";
import {
  AppBar,
  Backdrop,
  CircularProgress,
  Container,
  CssBaseline,
  Grid,
  IconButton,
  List,
  TextField,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { Send } from "@material-ui/icons";
import axios from "../../util/api";
import ChatItem from "./ChatItem";
const Chat = require("twilio-chat");
import Router from 'next/router'
import { Client as ConversationsClient } from "@twilio/conversations"
import ChatBox from "../Chat/Box"
import createChatRoomByName from "./CreateChatRoomByName"
import {checkChatRoomExistByClient,checkChatRoomExistBySession }from "../../functions/twilio/checkChatRoomExist"
import {createChatRoomByUniqueName,createChatRoomBySession }from "../../functions/twilio/createChatRoom"
import {joinChatRoomByBodyIdentity,joinChatRoomBySessionIdentity }from "../../functions/twilio/joinChatRoom"
import {postRoomName }from "../../functions/twilio/room"


class ChatScreen extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {
        text: "",
        messages: [],
        loading: false,
        channel: null,
        email:null,
        room:null,
        conversations: [],
        selectedconversationSID: null,
        selectedConversation:null,
        newMessage:[],
        // newMessage:"",
        loadingState:false,
        statusString: "disconnected",
        token:"",
        status:"default",
        client:"",
        roomName:""
      };
  
      this.scrollDiv = React.createRef();
    }

    newToken = async () => {
      // before we generate a token, we must confirm once more the user identification

      try{
        const response = await axios.get(`/token/email`)
        console.log("response",response)

        const { data } = response;
        this.setState({ token: data.token,email:data.identity }, this.initConversations);

      }catch (err){
        console.log("error",err)
        Router.push("/google_login")


      }


    };
  
    initConversations = async () => {
      window.conversationsClient = ConversationsClient;
      this.conversationsClient = await ConversationsClient.create(this.state.token);
      // this.setState({ statusString: "Connecting to Twilio…" });


      // check whether the room with that name is created or not
      const existOrNot = await checkChatRoomExistBySession(this.conversationsClient)

      if (!existOrNot && existOrNot !== undefined){

        try{
          const createdConversation = await createChatRoomBySession()
          const conversationSID = createdConversation.data.conversation.sid

          const addedParticipant = await joinChatRoomBySessionIdentity(conversationSID)
          console.log("added the participant to a chat room successfully")
          Router.push("/newChat")

          }
          catch(e){
            console.log("error",e)
          }

      }






    };


    componentDidMount = async () => {
        this._isMounted = true;
        
        let token = "";



      }
    


    componentWillUnmount() {
      this._isMounted = false;
    }


    sendMessage = event => {
      console.log("sendMessage newMessage", this.state.newMessage)
      event.preventDefault();

      // const message = this.state.newMessage;
      if (this.state.selectedConversation){
        this.state.selectedConversation.sendMessage(this.state.newMessage);
      }
    

    };

    sendRoomName =async event => {
      event.preventDefault();
      console.log("room name", this.state.roomName)
      const roomName = this.state.roomName
      if (roomName){
        await postRoomName(roomName)
        // verify the emial in backend. If fail, route to log in page 
        // if success create a chat room by the provided room name
        await this.newToken()
      }


    }

    checkSession = async event => {
      event.preventDefault();
      const result = await axios.get(`/api/session/roomName`)
      console.log("result",result)
    }


    joinChannel = async (channel) => {

     };

     handleInputChange = event => {
       const value = event.target.value
       const name = event.target.name

       this.setState({
        [name]: value
      });

     }


  
     
     scrollToBottom = () => {
      //  const scrollHeight = this.scrollDiv.current.scrollHeight
       const scrollHeight = 0

      //  const height = this.scrollDiv.current.clientHeight;
       const height = 0;

       const maxScrollTop = scrollHeight - height;
      //  this.scrollDiv.current.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
     };


     render() {
        const { loading, text, messages, channel, email, room, conversations, newMessage, selectedconversationSID } = this.state;
        const { location } = this.props;
        const { state } = location || {};




        let conversationContent;
        if(this.state.selectedConversation){
          conversationContent = (
            <ChatBox
              conversationProxy={this.state.selectedConversation}
              myIdentity={this.state.name}
              email={email}
            />
          )

        }
      

        return (
          <div>

          <form onSubmit={this.sendRoomName}>
              <label>
                  Room Name:
                  <input type="text" name="roomName" onChange={this.handleInputChange}/>
                </label>
                <input type="submit" value="Submit" />
          </form>

          <button onClick={this.checkSession}>get session roomName</button>

          </div>
        );
      }

  }

  const styles = {
    textField: { width: "100%", borderWidth: 0, borderColor: "transparent" },
    textFieldContainer: { flex: 1, marginRight: 12 },
    gridItem: { paddingTop: 12, paddingBottom: 12 },
    gridItemChatList: { overflow: "auto", height: "70vh" },
    gridItemMessage: { marginTop: 12, marginBottom: 12 },
    sendButton: { backgroundColor: "#3f51b5" },
    sendIcon: { color: "white" },
    mainGrid: { paddingTop: 100, borderWidth: 1 },
  };
  
  export default ChatScreen;