import axios from "../../util/api";

// error: create a conversation room with name that been used
// a route to check whether the conversation room is created 

export default async function createChatRoomByName(conversationsClient,identity,uniqueName){
  if(uniqueName){
    // check whether the user (conversations client) has joined the conversation (by uniqueName) before 
    const createdOrNot =  await conversationsClient.getConversationByUniqueName(uniqueName)
          .then(conversation => {
            console.log("getConversationBySid",conversation)
            return true
          })
          .catch(error => {
            console.log("error",error)
            return false
          })
    
    if (!createdOrNot){
      console.log("createdOrNot inside",createdOrNot)
      //create a room by the input name
      const createdConversation = await axios.post(`http://localhost:3000/api/create_conversation`,{"uniqueName":uniqueName});
      console.log("createdConversation",createdConversation.data.conversation.sid)
      const conversationSID = createdConversation.data.conversation.sid
      const addedParticipant = await axios.post(`http://localhost:3000/api/add_participant`,{"identity":identity,"conversationSID":conversationSID});
      console.log("addedParticipant",addedParticipant)
    }
  }
}
  
