import React, { useState } from "react";

import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';


function LoadingComponent(props) {

  return (

    <Box sx={{ display: 'flex' }}>
    <CircularProgress />
  </Box>
  );
}

export default LoadingComponent;