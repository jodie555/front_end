import React, {useState}from 'react'

export const AuthContext = React.createContext({
    token: '',
    isLoggedIn:false,
    login: (token) => {},
    logout: () => {}
})

export const AuthContextProvider = (props) => {
    const [isSupplier,setSupplier] = useState(null)


    const userIsLoggedIn = !!AuthContext.token

    const loginHandler = (response) => {
        setToken(response)
        localStorage.setItem(
            "isSupplier",
            response.data.isSupplier 
          );
        localStorage.setItem("userId", response.data._id);
    }

    const logoutHandler = () => {
        setToken(null)
    }
    

    const contextValue = {
        token: AuthContext.token,
        isLoggedIn: userIsLoggedIn,
        login: loginHandler,
        logout: logoutHandler,

    }




    return <AuthContext.Provider value={contextValue}>{props.children}</AuthContext.Provider>
}

