import React,{useEffect} from "react";
import axiosDefault from "../../../utils/axios"
const axios = axiosDefault.axiosDefault()
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import { Row, Column, Item } from "@mui-treasury/components/flex";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";


const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#eeeeee", //#6bb9f0
  },
  title: {
    color: "#2ecc71",
    fontWeight: "bold",
  },
  content: {
    flex: "1 0 auto",
  },
  avatarImage: {
    paddingLeft: theme.spacing(3),
    alignItems: "center",
  },
  deleteIcon: {
    height: 38,
    width: 38,
  },
  endRoomButton: {
    backgroundColor: "white",
    color: "#f64747",
    "&:hover": {
      backgroundColor: "#cccccc",
      color: "#f64747",
      borderColor: "#f64747",
    },
    borderColor: "#f64747",
    borderStyle: "solid",
    borderRadius: "7",
  },
}));

export default React.memo(function TutorCard(props) {
  const [openDialogue, setOpenDialogue] = React.useState(false);
  const classes = useStyles();

  
  return (
    <>
      {/* <Row p={1.5} gap={1} className={classes.root} borderRadius={8}> */}
        <Column position={"middle"}>
          <Item ml={1}>
            <div className={classes.avatarImage}>
              <Avatar className={classes.avatar} src={props.supplierImageURL} />
            </div>
            <Rating
              name="size-small"
              value={props.rating}
              precision={0.5}
              size="small"
              readOnly
            />
          </Item>
        </Column>

        <Column gap={0.5}>
          <Item ml={1} position={"middle"}>
            <Row gap={0.1}>
              <Typography component="h5" variant="h5" className={classes.title}>
                {props.supplierName}, {props.supplierMajor}
              </Typography>
            </Row>
          </Item>
          <Item ml={1} position={"middle"}>
            <Row gap={0.1}>
              <Item>
                <Typography
                  variant="subtitle1"
                  color="textSecondary"
                  className={classes.title}
                >
                  {props.supplierUniversity}
                </Typography>
              </Item>
            </Row>
          </Item>
        </Column>

      {/* </Row> */}
      <div style={{ height: "10px" }}></div>


    </>
  );
});
