import React from "react";
import { ListItem } from "@material-ui/core";

class ChatBox extends React.Component {
  render() {
    const { message, email } = this.props;
    const isOwnMessage = message.author === email;

    return (
      <ListItem style={styles.listItem(isOwnMessage)}>
        <div style={styles.container(isOwnMessage)}>
          {message.body}
          <div style={styles.timestamp(isOwnMessage)}>
            {new Date(message.dateCreated.toISOString()).toLocaleString()}
          </div>
        </div>
        <div></div>
      </ListItem>
    );
  }
}

const styles = {
  listItem: (isOwnMessage) => ({
    flexDirection: "column",
    alignItems: isOwnMessage ? "flex-end" : "flex-start",
  }),
  container: (isOwnMessage) => ({
    maxWidth: "75%",
    borderRadius: 12,
    padding: 16,
    color: isOwnMessage ? "white" : "black",
    fontSize: 12,
    backgroundColor: isOwnMessage ? "#2ECC71" : "#F8F8F8",
    boxShadow: "2px 4px 9px #888888",
  }),
  author: { fontSize: 10, color: "gray" },
  timestamp: (isOwnMessage) => ({
    fontSize: 8,
    color: isOwnMessage ? "white" : "black",
    textAlign: "right",
    paddingTop: 4,
  }),
};

export default ChatBox;
