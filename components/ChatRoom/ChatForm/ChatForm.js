import React from 'react';
import styles from './Form.module.css'


const ChatForm = ({sendMessage, handleInputChange, sendButtonDisabled, newMessage}) => {
    return (
        <form onSubmit={sendMessage} className={styles.chatForm}>
            <>
                <label>
                    <input type="text" name="newMessage" value={newMessage} onChange={handleInputChange} className={styles.chatInput}/>
                </label>
                <input type="submit" value="Send" className={styles.chatButton} disabled={sendButtonDisabled}/>
            </>
        </form>
    )
}

export default ChatForm;