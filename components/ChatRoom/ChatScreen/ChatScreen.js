import React from "react";
import axios from "../../../util/api";

import Router from "next/router";
import { Client as ConversationsClient } from "@twilio/conversations";
import ChatBox from "../ChatBox/ChatBox";
import ChatForm from "../ChatForm/ChatForm";
import styles from "./ChatScreen.module.css";

class ChatScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: "",
      messages: [],
      loading: false,
      channel: null,
      email: null,
      room: null,
      conversations: [],
      selectedConversationSID: null,
      requestId: null,

      selectedConversation: null,
      newMessage: [],
      // newMessage:"",
      loadingState: false,
      statusString: "disconnected",
      token: "",
      status: "default",
      client: "",
      roomName: "",
      chatTitle: "",
      requestStatus: 1,
      supplierId: "",
      userId:"",
      supplierName: "",
      supplierUniversity: "",
      supplierMajor: "",
      supplierImageURL: "",
      isUser: props.isUser,
    };

    this.scrollDiv = React.createRef();
  }

  handleInputChange = (event) => {
    const value = event.target.value;
    const name = event.target.name;

    this.setState({
      [name]: value,
    });
  };

  newToken = async () => {
    // before we generate a token, we must confirm once more the user identification
    try {
      if (this.props.selectedConversationSID) {
        localStorage.setItem(
          "selectedConversationSID",
          this.props.selectedConversationSID
        );
      }

      const response = await axios.get(`/token/id`);

      const { data } = response;
      this.setState(
        {
          token: data.token,
          email: data.identity,
          selectedConversationSID: localStorage.getItem(
            "selectedConversationSID"
          ),
        },
        this.initConversations
      );
    } catch (err) {
      console.log("error", err);
      Router.push("/google_login");
    }
  };

  initConversations = async () => {
    window.conversationsClient = ConversationsClient;
    this.conversationsClient = await ConversationsClient.create(
      this.state.token
    ).catch(e=>{
      console.log(e)
    });
    // this.setState({ statusString: "Connecting to Twilio…" });




    this.conversationsClient.on("connectionStateChanged", (state) => {
      if (state === "connecting") {
        this.setState({
          statusString: "Connecting to Twilio…",
          status: "default",
        });
      }

      if (state === "connected") {
        this.setState({
          statusString: "You are connected.",
          status: "success",
        });
      }
      if (state === "disconnecting") {
        this.setState({
          statusString: "Disconnecting from Twilio…",
          conversationsReady: false,
          status: "default",
        });
      }

      if (state === "disconnected") {
        this.setState({
          statusString: "Disconnected.",
          conversationsReady: false,
          status: "warning",
        });
      }

      if (state === "denied") {
        this.setState({
          statusString: "Failed to connect.",
          conversationsReady: false,
          status: "error",
        });
      }
    });

    this.conversationsClient.on("conversationJoined", (conversation) => {
      console.log("conversation.sid",conversation.sid)
      if (conversation.sid === this.state.selectedConversationSID) {
        this.setState({
          selectedConversation: conversation,
        });
      }

      // this.setState({ conversations: [...this.state.conversations, conversation] });
    });
    this.conversationsClient.on("conversationLeft", (thisConversation) => {
      this.setState({
        conversations: [
          ...this.state.conversations.filter((it) => it !== thisConversation),
        ],
      });
    });



  };

  componentDidMount = async () => {

    this._isMounted = true;


    this.newToken();
  };

  componentDidUpdate = (oldProps, oldState) => {};

  componentWillUnmount() {
    this._isMounted = false;
  }

  sendMessage = (event) => {
    event.preventDefault();

    // const message = this.state.newMessage;
    if (this.state.selectedConversation) {
      this.state.selectedConversation.sendMessage(this.state.newMessage);

      // chat room send message
      if(this.state.supplierId){
        const res =  axios.post(`/api/notification/new-message`,{receiverID: this.state.supplierId})
      }
    }
    this.setState({
      newMessage: "",
    });
  };

  render() {
    const {
      loading,
      text,
      messages,
      channel,
      email,
      room,
      conversations,
      newMessage,
      selectedConversationSID,
    } = this.state;
    const { location } = this.props;
    const { state } = location || {};
    console.log("isUser::", this.state.isUser)


    let conversationContent;
    if (this.state.selectedConversation) {
      conversationContent = (
        <ChatBox
          conversationProxy={this.state.selectedConversation}
          myIdentity={this.state.name}
          email={email}
        />
      );
    }

    return (
      <>


        <div className={styles.chatContent}>{conversationContent}</div>

        
        <ChatForm
          sendMessage={this.sendMessage}
          handleInputChange={this.handleInputChange}
          sendButtonDisabled={!this.state.newMessage}
          newMessage={this.state.newMessage}
        />
        
      </>
    );
  }
}

export default ChatScreen;
