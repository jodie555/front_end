import React from "react";
// import { BrowserRouter, Switch, Route } from "react-router-dom";
import PropTypes from "prop-types";
import NavbarDrawer from "../../NavbarDrawer/NavbarDrawer.js";
import Copyright from "../../Copyright/Copyright.js";
import { createTheme, ThemeProvider, makeStyles, withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";


import { withRouter } from 'next/router'

const styles = (theme) => ({
  root: {
    display: "flex",
  },
  title: {
    flexGrow: 1,
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    
  },
  paper: {
    padding: theme.spacing(1),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
    maxHeight: "100%",  
    backgroundColor: "white",
  },
  fixedHeight: {
    height: 240,
  },
});

const theme = createTheme({
  palette: {
    primary: {
      light: "##bfefd3",
      main: "#2ecc71",
      dark: "#2ecc71",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ff7961",
      main: "#f44336",
      dark: "#ba000d",
      contrastText: "#000",
    },
  },
});

class OpenNewChatRoom extends React.Component {

  constructor(props){
    super(props);

  }

  componentDidMount(){

  }

  render() {
    console.log("OpenNewChatRoom ",this.props.router.query)
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <ThemeProvider theme={theme}>
      <CssBaseline />
      <NavbarDrawer notShowSwitchBar={true}/>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Grid container spacing={3}>
            {/* Requests */}
            <Grid item xs={12}>
              <Paper className={classes.paper}>
                {this.props.children}
              </Paper>
            </Grid>
          </Grid>
          <Box pt={4}>
            <Copyright />
          </Box>
        </Container>
      </main>
      </ThemeProvider>
    </div>
      
    )
  }
}

OpenNewChatRoom.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withRouter(withStyles(styles)(OpenNewChatRoom))

//To retain props upon refreshing
export async function getServerSideProps(context) {
  return {
    props: {}, // will be passed to the page component as props
  };
}