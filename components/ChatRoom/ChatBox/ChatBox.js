import React from "react";
import { ListItem } from "@material-ui/core";
import Item from "../ChatItem/ChatItem"

class ChatBox extends React.Component {

  //this ChatBox will show all the messages from that conversation room
  //

  constructor(props) {
    super(props);
    this.state = {
        newMessage: '',
        messages: [],
        loadingState: 'initializing',
        boundConversations: new Set(),
    };
  }

  loadMessagesFor = (thisConversation) => {
    if (this.props.conversationProxy === thisConversation) {
        // get all the messages in the conversation room
        thisConversation.getMessages()
            .then(messagePaginator => {
                console.log('messagePaginator',messagePaginator)
                if(messagePaginator !== undefined ){
                  if (this.props.conversationProxy === thisConversation) {
                    this.setState({ messages: messagePaginator.items, loadingState: 'ready' });
                }
                }

            })
            .catch(err => {
                console.error("Couldn't fetch messages IMPLEMENT RETRY", err);
                this.setState({ loadingState: "failed" });
            });
    }
  };

  componentDidMount = () => {
      if (this.props.conversationProxy) {
        this.loadMessagesFor(this.props.conversationProxy);

        if (!this.state.boundConversations.has(this.props.conversationProxy)) {
            let newConversation = this.props.conversationProxy;
            newConversation.on('messageAdded', m => this.messageAdded(m, newConversation));
            this.setState({boundConversations: new Set([...this.state.boundConversations, newConversation])});
        }
      }
      this.scrollToBottom();
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.conversationProxy !== prevProps.conversationProxy) {
        this.loadMessagesFor(this.props.conversationProxy);

        if (!this.state.boundConversations.has(this.props.conversationProxy)) {
            let newConversation = this.props.conversationProxy;
            newConversation.on('messageAdded', m => this.messageAdded(m, newConversation));
            this.setState({boundConversations: new Set([...this.state.boundConversations, newConversation])});
        }
    }
    this.scrollToBottom();
  };

  messageAdded = (message, targetConversation) => {
    if (targetConversation === this.props.conversationProxy)
        this.setState((prevState, props) => ({
            messages: [...prevState.messages, message]
        }));
  };

  scrollToBottom = () => {
    //https://stackoverflow.com/questions/11039885/scrollintoview-causing-the-whole-page-to-move
    this.messagesEnd.scrollIntoView({ behavior: "smooth" ,block: 'nearest', inline: 'start'});
    
   };


  render() {
    const { messages } = this.state


    return (
      <div>
        {messages && messages.map(m => {
            return (
              <Item 
              message={m}
              email={this.props.email}
              />
          )
          })}
          <div //style={{ float:"left", clear: "both" }}
                ref={(el) => { this.messagesEnd = el; }}>
          </div>

      </div>
    );
  }
}

export default ChatBox;