import React from "react";
import Button from "@material-ui/core/Button";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

const useStyles = makeStyles((theme) => ({
  endButton: {
    backgroundColor: "#f64747",
    maxWidth: "150px",
    minWidth: "150px",
    "&:hover": {
      background: "#f64747",
    },
  },
  cancelButton: {
    backgroundColor: "#cccccc",
    maxWidth: "150px",
    minWidth: "150px",
    "&:hover": {
      background: "#cccccc",
    },
  },
  dialogueActions: {
    transform: "translate(-24%, 0%)",
  },
}));

export default function AlertDialog(props) {
  const classes = useStyles();
  return (
    <div>
      <Dialog
        open={props.open}
        onClose={props.handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Are you sure to end the chat room now?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Ending the chat room means you will not be able to continue this
            conversation anymore, and the service from the mentor is going to
            end.
          </DialogContentText>
        </DialogContent>
        <DialogActions className={classes.dialogueActions}>
          <Button
            variant="contained"
            className={classes.cancelButton}
            onClick={props.cancelAction}
            color="primary"
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            className={classes.endButton}
            onClick={props.mainAction}
            color="primary"
            autoFocus
          >
            End Chatroom
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
