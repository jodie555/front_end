import React, { useState, useEffect } from "react";
import axiosDefault from "../../../utils/axios"
const axios = axiosDefault.axiosDefault()
import Router from "next/router";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import { Row, Column, Item } from "@mui-treasury/components/flex";
import Typography from "@material-ui/core/Typography";

import Rating from "@material-ui/lab/Rating";
import AlertBox from "./AlertBox.js";
import StartCallButton from "../../../components/Buttons/StartCallButton/StartCallButton"
import EndChatRoomButton from "../../../components/Buttons/EndChatRoomButton/EndChatRoomButton"

import routeToDashboard from "../../../functions/routings/defaultRoute.js";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#eeeeee", //#6bb9f0
  },
  title: {
    color: "#2ecc71",
    fontWeight: "bold",
  },
  content: {
    flex: "1 0 auto",
  },
  avatarImage: {
    paddingLeft: theme.spacing(3),
    alignItems: "center",
  },
  deleteIcon: {
    height: 38,
    width: 38,
  },
  endRoomButton: {
    backgroundColor: "white",
    color: "#f64747",
    "&:hover": {
      backgroundColor: "#cccccc",
      color: "#f64747",
      borderColor: "#f64747",
    },
    borderColor: "#f64747",
    borderStyle: "solid",
    borderRadius: "7",
  },
}));

// export default React.memo(function TutorCard(props) {


const App = (props:any) => {  
  console.log("propsss",props)
  const [openDialogue, setOpenDialogue] = React.useState(false);
  const [ requestId,setRequestId ] = useState(props.requestId)
  const [requestInfo,setRequestInfo] = useState({
    chatTitle: "",
    requestStatus: 0,
    supplierId: "",
    supplierImageURL: "",
    supplierName: "",
    supplierUniversity: "",
    supplierMajor: "",

  })
  const [ buttonDisable, setButtonDisable] = useState(true)

  const classes = useStyles();

  const handleCloseDialogue = () => {
    setOpenDialogue(false);
  };

  const endChatRoomPopUp = async (event:any) => {
    setOpenDialogue(true);
  };
  

  const endChatroom = async (event:any) => {
    //axios set request status to 2 with props.requestId
    const result = await axios.patch(
      "/api/requests/" + props.requestId + "/end-request"
    );
    //route to dashboard
    console.log(result);
    if (result.data.status === true) {
      routeToDashboard(2);
    }
  };

  useEffect(()=>{
    if(props.selectedConversationSID){
      axios.get(
        "/api/requests/" +
        props.selectedConversationSID +
          "/chatroom-info"
      ).then((response:any) => {
        setRequestInfo({
          ...requestInfo,
          chatTitle:response.data.description,
          requestStatus: response.data.status,
          supplierId: response.data.supplierId._id,
          supplierImageURL: response.data.supplierId.avatarUrl,
          supplierName: response.data.supplierId.firstName,
          supplierUniversity: response.data.supplierId.universityIdentities[0].university.name,
          supplierMajor: response.data.supplierId.universityIdentities[0].university.name,
    
        })
      })

    }
  },[props.selectedConversationSID])


  const buttonInfo = {
    requestId:requestId,
    supplierId:requestInfo.supplierId,
    disabled:buttonDisable
  }

  return (
    <div >
      <Row p={1.5} gap={1} className={classes.root} borderRadius={8}>
        <Column position={"middle"}>
          <Item ml={1}>
            <div className={classes.avatarImage}>
              <Avatar src={requestInfo.supplierImageURL} />
            </div>
            <Rating
              name="size-small"
              value={props.rating}
              precision={0.5}
              size="small"
              readOnly
            />
          </Item>
        </Column>

        <Column gap={0.5}>
          <Item ml={1} position={"middle"}>
            <Row gap={0.1}>
              <Typography component="h5" variant="h5" className={classes.title}>
                {requestInfo.supplierName}, {requestInfo.supplierMajor}
              </Typography>
            </Row>
          </Item>
          <Item ml={1} position={"middle"}>
            <Row gap={0.1}>
              <Item>
                <Typography
                  variant="subtitle1"
                  color="textSecondary"
                  className={classes.title}
                >
                  {requestInfo.supplierUniversity}
                </Typography>
              </Item>
            </Row>
          </Item>
        </Column>

        <Column position={"right"}>
            <Item ml={1} position={"middle"}>
              <StartCallButton {...buttonInfo}/>
            </Item>
          </Column>
          <Column position={"right"}>
            <Item ml={1} position={"middle"}>
              <EndChatRoomButton endChatRoomPopUp = {endChatRoomPopUp}/>
            </Item>
          </Column>
      </Row>
      <div style={{ height: "10px" }}></div>

      {/* pop up box after clicking end chat button */}
      <AlertBox
        open={openDialogue}
        mainAction={endChatroom}
        cancelAction={handleCloseDialogue}
      />
    </div>
  );
}
export default App;