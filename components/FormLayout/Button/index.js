import React from 'react';
import {
  Button,
} from "@material-ui/core";
import { makeStyles, withStyles } from "@material-ui/core/styles";

// import './Input.css';


const useStyles = makeStyles((theme) => ({

  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));



export default function formButton(props) {
  const classes = useStyles();
  return <Button
  variant="contained"
  color="primary"
  type="submit"
  size="large"
  fullWidth
  onClick={props.onSubmit}
  className={classes.submit}
>
  {props.content}
</Button>

  }

