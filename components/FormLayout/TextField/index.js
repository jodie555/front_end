import React from 'react';
import {
  TextField,
} from "@material-ui/core";
// import './Input.css';



export default function formTextField(props) {
  return (<TextField
  variant="outlined"
  margin="normal"
  required={props.required}
  fullWidth
  type={props.type}
  id={props.id}
  name={props.name}
  label={props.label}
  value={props.value}
  onChange={props.onChange}
  error={props.error}
  helperText={props.error}
  autoComplete={props.autoComplete}
  autoFocus={props.autoFocus}
/>)

  }

