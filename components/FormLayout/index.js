import React, { useState, useEffect } from "react";
import { Typography, Paper, Link, Grid, Button, Box } from "@material-ui/core";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
// import DatePicker from '@material-ui/lab/DatePicker';
import AutocompleteCustom from "../AutoComplete/AutoComplete.js";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Copyright from "../Copyright/Copyright.js";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import CssBaseline from "@material-ui/core/CssBaseline";
import styles from "./FormLayout.module.css"



class AddIdentityForm extends React.Component {
  constructor(props) {
    super(props);

  }


  // }
  render() {
    const {  errors, stylesClassName, title, children } = this.props;
    // const { errors } = this.state;


    return (
    <div>
      <div className={stylesClassName}>
        {errors.message && <p style={{ color: "red" }}>{errors.message}</p>}


        <Container component="main" maxWidth="xs">

        {/* <div className={styles.paper}> */}
          <Typography component="h1" variant="h5" className={styles.title}>
              {title}
          </Typography>
            <Grid
              container
              spacing={3}
              direction="row"
              justifyContent="center"
              alignItems="center"
            >
            <form className={styles.form} >
              {children}
            </form>
          </Grid>
        {/* </div> */}
        <Box mt={8}>
          <Copyright />
        </Box>
      </Container>
    </div>
  </div>
    );
  }
}

// export default withStyles(styles)(AddIdentityForm);
export default AddIdentityForm;
