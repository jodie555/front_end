import React, {useState, useEffect} from "react";
import Rating from '@material-ui/lab/Rating';
import RequestCardLayout from "../RequestCardLayout/RequestCardLayout";
import Avatar from '@material-ui/core/Avatar';
import { Row} from '@mui-treasury/components/flex';
import BeginChatButton from '../../Buttons/BeginChatButton/BeginChatButton'
import { useTutorInfoStyles } from '@mui-treasury/styles/info/tutor';
import { Info, InfoTitle, InfoSubtitle } from '@mui-treasury/components/info';
import { useDynamicAvatarStyles } from '@mui-treasury/styles/avatar/dynamic';

function RequestCardSupplierInfo(props){


    const avatarStyles = useDynamicAvatarStyles({ radius: 12, size: 48 });
    const middleContent = {
        // firstTitle: "Request Inquiry",
        // firstContent: request.description,
        secondTitle: "Mentor University",
        secondContent: props.supplier.university,
        thirdTitle: "Mentor Major",
        thirdContent: props.supplier.course,
    }

    return (<RequestCardLayout
        leftComponents={
            <>
                <Row>
                <Avatar
                classes={avatarStyles}
                src={process.env.BACKEND_URL+"/avatar01.png"}
                // src={props.supplierImageURL}
                />
                <Info position={'middle'} useStyles={useTutorInfoStyles}>
                <InfoTitle>{props.supplier.firstName} </InfoTitle>       
                </Info>
            </Row>
            <Rating
                name="size-large"
                value={4}
                readOnly
                // defaultValue={props.supplierRating}
                // defaultValue={props.supplierRating}
                // precision={0.5}
                // emptyIcon={<StarBorderIcon fontSize="inherit" />}
            />
            </>}
        middleContent={middleContent}
        rightComponents={
            <>
                <BeginChatButton 
                supplierId={props.supplier.supplierId}
                // requestId={request.requestId} 
                />  
            </>
        }
        />)

  }
  
  export default RequestCardSupplierInfo;