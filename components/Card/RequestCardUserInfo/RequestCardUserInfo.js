import React, {useState, useEffect} from "react";
import Rating from '@material-ui/lab/Rating';
import RequestCardLayout from "../RequestCardLayout/RequestCardLayout";
import Avatar from '@material-ui/core/Avatar';
import { Row, Column, Item } from '@mui-treasury/components/flex';
import JoinChatButton from '../../Buttons/JoinChatButton/JoinChatButton'
import { useTutorInfoStyles } from '@mui-treasury/styles/info/tutor';
import { Info, InfoTitle, InfoSubtitle } from '@mui-treasury/components/info';
import { useDynamicAvatarStyles } from '@mui-treasury/styles/avatar/dynamic';

function RequestCardUserInfo(props){

    const avatarStyles = useDynamicAvatarStyles({ radius: 12, size: 48 });
    const middleContent = {
        // firstTitle: "Request Inquiry",
        // firstContent: request.description,
        secondTitle: "User Name",
        secondContent: props.user.userName,
        // thirdTitle: "User email",
        // thirdContent: props.user.email,
    }

    return (<RequestCardLayout
        leftComponents={
            <>
                <Row>
                <Avatar
                classes={avatarStyles}
                src={process.env.BACKEND_URL+"/avatar01.png"}
                // src={props.supplierImageURL}
                />
                <Info position={'middle'} useStyles={useTutorInfoStyles}>
                <InfoTitle>{props.user.userName} </InfoTitle>       
                </Info>
            </Row>

            </>}
        middleContent={middleContent}
        rightComponents={
            <>
                <JoinChatButton 
                userId={props.user.userId}
                chatroomId={props.user.chatroomId}
                requestId={props.user.requestId} 
                />  
            </>
        }
        />)

  }
  
  export default RequestCardUserInfo;