import * as React from 'react';

import Typography from '@material-ui/core/Typography'
import { Row, Column, Item } from '@mui-treasury/components/flex';

export default function UniversityIdentityCardLayout(props){
    const {university, course, entryYear,editButton} = props;
    console.log("new layouts")
    return (
        <>
        <Row p={1.5} gap={2} bgcolor={'#f5f5f5'} borderRadius={16}>
        <Column gap={1.0}>
            <Item ml={1} position={'middle'}>
              <Row gap={1.5}>
                <Item>
                  <Typography color="primary" variant="caption">
                    <b style={{fontSize: "15px"}}>university :</b>
                  </Typography>
                </Item>
                <Item>
                  <Typography className="request-card-description">
                    {university}           
                  </Typography> 
                </Item>
              </Row>
            </Item>
            <Item ml={1} position={'middle'}>
              <Row gap={1.5}>
                <Item>
                  <Typography color="primary" variant="caption">
                    <b style={{fontSize: "15px"}}>course :</b>
                    </Typography>
                </Item>
                <Item>
                  <Typography>
                    {course}
                  </Typography>
                </Item>
              </Row>
            </Item>  
            <Item ml={1} position={'middle'}>
              <Row gap={1.5}>
                <Item>
                  <Typography color="primary" variant="caption">
                    <b style={{fontSize: "15px"}}>Entry Year :</b>
                  </Typography>
                </Item>
                <Item>
                  <Typography>{entryYear}</Typography>
                </Item>
              </Row>
            </Item>
            
          </Column>

          <Column position={'right'}>
            {editButton}
          </Column>
        </Row>
        <div style={{height: "10px"}}>

        </div>
        {/* <Divider variant="middle"/> */}
      </>
    )
}