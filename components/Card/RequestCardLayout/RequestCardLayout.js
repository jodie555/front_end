import * as React from 'react';

import Typography from '@material-ui/core/Typography'
import { Row, Column, Item } from '@mui-treasury/components/flex';

export default function RequestCardLayout(props){
    const {leftComponents, middleContent, rightComponents} = props;
    console.log("new layouts")
    return (
        <>
        <Row p={1.5} gap={2} bgcolor={'#f5f5f5'} borderRadius={16}>
          <Column position={'middle'}>
            <Item ml={1}>
                {leftComponents}
            </Item>
          </Column>

          <Column gap={1.0}>
            <Item ml={1} position={'middle'}>
              <Row gap={1.5}>
                <Item>
                  <Typography color="primary" variant="caption">
                    <b style={{fontSize: "15px"}}>{middleContent.firstTitle}</b>
                  </Typography>
                </Item>
                <Item>
                  <Typography className="request-card-description">
                    {middleContent.firstContent}           
                  </Typography> 
                </Item>
              </Row>
            </Item>
            <Item ml={1} position={'middle'}>
              <Row gap={1.5}>
                <Item>
                  <Typography color="primary" variant="caption">
                    <b style={{fontSize: "15px"}}>{middleContent.secondTitle}</b>
                    </Typography>
                </Item>
                <Item>
                  <Typography>{middleContent.secondContent} </Typography>
                </Item>
              </Row>
            </Item>  
            <Item ml={1} position={'middle'}>
              <Row gap={1.5}>
                <Item>
                  <Typography color="primary" variant="caption">
                    <b style={{fontSize: "15px"}}>{middleContent.thirdTitle}</b>
                  </Typography>
                </Item>
                <Item>
                  {/* <Typography>{middleContent.thirdContent}</Typography> */}
                </Item>
              </Row>
            </Item>
            
          </Column>

          <Column position={'right'}>
            {rightComponents}
          </Column>
        </Row>
        <div style={{height: "10px"}}>

        </div>
        {/* <Divider variant="middle"/> */}
      </>
    )
}