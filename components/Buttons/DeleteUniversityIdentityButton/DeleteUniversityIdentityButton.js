import React, { useState } from "react";
import { Button } from "@material-ui/core";
import Router from "next/router";
import { useRouter } from 'next/router'
import axiosDefault from "../../../utils/axios";
import deleteUniversityIdentityById from '../../../functions/deleteUniversityIdentityById/deleteUniversityIdentityById.js';


const axios = axiosDefault.axiosDefault()

function BeginChatButton(props) {
  const [isDisabled, setIsDisabled] = useState(false);

  const router = useRouter()

  const universityidentityId = props.universityidentityId

  const deleteUniversityIdentity = async (event) => {
    if(universityidentityId!== ""){
      deleteUniversityIdentityById(universityidentityId)
      console.log("deleted")
      router.reload(window.location.pathname)

    }

  };

  return (

    <Button
      variant="contained"
      color="primary"
      type="submit"
      onClick={deleteUniversityIdentity}
    >
      Delete
    </Button>
  );
}

export default BeginChatButton;