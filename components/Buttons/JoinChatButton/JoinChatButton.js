import React, { useState } from "react";
import { Button } from "@material-ui/core";
import Router from "next/router";
import axiosDefault from "../../../utils/axios";

import LoadingComponent from '../../Loading/Loading'

const axios = axiosDefault.axiosDefault()

function BeginChatButton(props) {
  const [isDisabled, setIsDisabled] = useState(false);
  const [loading, setLoading] = useState(false)

  const routeToChatroom = async (event) => {
    setLoading(true)
    //Create a new request
    let response = await axios.get(`/api/chatrooms/supplier/`+props.chatroomId);



    Router.push(
      {
        pathname: "/supplierChatRoom",
        query: {
          conversationSID: response.data.conversationSID,
          requestId: props.requestId,
          isUser: false,
        },
      },
      "/supplierChatRoom"
    );


  };

  return (

        <div>
        {loading?LoadingComponent():(
        <Button
        color="primary"
        variant="contained"
        disabled={isDisabled}
        onClick={routeToChatroom}
        >
        Chat
        </Button>
        )}
        </div>
  );
}

export default BeginChatButton;