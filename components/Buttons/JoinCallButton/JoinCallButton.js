
import React, { useState } from 'react';

import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import Router from "next/router";


function JoinCallButton(props) {

  const handleJoinCall = async (event) => {
    Router.push(
      {
        pathname: '/newvideo',
        query: {
          requestContentState: 1, //go to the ongoing requests tab
          supplierId: props.supplierId,
          requestId: props.requestId,
          userId: props.userId,
          client: "supplier",
        },
      }
        , '/newvideo'
      )


    }
  
  
    return (
      <Button
      variant="outlined"
      color="primary"
      startIcon={<DeleteIcon />}
      onClick={handleJoinCall}
      disabled={props.isDisabled}

    >
      Join Call
    </Button>
    );
  }
  
  export default JoinCallButton;