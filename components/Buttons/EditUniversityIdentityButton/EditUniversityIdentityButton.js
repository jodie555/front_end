import React, { useState } from "react";
import { Button } from "@material-ui/core";
import Router from "next/router";
import axiosDefault from "../../../utils/axios";
const axios = axiosDefault.axiosDefault()

function BeginChatButton(props) {
  const [isDisabled, setIsDisabled] = useState(false);

  const routeToEditUniversityidentity = async (event) => {
    console.log("props",props)


    Router.push(
      {
        pathname: "/supplier/edit-identity",
        query: {
          universityidentityId: props.universityidentityId,
        },
      }
    );


  };

  return (
    <Button
      color="primary"
      variant="contained"
      disabled={isDisabled}
      onClick={routeToEditUniversityidentity}
    >
      Edit
    </Button>
  );
}

export default BeginChatButton;