
import React, { useState } from 'react';

import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Router from "next/router";
const useStyles = makeStyles((theme) => ({
  startCallButton:{

  }
}));

function StartCallButton(props) {
    const classes = useStyles();

    const handleStartCall = async (event) => {
    Router.push(
      {
        pathname: '/newvideo',
        query: {
          requestContentState: 1, //go to the ongoing requests tab
          supplierId: props.supplierId,
          requestId: props.requestId,
          userId: props.userId,
          client: "user",
        },
      }
        , '/newvideo'
      )


    }
  
  
    return (
      <Button
      variant="outlined"
      color="primary"
      className={classes.startCallButton}
      startIcon={<DeleteIcon />}
      onClick={handleStartCall}
    >
      Start Call
    </Button>
    );
  }
  
  export default StartCallButton;