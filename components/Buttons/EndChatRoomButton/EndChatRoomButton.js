
import React, { useState } from 'react';

import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import { makeStyles, useTheme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  endRoomButton:{

  }
}));

function EndChatRoomButton(props) {
    const classes = useStyles();
    // const [openDialogue, setOpenDialogue] = React.useState(false);

    // const endChatRoomPopUp = async (event) => {
    //   setOpenDialogue(true);
    // };
  
  
    return (
      <Button
      variant="outlined"
      color="primary"
      className={classes.endRoomButton}
      startIcon={<DeleteIcon />}
      onClick={props.endChatRoomPopUp}
      // disabled={props.disabled}
    >
      End Chatroom
    </Button>
    );
  }
  
  export default EndChatRoomButton;