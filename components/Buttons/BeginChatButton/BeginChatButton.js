import React, { useState } from "react";
import { Button } from "@material-ui/core";
import Router from "next/router";
import axiosDefault from "../../../utils/axios";
const axios = axiosDefault.axiosDefault()


import LoadingComponent from '../../Loading/Loading'


function BeginChatButton(props) {
  const [isDisabled, setIsDisabled] = useState(false);
  const [loading, setLoading] = useState(false)


  const routeToChatroom = async (event) => {
    setLoading(true)
    //Create a new request
    let response = await axios.post(`/api/requests/user`, {supplierId: props.supplierId});
    let chatroomResponse;

    let conversationSID;
    console.log("response.data.request",response)
    if(response.data.request){
      if (response.data.request.chatroomId){
        chatroomResponse = await axios.get(`/api/get_conversationId/`+response.data.request.chatroomId);
        conversationSID = chatroomResponse.data.conversationSID;
      }else{
        conversationSID = null;
      }
    }

    if (!conversationSID) {
      console.log("dd")
      const result = await axios.post(`/api/create_conversation/request`, {
        requestId: response.data.request._id,
      });
      conversationSID = result.data.conversationSID;
    }

    Router.push(
      {
        pathname: "/userChatRoom",
        query: {
          conversationSID: conversationSID,
          requestId: response.data.request._id,
          isUser: true,
        },
      },
      "/userChatRoom"
    );


  };

  return (
    <div>
    {loading?LoadingComponent():(
    <Button
      color="primary"
      variant="contained"
      disabled={isDisabled}
      onClick={routeToChatroom}
    >
      Chat
    </Button>)}
    </div>
  );
}

export default BeginChatButton;