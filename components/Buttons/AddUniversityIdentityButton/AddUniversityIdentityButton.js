import React, { useState } from "react";
import { Button } from "@material-ui/core";
import Router from 'next/router'



function AddUniversityIdentityButton(props) {

  const addUniversityIdentity = async (event) => {

    Router.push(
      {
        pathname: '/supplier/add-identity',

      }
      )
  };

  return (

      <Button
      variant="contained"
      color="primary"
      type="submit"
      onClick={addUniversityIdentity}

    >
      Add University Identity
    </Button>
  );
}

export default AddUniversityIdentityButton;