import React, { Component } from "react";

export default class CenterWrapper extends Component {
    render() {
        return (
            <div style={{ width: "100%", height: "100%", display: "flex", justifyContent: "center", alignItems: "center" }}>
                <div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}
