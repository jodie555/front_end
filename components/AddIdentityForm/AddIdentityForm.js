import React, { useState, useEffect } from "react";
import axios from "../../util/api";
import { Typography, Paper, Link, Grid, Button, Box } from "@material-ui/core";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
// import DatePicker from '@material-ui/lab/DatePicker';
import AutocompleteCustom from "../AutoComplete/AutoComplete.js";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
// import styles from './CreateRequestForm.module.css';
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Copyright from "../Copyright/Copyright.js";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import { validateUniversityIdentity } from "../../functions/validation/universityIdentity/validate";
import Router from "next/router";

const styles = (theme) => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    width: "60%",
    // overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "60%",
    margin: "0.7rem",
  },
  title: {
    color: "#2ecc71",
  },
  inputLabel: {
    [theme.breakpoints.down("sm")]: {},
  },
  dropDown: {
    [theme.breakpoints.down("sm")]: {
      minWidth: "300px",
      marginBottom: "2.5rem",
    },
    [theme.breakpoints.up("lg")]: {
      minWidth: "300px",
      marginBottom: "2.5rem",
    },
  },
  descriptionField: {
    [theme.breakpoints.down("sm")]: {
      minWidth: "300px",
      marginBottom: "3.5rem",
    },
    [theme.breakpoints.up("lg")]: {
      minWidth: "500px",
      marginBottom: "2.5rem",
    },
  },
  submitButton: {
    margin: theme.spacing(3, 20, 2),
  },
});

class AddIdentityForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {
        university: "",
        subject: "",
        course: "",
        entryYear: "",
        verificationEmail: "",
        documentProof: "",
      },
      universitiesOptions: [""],
      subjectsOptions: [""],
      isChooseCourseDisabled: true,
      coursesOptions: [""],
      errors: {},
    };
  }

  async componentDidMount() {
    const universityData = await axios.get(`/api/universities`).catch((e) => {
      return e;
    });
    const subjectData = await axios.get(`/api/subjects`).catch((e) => {
      return e;
    });

    this.setState({
      universitiesOptions: universityData.data, //.map((uni) => {
      // return (
      //   <option key={uni.name} value={uni._id}>
      //     {uni.name}
      //   </option>
      // );
      // }),
      subjectsOptions: subjectData.data, //.map((sub) => {
      //   return (
      //     <option key={sub.name} value={sub._id}>
      //       {sub.name}
      //     </option>
      //   );
      // }),
    });
  }

  async componentDidUpdate(prevProps, prevState) {
    // if (prevState.user.university !== this.state.user.university ){
    if (
      prevState.user.university !== this.state.user.university ||
      prevState.user.subject !== this.state.user.subject
    ) {
      if (this.state.user.university && this.state.user.subject) {
        this.setState({
          isChooseCourseDisabled: false,
        });
        console.log(
          "inside update course field disabled:",
          this.state.isChooseCourseDisabled
        );
        const courseData = await axios
          .get(
            `/api/courses/?university=` +
              this.state.user.university +
              `&subject=` +
              this.state.user.subject
          )
          .catch((e) => {
            return e;
          });
        console.log("CourseData", courseData);

        this.setState({
          coursesOptions: courseData.data,
        });
      }
    }
    // }
  }

  submitUniversityIdentity = async (user) => {
    var bodyFormData = new FormData();
    user.university && bodyFormData.append("university", user.university);
    user.subject && bodyFormData.append("subject", user.subject);
    user.course && bodyFormData.append("course", user.course);
    user.verificationEmail &&
      bodyFormData.append("verificationEmail", user.verificationEmail);
    user.documentProof &&
      bodyFormData.append("documentProof", user.documentProof);
    user.entryYear && bodyFormData.append("entryYear", user.entryYear);

    // console.log("token",this.state.token)
    axios
      .post("/api/universityIdentities", bodyFormData)
      .then((res) => {
        console.log("res.data", res.data);
        if (res.data.status === true) {
          // routeToNextPage(res.data.messageStatus);
          Router.push(
            {
              pathname: "/supplier/profile",
  
            }
          );
        } else {
          this.setState({
            errors: { message: res.data.message },
          });
        }

      })
      .catch((err) => {
        // console.log("Sign up data submit error: ", err);
        this.setState({
          errors: err.response.data,
        });
      });
  };

  submitForm = (event) => {
    event.preventDefault();
    var payload = validateUniversityIdentity(this.state.user);
    if (payload.success) {
      this.setState({
        errors: {},
      });
      var user = {
        university: this.state.user.university,
        subject: this.state.user.subject,
        course: this.state.user.course,
        verificationEmail: this.state.user.verificationEmail,
        documentProof: this.state.user.documentProof,
        entryYear: this.state.user.entryYear,
      };
      this.submitUniversityIdentity(user);
    } else {
      this.state.errors = payload.errors;
    }
  };

  handleChange = (event) => {
    const name = event.target.name;
    const user = this.state.user;

    if (event.target.files) {
      const { name, files } = event.target;
      user[name] = files[0];
    } else {
      user[name] = event.target.value;
    }

    this.setState({
      user,
    });
  };

  handleAutocompleteChange = (user, name, newValue) => {
    this.setState({
      user: {
        ...user,
        [name]: newValue._id,
      },
    });
  };

  // }
  render() {
    const { classes } = this.props;
    const { errors } = this.state;
    return (
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        {errors.message && <p style={{ color: "red" }}>{errors.message}</p>}


                <Typography
                  className={classes.title}
                  variant="h4"
                  align="center"
                  component="h1"
                  gutterBottom
                >
                  Add New University Identity
                </Typography>
                <form className={classes.form} encType="multipart/form-data">
                  <Grid container>
                    <Grid item xs={12}>
                      <AutocompleteCustom
                        name={"university"}
                        label={"University"}
                        options={this.state.universitiesOptions}
                        // getOptionLabel={(option) => option.name ? option.name : ""}
                        // getOptionLabel={(option) => console.log("option",option)}

                        user={this.state.user}
                        error={this.state.errors.university}
                        value={this.state.user.university}
                        onChange={this.handleAutocompleteChange}
                        disabled={false}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <AutocompleteCustom
                        name={"subject"}
                        label={"Subject"}
                        options={this.state.subjectsOptions}
                        user={this.state.user}
                        error={this.state.errors.subject}
                        value={this.state.user.subject}
                        onChange={this.handleAutocompleteChange}
                        disabled={false}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <AutocompleteCustom
                        name={"course"}
                        label={"Course"}
                        options={this.state.coursesOptions}
                        user={this.state.user}
                        error={this.state.errors.course}
                        value={this.state.user.course}
                        onChange={this.handleAutocompleteChange}
                        disabled={this.state.isChooseCourseDisabled}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        name="verificationEmail"
                        label="Verification Email"
                        value={this.state.user.verificationEmail}
                        onChange={this.handleChange}
                        error={this.state.errors.verificationEmail}
                        helperText={this.state.errors.verificationEmail}
                        required
                        // fullWidth
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <TextField
                        name="entryYear"
                        label="Entry Year"
                        value={this.state.user.entryYear}
                        onChange={this.handleChange}
                        error={this.state.errors.entryYear}
                        helperText={this.state.errors.entryYear}
                        required
                        // fullWidth
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Button
                        variant="contained"
                        component="label"
                        className={classes.uploadButton}
                        startIcon={<CloudUploadIcon />}
                        onChange={this.handleChange}
                      >
                        Upload Document Proof
                        <input
                          accept="image/*"
                          name="documentProof"
                          id="raised-button-file"
                          type="file"
                        />
                      </Button>
                    </Grid>

                    <Button
                      variant="contained"
                      color="primary"
                      type="submit"
                      onClick={this.submitForm}
                      className={classes.submitButton}
                    >
                      Submit
                    </Button>
                  </Grid>
                </form>
      </main>
    );
  }
}

export default withStyles(styles)(AddIdentityForm);
