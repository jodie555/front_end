import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import { Row, Item } from '@mui-treasury/components/flex';
import { Info, InfoTitle, InfoSubtitle } from '@mui-treasury/components/info';
import { useTutorInfoStyles } from '@mui-treasury/styles/info/tutor';
import { useSizedIconButtonStyles } from '@mui-treasury/styles/iconButton/sized';
import { useDynamicAvatarStyles } from '@mui-treasury/styles/avatar/dynamic';
import Divider from '@material-ui/core/Divider';


import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import AcceptSupplierButton from "../../AcceptSupplierButton/AcceptSupplierButton.js";
// Reference: https://mui-treasury.com/components/card/

const useStyles = makeStyles(() => ({
    action: {
        backgroundColor: '#fff',
        boxShadow: '0 1px 4px 0 rgba(0,0,0,0.12)',
        '&:hover': {
        backgroundColor: '#fff',
        color: '#000',
        },
    },
    }));

export default React.memo(function TutorCard(props) {
    const styles = useStyles();
    const iconBtnStyles = useSizedIconButtonStyles({ padding: 6 });
    const avatarStyles = useDynamicAvatarStyles({ radius: 12, size: 48 });
    console.log("chattitle",props.chatTitle)
    return (
        <Row p={1.5} gap={2} bgcolor={'#f5f5f5'} borderRadius={16}>
        <Item>
            <Avatar
            classes={avatarStyles}
            src={props.imageURL}
            />
            <Rating
            name="size-small"
            value={props.rating}
            precision={0.5}
            size="small"
            />
        </Item>
        <Info position={'middle'} useStyles={useTutorInfoStyles}>
            <InfoTitle>{props.firstName}</InfoTitle>
            <InfoSubtitle>{props.university}</InfoSubtitle>
            <InfoSubtitle>{props.universityMajor}</InfoSubtitle>
        </Info>
        <Item ml={1} position={'middle'}>
             <AcceptSupplierButton 
             supplierId={props.supplerId}
             requestId={props.requestId}
             />
        </Item>
        </Row>

    );
});