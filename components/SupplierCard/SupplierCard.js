import React from "react";
import BeginChatButton from "../BeginChatButton/BeginChatButton.js";
import CheckOutButton from "../CheckOutButton/CheckOutButton.js";
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import StarBorderIcon from '@material-ui/icons/StarBorder';

function SupplierCard(props){
    return (
        <div className="supplier-card-container">
            <div className="supplier-card-left">
                <div className="supplier-card header section">
                    <div className="supplier-card header image">
                        <img alt="Alan Park" src={props.imageURL} />
                    </div>
                    <div className="supplier-card header info">
                        <div className="supplier-card name">
                            {props.firstName} 
                        </div>
                        <div className="supplier-card rating">
                            <Box component="fieldset" mb={3} borderColor="transparent">
                                <Rating
                                    name="size-large"
                                    defaultValue={props.rating}
                                    precision={0.5}
                                    emptyIcon={<StarBorderIcon fontSize="inherit" />}
                                />
                            </Box>
                        </div>
                    </div>
                </div>
            </div>
            <div className="supplier-card-center">               
                <div className="supplier-card-university">
                    {props.university}
                </div>
                <div className="supplier-card-major">
                    {props.major}
                </div>
                <div className="supplier-card section about">
                    {props.introduction}
                </div>
            </div>
            <div className="supplier-card-right">
                <CheckOutButton />
                <BeginChatButton />
            </div>
        </div>
    )
}

export default SupplierCard;