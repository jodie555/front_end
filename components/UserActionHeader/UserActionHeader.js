import React, {useState, useEffect} from 'react';
import Link from 'next/link'


function UserActionHeader(props){
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    useEffect(() => {
        const loggedInStatus = localStorage.getItem("loggedInStatus")
        setIsLoggedIn(loggedInStatus)
    }, [])

    if(!isLoggedIn){
        return (
        <ul className="user-action-header">
            <li>
                <Link href="/register/user" >Sign up</Link>
            </li>
            <li>
                <Link href="/user/login">Log in</Link>
            </li>
         </ul>
         )
    }else{
        return (
        <ul className="user-action-header">
            <li>
                <Link href="/user/logout">
                    Log Out
                </Link>
            </li>
        </ul>
        )
    }
}

export default UserActionHeader;