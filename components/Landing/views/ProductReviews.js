import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ButtonBase from '@material-ui/core/ButtonBase';
import Container from '@material-ui/core/Container';
import Typography from '../components/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';

const styles = (theme) => ({
  root: {
    marginTop: theme.spacing(8),
    marginBottom: theme.spacing(4),
  },
  images: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexWrap: 'wrap',
  },
  imageWrapper: {
    position: 'relative',
    display: 'block',
    padding: 0,
    borderRadius: 0,
    height: '40vh',
    [theme.breakpoints.down('sm')]: {
      width: '100% !important',
      height: 100,
    },
    '&:hover': {
      zIndex: 1,
    },
    '&:hover $imageBackdrop': {
      opacity: 0.15,
    },
    '&:hover $imageMarked': {
      opacity: 0,
    },
    '&:hover $imageTitle': {
      border: '4px solid currentColor',
    },
  },
  imageButton: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: theme.palette.common.white,
  },
  imageSrc: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: 'cover',
    backgroundPosition: 'center 40%',
  },
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    background: theme.palette.common.black,
    opacity: 0.5,
    transition: theme.transitions.create('opacity'),
  },
  imageTitle: {
    position: 'relative',
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px 14px`,
  },
  imageMarked: {
    height: 3,
    width: 18,
    background: theme.palette.common.white,
    position: 'absolute',
    bottom: -2,
    left: 'calc(50% - 9px)',
    transition: theme.transitions.create('opacity'),
  },
  cardRoot: {
    // display: 'flex',
    position: 'relative',
    display: 'block',
    padding: 0,
    borderRadius: 0,
    height: '40vh',
    [theme.breakpoints.down('sm')]: {
      width: '100% !important',
      height: 100,
    },
    opacity: 0.5,
    '&:hover': {
      zIndex: 1,
      opacity: 1,
    },
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  chip:{
    background: theme.palette.primary.main,
    color: "white",
  },
});

function ProductReviews(props) {
  const { classes } = props;

  const images = [
    {
      picture:
        'avatar02.jpg',
      title: 'A Level Student',
      description: 'The most loved featured for me has to be the quick available of the mentors from top universities. Whatever is the question or subject, the mentors are always here to guide us through the application.',
      width: '40%',
    },
    {
      picture:
        'avatar03.jpg',
        title: 'A Level Student',
        description: 'I got accepted into my dream university with help from the mentors!',
        width: '40%',
    },
    {
      picture:
        'avatar04.jpg',
      title: 'Bachelor Student',
      description: 'This app has helped me to connect with current MSc students and let me know about the Master Course I intend to apply to.',
      width: '40%',
    },
    {
      picture:
        'avatar05.jpg',
      title: 'A Level Student',
      description: 'Through this app, I have had several interview sessions with some mentors. They have made me becoming more confident in interviews. Without them, I may not be able to get into my dream subject!',
      width: '38%',
    },
    {
      picture:
        'avatar06.jpg',
      title: 'IB Student',
      description: 'Through this app, I have had several interview sessions with some mentors. They have made me becoming more confident in interviews. Without them, I may not be able to get into my dream subject!',
      width: '38%',
    },
    {
      picture:
        'avatar07.jpg',
      title: 'Parent',
      description: 'Recommend this platform for other parents worrying about the future of their kids!',
      width: '24%',
    }
  ];

  return (
    <Container className={classes.root} component="section">
      <Typography variant="h4" marked="center" align="center" component="h2">
        What Our Users Say
      </Typography>
      <div className={classes.images}>
        {images.map((image) => (
        <Card className={classes.cardRoot} style={{ width: image.width,}}>
          <Avatar alt="Remy Sharp" src={image.picture} className={classes.avatar} />
          <div className={classes.details}>
            <CardContent className={classes.content}>
              <Chip label={image.title} className={classes.chip}/>
              <Typography variant="subtitle1" color="textSecondary">
                {image.description}
              </Typography>
            </CardContent>
          </div>
        </Card>
        ))}
      </div>
    </Container>
  );
}

ProductReviews.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductReviews);