import * as React from 'react';
import PropTypes from 'prop-types';
import Button from '../components/Button';
import { withStyles } from '@material-ui/core/styles';
import Typography from '../components/Typography';
import ProductHeroLayout from './ProductHeroLayout';

// import backgroundImage from "../../../public/landing.png";
  const backgroundImage = "/landing.png";
  const styles = (theme) => ({
    background: {
      // backgroundImage: `url(${backgroundImage})`,
      backgroundImage: `url(${backgroundImage})`,
      // backgroundColor: '#7fc7d9', // Average color of the background image.
      backgroundPosition: 'center',
    },
    button: {
      minWidth: 200,
      maxHeight: 50,
    },
    h5: {
      marginBottom: theme.spacing(4),
      marginTop: theme.spacing(4),
      [theme.breakpoints.up('sm')]: {
        marginTop: theme.spacing(10),
      },
      color: "#2ecc71"
    },
    more: {
      marginTop: theme.spacing(2),
      color: "#2ecc71"
    },
  });

function ProductHero(props) {
  const { classes } = props;

  return (
    <ProductHeroLayout
      backgroundClassName={classes.background}
    >
      {/* Increase the network loading priority of the background image. */}
      <img
        style={{ display: 'none' }}
        src={backgroundImage}
        alt="increase priority"
      />
      <Typography color="inherit" align="center" variant="h2" marked="center" className={classes.h5}>
        Learn from Our Mentors
      </Typography>
      <Typography
        color="inherit"
        align="center"
        variant="h5"
        className={classes.h5}
      >
        Our verified mentors are current students at top U.K. universities.
      </Typography>
      <Button
        color="primary"
        variant="contained"
        size="large"
        component="a"
        href="/premium-themes/onepirate/sign-up/"
        className={classes.button}
      >
        Register
      </Button>
      <Typography variant="body2" color="inherit" className={classes.more}>
        Seek your Guidance Now!
      </Typography>
    </ProductHeroLayout>
  );
}

ProductHero.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductHero);