import * as React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Typography from '../components/Typography';

const styles = (theme) => ({
  root: {
    display: 'flex',
    overflow: 'hidden',
    flexDirection: 'row',
  },
  container: {
    marginTop: theme.spacing(15),
    marginBottom: theme.spacing(30),
    display: 'flex',
    position: 'relative',
  },
  item: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(0, 5),
  },
  image: {
    height: 130,
  },
  title: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    fontFamily: 'Roboto Condensed',
    fontWight: 'fontWeightBold',
  },
  curvyLines: {
    pointerEvents: 'none',
    position: 'absolute',
    top: -180,
  },
});

function SellingPoints(props) {
  const { classes } = props;

  return (
    <section className={classes.root}>
      <Container className={classes.container}>
        <img
          src="/static/themes/onepirate/productCurvyLines.png"
          className={classes.curvyLines}
          alt="curvy lines"
        />
        <Grid container spacing={5}>
          <Grid item xs={12} md={12}>
            <div className={classes.item}>
              <img
                className={classes.image}
                src={"/education.svg"}
                alt="university"
              />
              <Typography variant="h5" className={classes.title}>
                One-on-one Live University Entry Advice
              </Typography>
              <Typography variant="subtitle1">
                {'Our mentors come from top U.K. universities.'}
              </Typography>
              <Typography variant="subtitle1">
                {'Ask them anything about the university, courses, accomodation and city life!'}
              </Typography>
            </div>
          </Grid>
          <Grid item xs={12} md={12}>
            <div className={classes.item}>
              <img
                className={classes.image}
                src={"/interviews.svg"}
                alt="graph"
              />
              <Typography variant="h5" className={classes.title}>
                Interviews Practice, Help with CV & Personal Statement
              </Typography>
              <Typography variant="subtitle1">
                {'Our mentors can quickly organize 1-on-1 interview practice to prepare you for the best before the real ones.'}
              </Typography>
              <Typography variant="subtitle1">
                {'You can also upload your CV and personal statements for them to comment on!'}
              </Typography>
            </div>
          </Grid>
          <Grid item xs={12} md={12}>
            <div className={classes.item}>
              <img
                className={classes.image}
                src={"/hangout.svg"}
                alt="clock"
              />
              <Typography variant="h5" className={classes.title}>
                Build Early Connection with Our Senior Students at Uni
              </Typography>
              <Typography variant="subtitle1">
                {'Make early connections even before the school term begins! '}
              </Typography>
              <Typography variant="subtitle1">
                {'They can guide you anything from academics to getting internships.'}
              </Typography>
            </div>
          </Grid>
        </Grid>
      </Container>
    </section>
  );
}

SellingPoints.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SellingPoints);